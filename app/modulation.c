// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   modulation.c
//
//      CONTENTS    :   Routines for communication and control of modulated
//                      data output
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
FlagStatus Flag_Build_TASK = RESET;
TypeDef_ModulationMode ModulationMode = MODULATION_MODE_TASK;
siMet1Modulation iMet1Modulation;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static uint32_t FM0InterruptCounter = 0;
static uint32_t FM0ByteNumber = 0;
static uint32_t FM0Byte = 0;
static uint8_t FM0Bit = 0x80;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void InitTaskTimer(void);
static void InitiMet1Timer(void);
static void ResetBuffer(siMet1Modulation* ptrModulation);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// *************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the modulation data pin.
//
//  UPDATED   : 2016-04-06 JHM
//
// *************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for GPIOX
  RCC_AHBPeriphClockCmd(MOD0_GPIO_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = MOD0_PIN;
  GPIO_Init(MOD0_PORT, &GPIO_InitStructure);

  // Start with the pin low
  GPIO_ResetBits(MOD0_PORT, MOD0_PIN);
} // InitGPIO

// *************************************************************************
//
//  FUNCTION  : InitTaskTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the timer for the modulation data. No prescaler
//              is used in an attempt to make the modulation clock as
//              accurate as possible. Based on the calculation:
//              N = 11719
//              SYSCLK = 48000000
//              TIMCLK = SYSCLK / N = 4095.91262 Hz
//
//  UPDATED   : 2016-04-08 JHM
//
// *************************************************************************
static void InitTaskTimer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // Reset the timer peripheral to defaults
  TIM_DeInit(MOD0_TIM);

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(MOD0_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set the interrupt priority to highest
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

  TIM_TimeBaseStructure.TIM_Prescaler = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 11719 - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(MOD0_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(MOD0_TIM, TIM_IT_Update, DISABLE);

  // Start with the timer disabled
  TIM_Cmd(MOD0_TIM, DISABLE);
} // InitTimer

// *************************************************************************
//
//  FUNCTION  : InitiMet1Timer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2016-07-21 JHM
//
// *************************************************************************
static void InitiMet1Timer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // Disable the timer peripheral
  TIM_Cmd(IMET1_TIM, DISABLE);

  // Reset the timer peripheral to defaults
  TIM_DeInit(IMET1_TIM);

  // Enable the clock for the timer
  RCC_APB1PeriphClockCmd(IMET1_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set the interrupt priority to highest
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

  // Set the timer clock to 264 kHz
  TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(SystemCoreClock / IMET1_TIM_CLK) - 1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  // Set period to maximum value
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(IMET1_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);

  // Start with the timer disabled
  TIM_Cmd(IMET1_TIM, DISABLE);
} // InitiMet1Timer

// *************************************************************************
//
//  FUNCTION  : ResetBuffer
//
//  I/P       : siMet1Modulation* ptrModulation = Pointer to iMet-1 modulation
//                                                data structure
//
//  O/P       : None.
//
//  OPERATION : Resets the values and buffer for iMet-1 modulation
//
//  UPDATED   : 2016-07-21 JHM
//
// *************************************************************************
static void ResetBuffer(siMet1Modulation* ptrModulation)
{
  int i;

  ptrModulation->Start = 0;
  ptrModulation->End = 0;
  ptrModulation->BitCounter = 0;
  ptrModulation->CCR_Value = IMET1_MARK_CCR_VAL;

  for (i = 0; i < IMET1_BUF_SIZE; i++)
  {
    ptrModulation->Buffer[i] = 0;
  }
} // ResetBuffer

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Modulation_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the modulation hardware and data.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
void Modulation_Init(void)
{
  // Initialize the GPIO
  InitGPIO();

  // Get the modulation mode from config
  ModulationMode = (TypeDef_ModulationMode)iQ_Config.ModulationMode;

  if (ModulationMode == MODULATION_MODE_TASK)
  {
    // Initialize the TASK timer peripheral
    InitTaskTimer();
  }
  else if (ModulationMode == MODULATION_MODE_IMET1)
  {
    // Reset the buffer
    ResetBuffer(&iMet1Modulation);

    // Initialize the iMet-1 timer peripheral
    InitiMet1Timer();
  }
  else
  {
    Modulation_Disable();
  }
} // Modulation_Init

// **************************************************************************
//
//  FUNCTION  : Modulation_Enable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Enables the modulation interrupt. This routine should not be
//              called until the data buffer is full.
//
//  UPDATED   : 2016-04-08 JHM
//
// **************************************************************************
void Modulation_Enable(void)
{
  if (ModulationMode == MODULATION_MODE_TASK)
  {
    // Enable the overflow interrupt
    TIM_ITConfig(MOD0_TIM, TIM_IT_Update, ENABLE);

    // Enable the timer
    TIM_Cmd(MOD0_TIM, ENABLE);
  }
  else if (ModulationMode == MODULATION_MODE_IMET1)
  {
    // Disable the clock to reset to zero
    TIM_Cmd(IMET1_TIM, DISABLE);

    // Reset the clock
    TIM_SetCounter(IMET1_TIM, 0);

    // Set the CCR values
    TIM_SetCompare1(IMET1_TIM, IMET1_BAUD_CCR_VAL);
    TIM_SetCompare2(IMET1_TIM, IMET1_MARK_CCR_VAL);

    // Enable the CCR interrupts
    TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, ENABLE);

    // Enable the timer
    TIM_Cmd(IMET1_TIM, ENABLE);
  }
} // Modulation_Enable

// **************************************************************************
//
//  FUNCTION  : Modulation_Disable
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Disables the modulation interrupt
//
//  UPDATED   : 2017-04-10 JHM
//
// **************************************************************************
void Modulation_Disable(void)
{
  // Disable the Update interrupt for TASK mode if it is enabled
  if (MOD0_TIM->DIER & TIM_DIER_UIE)
  {
    TIM_ITConfig(MOD0_TIM, TIM_IT_Update, DISABLE);
  }

  // Disable the CCR interrupts for iMet-4 mode if they are enabled
  if (MOD0_TIM->DIER & (TIM_DIER_CC1IE | TIM_DIER_CC2IE))
  {
    TIM_ITConfig(IMET1_TIM, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);
  }

  // Disable both timers if they are enabled
  if (MOD0_TIM->CR1 & TIM_CR1_CEN)
  {
    TIM_Cmd(MOD0_TIM, DISABLE);
  }
  if (IMET1_TIM->CR1 & TIM_CR1_CEN)
  {
    TIM_Cmd(IMET1_TIM, DISABLE);
  }
} // Modulation_Disable

// **************************************************************************
//
//  FUNCTION  : Modulation_TASK_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called each time the modulation data
//              interrupt occurs.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
void Modulation_TASK_Handler(void)
{
  // Test frequency - this should be 4096 / 2 = 2048 Hz
  //MOD0_PORT->ODR ^= MOD0_PIN;

  uint32_t index;

  FM0InterruptCounter++;

  if (FM0InterruptCounter % 2)
  {
    if(FM0Bit & FM0Byte);
      //do nothing
    else
    {
      //toggle for a zero
      MOD0_PORT->ODR ^= MOD0_PIN;
    }
  }
  else
  {
    if (FM0Bit == 1)
    {
      FM0Bit = 0x80;
      FM0Byte = *TXmsgPointer++;
      FM0ByteNumber++;
    }
    else
      FM0Bit >>= 1;                // next bit

      // Always toggle for rising edge
    MOD0_PORT->ODR ^= MOD0_PIN;
  }

  if(FM0ByteNumber > 63)
  {
    FM0ByteNumber = 0;

    index = TXmsgNumber % 4;

    TXmsgNumber++;
    TXmsgPointer = &TXmsgOut[index].Byte[0];

    // Let the main know we need a new packet built (since buffered,
    // we will have 250ms)
    Flag_Build_TASK = SET;
  }
} // Modulation_TASK_Handler

// **************************************************************************
//
//  FUNCTION  : Modulation_iMet1_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called each time iMet-1 baud rate
//              interrupt occurs.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
void Modulation_iMet1_Handler(void)
{
  // Uncomment this for baud clock testing
  //MOD0_PORT->ODR ^= MOD0_PIN;

  if (iMet1Modulation.BitCounter == 0)
  {
    if (iMet1Modulation.Start == iMet1Modulation.End)
    {
      // Buffer is empty. Keep CCR value at MARK
      iMet1Modulation.CCR_Value = IMET1_MARK_CCR_VAL;
    }
    else
    {
      // New data to send, so send the start bit
      iMet1Modulation.CCR_Value = IMET1_SPACE_CCR_VAL;
      // Increment the bitcounter
      iMet1Modulation.BitCounter++;
    }
  }
  else
  {
    if (iMet1Modulation.BitCounter < 9)
    {
      if (iMet1Modulation.Buffer[iMet1Modulation.Start] & (0x01 << (iMet1Modulation.BitCounter - 1)))
      {
        iMet1Modulation.CCR_Value = IMET1_MARK_CCR_VAL;
      }
      else
      {
        iMet1Modulation.CCR_Value = IMET1_SPACE_CCR_VAL;
      }
      // Increment the bitcounter
      iMet1Modulation.BitCounter++;
    }
    else
    {
      // Send the stop bit
      iMet1Modulation.CCR_Value = IMET1_MARK_CCR_VAL;
      // Reset the bitcounter
      iMet1Modulation.BitCounter = 0;
      // Advance the buffer (safe)
      iMet1Modulation.Start = (iMet1Modulation.Start + 1) % IMET1_BUF_SIZE;
    }
  }
} // Modulation_iMet1_Handler

// **************************************************************************
//
//  FUNCTION  : Modulation_iMet1_Send
//
//  I/P       : uint8_t* Bytes = Pointer to the byte array to send
//              uint16_t Length = Number of bytes
//
//  O/P       : None.
//
//  OPERATION : Writes data to the modulation buffer. This will automatically
//              be handled by the interrupts and send out the data pin.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
void Modulation_iMet1_Send(uint8_t* Bytes, uint16_t Length)
{
  int i;

  for (i = 0; i < Length; i++)
  {
    // Add the data to the next location
    iMet1Modulation.Buffer[iMet1Modulation.End] = Bytes[i];

    // Advance the buffer
    iMet1Modulation.End = (iMet1Modulation.End + 1) % IMET1_BUF_SIZE;
  }
} // Modulation_iMet1_Send

