#ifndef __UTILITIES_H
#define __UTILITIES_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   utilities.h
//
//      CONTENTS    :   Header file for various utility routines
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************

// *************************************************************************
// TYPES
// *************************************************************************

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void DelayMs(unsigned int uiDelay);
unsigned char HexConv(char* ucpData);
char* GetNextField(char* cpData);
int32_t ReadInt(char* cpData);
double ReadDouble(char* cpData);
void sprintUnsignedNumber(char* cpData, uint32_t Number, uint8_t CharLength);
void sprintSignedNumber(char* cpData, int32_t Number, uint8_t CharLength);
void sprintScientific(char* cpData, double Value, uint8_t DecimalPlaces);
void sprintHex(char* cpData, uint32_t Value, uint8_t CharLength);
char* strstr(const char* haystack, const char* needle);
void StrCat(char* destination, char* source);
void StrUpr(char* cpData);
double CalculateAverage(uint32_t* Array, uint16_t Count);

#endif
