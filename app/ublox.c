// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   ublox.c
//
//      CONTENTS    :   Routines for communication and control of Ublox M8
//                      Engine protocol
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// Headers
#define  UBX_HEADER1            0xB5
#define  UBX_HEADER2            0x62

// Message Classes
#define  CL_NAV             0x01
#define  CL_RXM             0x02
#define  CL_INF             0x04
#define  CL_ACK             0x05
#define  CL_CFG             0x06
#define  CL_UPD             0x09
#define  CL_MON             0x0A
#define  CL_AID             0x0B
#define  CL_TIM             0x0D
#define  CL_MGA             0x13
#define  CL_LOG             0x21
#define  CL_NMEA            0xF0

// Configuration Message IDs
#define  ID_PRT                 0x00
#define  ID_MSG                 0x01
#define  ID_RST                 0x04
#define  ID_RATE                0x08
#define  ID_NAV5                0x24
#define  ID_NAVX5               0x23

// Acknowledge Messages IDs
#define  ID_NAK                 0x00
#define  ID_ACK                 0x01

// Navigation Message IDs
#define  ID_POSECEF             0x01
#define  ID_PVT                 0x07
#define  ID_VELECEF             0x11
#define  ID_SVINFO              0x30
#define  ID_SAT                 0x35
#define  ID_SOL                 0x06
#define  ID_POSLLH              0x02

// Reserved bytes
#define  UBX_RESERVED           0x00

// Message States
#define  MSG_ST_OFFLINE          0
#define  MSG_ST_HEADER1          1
#define  MSG_ST_HEADER2          2
#define  MSG_ST_CLASS            3
#define  MSG_ST_ID               4
#define  MSG_ST_SIZE             5
#define  MSG_ST_PAYLOAD          6
#define  MSG_ST_CHECKSUM         7
#define  MSG_ST_READY            8

// Error Bits
#define ERROR_BIT_NAV5           0b1
#define ERROR_BIT_NAVX5          0b10
#define ERROR_BIT_RATE           0b100
#define ERROR_BIT_POS            0b1000
#define ERROR_BIT_SAT            0b10000
#define ERROR_BIT_SOL            0b100000

// Satellite Flags
#define FLAG_SAT_SVUSED          0x08
#define FLAG_SAT_DIFF            0x40
#define FLAG_SAT_ORBITSRC        0x700
#define FLAG_SAT_EPH             0x800
#define FLAG_SAT_HEALTH          0x30
#define FLAG_SAT_ALMANAC         0x1000

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
uint8_t UBX_Status = UBX_STATUS_OFFLINE;

sUBX_TX GPS_TX_Buffer;
sUBX_RX GPS_RX_Buffer;

FlagStatus Flag_SOL_Ready = RESET;
FlagStatus Flag_POSLLH_Ready = RESET;
FlagStatus Flag_SAT_Ready = RESET;
FlagStatus Flag_GPS_Valid = RESET;

sUBX_MSG_BUF UBX_MSG_Buffer;
sUBX_SOL_Buffer UBX_SOL_Buffer;
sUBX_POSLLH_Buffer UBX_POSLLH_Buffer;
sUBX_SAT_Buffer UBX_SAT_Buffer;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static uint16_t Fletcher8Bit(uint8_t* cpData, uint8_t StartIndex, uint8_t StopIndex);
static void InitGPSUART(void);
static void ClearTxBuffer(sUBX_TX* Buffer);
static void ClearRxBuffer(sUBX_RX* Buffer);
static void ClearMessageBuffers(void);
static void MessageHandler(sUBX_MSG* ptrMessage, uint8_t Byte);
static uint8_t GetAcknowledge(uint8_t ClassID, uint8_t MsgID);
static FlagStatus ValidateSOL(sUBX_NAV_SOL* ptrSOL);
static uint8_t FormatSatFlags(uint32_t SAT_Flag);

static void ConfigurePort(void);
static void ConfigureNavEngine(void);
static void ConfigureNavEngineExpert(void);
static void ConfigureMeasurementRate(void);
static void ConfigureMessage(uint8_t Class, uint8_t ID, uint8_t Rate);

static void DecodeSAT(sUBX_NAV_SAT* ptrSAT, sUBX_MSG* ptrMsg);
static void DecodeSOL(sUBX_NAV_SOL* ptrSOL, sUBX_MSG* ptrMsg);
static void DecodePOSLLH(sUBX_NAV_POSLLH* ptrPOS, sUBX_MSG* ptrMsg);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// *************************************************************************
//
//  FUNCTION  : Fletcher8Bit
//
//  I/P       : cpData = Pointer to the data used in the checksum
//              StartIndex = Start of checksum calculation
//              StopIndex = End of checksum calculation
//
//  O/P       : 16-bit checksum
//
//  OPERATION : Calculates a 16-bit checksum using the 8-bit Fletcher Checksum
//              Algorithm (see http://www.ietf.org/rfc/rfc1145.txt)
//
//  UPDATED   : 2015-01-14 JHM
//
// *************************************************************************
static uint16_t Fletcher8Bit(uint8_t* cpData, uint8_t StartIndex, uint8_t StopIndex)
{
  uint16_t sum1 = 0;
  uint16_t sum2 = 0;
  int index;

  for (index = StartIndex; index <= StopIndex; index++)
  {
    sum1 = (sum1 + cpData[index]);
    sum2 = (sum2 + sum1);
  }

  return (sum2 << 8) | sum1;
} // Fletcher8Bit

// *************************************************************************
//
//  FUNCTION  : InitGPSUART
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPS UART for communication with the Ublox
//              M8 Engine.
//
//  UPDATED   : 2016-04-01 JHM
//
// *************************************************************************
static void InitGPSUART(void)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(UBX_PORT_RCC, ENABLE);
  RCC_APB1PeriphClockCmd(UBX_USART_RCC, ENABLE);

  // Configure Pins
  GPIO_InitStructure.GPIO_Pin = UBX_TX_PIN | UBX_RX_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_Init(UBX_PORT, &GPIO_InitStructure);

  // Set pins to alternate functions
  GPIO_PinAFConfig(UBX_PORT, UBX_TX_PIN_SRC, UBX_TX_AF);
  GPIO_PinAFConfig(UBX_PORT, UBX_RX_PIN_SRC, UBX_RX_AF);

  // Configure Serial Hardware
  USART_InitStructure.USART_BaudRate = UBX_BAUDRATE;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

  USART_Init(UBX_USART, &USART_InitStructure);

  // Initialize Interrupts
  NVIC_InitStructure.NVIC_IRQChannel = UBX_USART_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set priority to 1 - the only thing that takes priority over this is the
  // modulation interrupt
  //NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_Init(&NVIC_InitStructure);

  // Disable transmit interrupt (we will enable prior to transmission)
  USART_ITConfig(UBX_USART, USART_FLAG_TXE, DISABLE);
  // Wait for transmission to become available
  while (USART_GetFlagStatus(UBX_USART, USART_FLAG_TC) == RESET);

  // Wait for receiver to become available
  USART_ITConfig(UBX_USART, USART_IT_RXNE, ENABLE);
  while (USART_GetFlagStatus(UBX_USART, USART_FLAG_RXNE) != RESET);
  USART_ClearITPendingBit(UBX_USART, USART_IT_RXNE);

  // Enable USART
  USART_Cmd(UBX_USART, ENABLE);
} // InitGPSUART

// **************************************************************************
//
//  FUNCTION  : ClearTxBuffer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Clears the data in the GPS TX buffer and resets to
//              initial values.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void ClearTxBuffer(sUBX_TX* Buffer)
{
  int i;

  Buffer->Start = 0;
  Buffer->End = 0;

  for (i = 0; i < UBX_TX_SIZE; i++)
  {
    Buffer->Buffer[i] = 0;
  }
} // ClearTxBuffer

// **************************************************************************
//
//  FUNCTION  : ClearRxBuffer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Clears the data in the GPS RX buffer and resets to
//              initial values.
//
//  UPDATED   : 2016-04-04 JHM
//
// **************************************************************************
static void ClearRxBuffer(sUBX_RX* Buffer)
{
  int i;

  Buffer->Start = 0;
  Buffer->End = 0;

  for (i = 0; i < UBX_RX_SIZE; i++)
  {
    Buffer->Buffer[i] = 0;
  }
} // ClearTxBuffer

// **************************************************************************
//
//  FUNCTION  : ClearMessageBuffers
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Clears the global UBX_MSG_Buffer and UBX_SOL_Buffer.
//
//  UPDATED   : 2016-11-17 JHM
//
// **************************************************************************
static void ClearMessageBuffers(void)
{
  uint16_t i;

  // Initialize message buffer
  UBX_MSG_Buffer.Start = 0;
  UBX_MSG_Buffer.End = 0;

  // Initialize the local message structure
  UBX_MSG_Buffer.Message[0].State = MSG_ST_HEADER1;
  UBX_MSG_Buffer.Message[0].Index = 0;
  UBX_MSG_Buffer.Message[0].Class = 0;
  UBX_MSG_Buffer.Message[0].ID = 0;
  UBX_MSG_Buffer.Message[0].PayloadLength = 0;
  UBX_MSG_Buffer.Message[0].Counter = 0;
  UBX_MSG_Buffer.Message[0].Checksum = 0;
  UBX_MSG_Buffer.Message[0].Flag_LSB = RESET;

  // Clear the messages
  for (i = 0; i < UBX_MSG_MAX; i++)
  {
    UBX_MSG_Buffer.Message[0].Message[i] = 0;
  }

  for (i = 1; i < UBX_MSG_BUF_SIZE; i++)
  {
    UBX_MSG_Buffer.Message[i] = UBX_MSG_Buffer.Message[0];
  }
} // ClearMessageBuffers

// **************************************************************************
//
//  FUNCTION  : MessageHandler
//
//  I/P       : sUBX_MSG* ptrMessage = Pointer to a Ublox message data struct
//                      uint8_t Byte = The byte to be handled
//
//  O/P       : None.
//
//  OPERATION : This places a byte from the RX buffer into the message
//              structure. Then it updates the states machine. Once the last
//              byte of the message has arrived it sets the state to READY.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
static void MessageHandler(sUBX_MSG* ptrMessage, uint8_t Byte)
{
  // An error has occurred
  if (ptrMessage->Index > UBX_MSG_MAX - 1)
  {
    // Error has occurred, reset the machine
    ptrMessage->State = MSG_ST_HEADER1;
  }

  // Move the byte into the next position
  ptrMessage->Message[ptrMessage->Index] = Byte;

  switch (ptrMessage->State)
  {
    case MSG_ST_HEADER1:
      // Set the index to 0 for start of message
      ptrMessage->Index = 0;
      // Check for first header
      if (Byte == UBX_HEADER1)
      {
        // Increment the index
        ptrMessage->Index++;
        // Advance the state machine
        ptrMessage->State = MSG_ST_HEADER2;
      }
      else
      {
        ptrMessage->Index = 0;
      }
      break;

    case MSG_ST_HEADER2:
      // Check for second header
      if (Byte == UBX_HEADER2)
      {
        ptrMessage->Index++;
        ptrMessage->State = MSG_ST_CLASS;
      }
      else
      {
        // Reset the index
        ptrMessage->Index = 0;
        // Reset the state machine
        ptrMessage->State = MSG_ST_HEADER1;
      }
      break;

    case MSG_ST_CLASS:
      ptrMessage->Class = Byte;
      ptrMessage->Index++;
      // Advance the state machine
      ptrMessage->State = MSG_ST_ID;
      break;

    case MSG_ST_ID:
      ptrMessage->ID = Byte;
      ptrMessage->Index++;
      // Advance the state machine
      ptrMessage->State = MSG_ST_SIZE;
      break;

    case MSG_ST_SIZE:
      if (ptrMessage->Flag_LSB == RESET)
      {
        ptrMessage->PayloadLength = (uint16_t)Byte;
        ptrMessage->Flag_LSB = SET;
      }
      else
      {
        ptrMessage->PayloadLength = ((uint16_t)Byte << 8) + ptrMessage->PayloadLength;
        ptrMessage->Counter = ptrMessage->PayloadLength;
        // Advance the state machine
        ptrMessage->State = MSG_ST_PAYLOAD;
        ptrMessage->Flag_LSB = RESET;
      }
      ptrMessage->Index++;
      break;

    case MSG_ST_PAYLOAD:
      // Decrement counter since we've already read the byte
      ptrMessage->Counter--;
      if (ptrMessage->Counter == 0)
      {
        // Advance the state machine
        ptrMessage->State = MSG_ST_CHECKSUM;
      }
      ptrMessage->Index++;
      break;

    case MSG_ST_CHECKSUM:
      if (ptrMessage->Flag_LSB == RESET)
      {
        ptrMessage->Checksum = (uint16_t)Byte;
        ptrMessage->Flag_LSB = SET;
        ptrMessage->Index++;
      }
      else
      {
        ptrMessage->Checksum = ((uint16_t)Byte << 8) + ptrMessage->Checksum;
        ptrMessage->Flag_LSB = RESET;
        // Advance the state machine
        ptrMessage->State = MSG_ST_READY;
      }
      break;

      case MSG_ST_READY:
        ptrMessage->Index = 0;
        break;
  } // switch
} // UBX_MessageHandler

// **************************************************************************
//
//  FUNCTION  : GetAcknowledge
//
//  I/P       : uint8_t ClassID = The class ID of the ACK/NAK
//              uint8_t MsgID = The message ID of the ACK/NAK
//
//  O/P       : 0 = NAK
//              1 = ACK
//
//  OPERATION : Reads a message and returns 1 if the message is an
//              acknowledge message for the class and message ID in question.
//              After reading a message, it increments the message buffer
//              index regardless of the result. This routine is meant to be
//              called immediately after a configuration command.
//
//  UPDATED   : 2017-08-19 JHM
//              20220204   jww
//
// **************************************************************************
static uint8_t GetAcknowledge(uint8_t ClassID, uint8_t MsgID)
{
#define     ACK_WAIT_TIME   1000            // wait max of 1 sec for ack/nak

    uint8_t     stat = 0;                   // default status is no ACK

    Wait(UBX_WAIT_CH, ACK_WAIT_TIME);                       // start timer
    while (GetWaitFlagStatus(UBX_WAIT_CH) == SET) {         // check messages until timeout
        if (UBX_MSG_Buffer.Start != UBX_MSG_Buffer.End) {   // message available
            if ((UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].Class == CL_ACK) && \
               (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].Message[6] == ClassID) && \
               (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].Message[7] == MsgID)) {        // ACK/NAK message
                if (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].ID == ID_ACK) {            // ACK received
                    stat = 1;
                } else {                // no ACK received
                    stat = 0;
                }
                UBX_MSG_Buffer.Start = (UBX_MSG_Buffer.Start + 1) % UBX_MSG_BUF_SIZE;       // clear msg
                return stat;            // ACK/NAK status found
            } else {                    // clear unused msg
                UBX_MSG_Buffer.Start = (UBX_MSG_Buffer.Start + 1) % UBX_MSG_BUF_SIZE;
            }
        }
    }
    return stat;            // timeout waiting for ACK/NAK

} // GetAcknowledge

// **************************************************************************
//
//  FUNCTION  : ValidateSOL
//
//  I/P       : sUBX_NAV_SOL* = Pointer to a NAV_SOL data structure to be
//                              validated.
//
//  O/P       : FlagStatus
//                SET = Data is valid
//                RESET = Data is invalid
//
//  OPERATION : Checks the NAV_SOL message to determine if there is an
//              adequate GPS lock.
//
//  UPDATED   : 2016-05-10 JHM
//
// **************************************************************************
static FlagStatus ValidateSOL(sUBX_NAV_SOL* ptrSOL)
{
  if (ptrSOL->gpsFix != 3)
  {
    return RESET;
  }

  if (ptrSOL->numSV < 4)
  {
    return RESET;
  }

  return SET;
} // ValidateSOL

// **************************************************************************
//
//  FUNCTION  : FormatSatFlags
//
//  I/P       : uint32_t = UBX_SAT message bitfield flags
//
//  O/P       : uint8_t = TASK bitfield flags
//
//  OPERATION : This routine rearranges the bits of the UBX_SAT message into
//              the flag configuration for TASK
//
//  UPDATED   : 2017-01-24 JHM
//
// **************************************************************************
static uint8_t FormatSatFlags(uint32_t SAT_Flag)
{
  uint8_t TASK_Flags = 0;

  if (SAT_Flag & FLAG_SAT_SVUSED)
  {
    TASK_Flags |= 0x01;
  }
  if (SAT_Flag & FLAG_SAT_DIFF)
  {
    TASK_Flags |= 0x02;
  }
  if ((SAT_Flag & FLAG_SAT_ALMANAC) || (SAT_Flag & FLAG_SAT_EPH))
  {
    TASK_Flags|= 0x04;
  }
  if (((SAT_Flag & FLAG_SAT_ORBITSRC) >> 8) == 1)
  {
    TASK_Flags |= 0x08;
  }
  if (((SAT_Flag & FLAG_SAT_HEALTH) >> 4) != 1)
  {
    TASK_Flags  |= 0x10;
  }
  if (((SAT_Flag & FLAG_SAT_ORBITSRC) >> 8) > 2)
  {
    TASK_Flags |= 0x20;
  }

  return TASK_Flags;
} // FormatSatFlags

// **************************************************************************
//
//  FUNCTION  : ConfigurePort
//
//  I/P       : USART_TypeDef* USARTx - Pointer to GPS UART port
//
//  O/P       : None.
//
//  OPERATION : Configures the CAM-M8Q GPS engine. This routine sets the
//              UART port to 9600/8/N/1/None and turns the Ublox protocol on
//              and the NMEA protocol off.
//
//  UPDATED   : 2015-10-09 JHM
//
// **************************************************************************
static void ConfigurePort(void)
{
  uint8_t Message[28];
  uint16_t Checksum;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_PRT;

  // Payload Length (Little Endian) = 20 bytes
  Message[4] = 0x14;
  Message[5] = 0x00;

  // Payload
  Message[6] = 0x01; // UART port ID
  Message[7] = UBX_RESERVED;
  Message[8] = 0x00; // No txReady
  Message[9] = 0x00; // No txReady

  // Set UART Mode (1 Stop Bit, No Parity, 8bit)
  Message[10] = 0xD0; // Mode LSB
  Message[11] = 0x08; // Mode
  Message[12] = 0x00; // Mode
  Message[13] = 0x00; // Mode MSB

  // Set UART baud rate
  Message[14] = (uint8_t)(UBX_BAUDRATE & 0xFF);
  Message[15] = (uint8_t)((UBX_BAUDRATE >> 8) & 0xFF);
  Message[16] = (uint8_t)((UBX_BAUDRATE >> 16) & 0xFF);
  Message[17] = (uint8_t)((UBX_BAUDRATE >> 24) & 0xFF);


  Message[18] = 0x01; // inProtoMask LSB = u-blox only, no NMEA
  Message[19] = 0x00; // inProtoMask MSB
  Message[20] = 0x01; // outProtoMask LSB = u-blox only, no NMEA
  Message[21] = 0x00; // outProtoMask MSB
  Message[22] = 0x00; // Flags (disable timeout)
  Message[23] = 0x00; // Flags
  Message[24] = UBX_RESERVED;
  Message[25] = UBX_RESERVED;

  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 25);
  Message[26] = (uint8_t)(Checksum & 0xFF);
  Message[27] = (uint8_t)(Checksum >> 8);

  // Advance the buffer so we know we are getting a fresh message
  UBX_MSG_Buffer.Start = UBX_MSG_Buffer.End;

  // Send the command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for the command to complete
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  // Wait 500ms for port configuration to complete
  Wait(UBX_WAIT_CH, 500);
  while (GetWaitFlagStatus(UBX_WAIT_CH) == SET);
} // ConfigurePort

// **************************************************************************
//
//  FUNCTION  : ConfigureNavEngine
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the navigation engine for optimal settings for
//              radiosonde flight
//
//  UPDATED   : 2016-04-05 JHM
//              20220204  jww
//
// **************************************************************************
static void ConfigureNavEngine(void)
{
  uint8_t Message[44];

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_NAV5;

  // Payload Length (Little Endian) = 36 bytes
  Message[4] = 0x24;
  Message[5] = 0x00;

  // Payload
  // Mask (all set to apply all parameters)
  Message[6] = 0xFF;
  Message[7] = 0xFF;
  // Dynamic model = airborne with <2g acceleration
  Message[8] = 0x07;
  // Fix mode = 3D only
  Message[9] = 0x2;
  // Fixed altitude for 2D fix mode = 0 m
  Message[10] = 0;
  Message[11] = 0;
  Message[12] = 0;
  Message[13] = 0;
  // Fixed altitude variance for 2D mode = 10,000 m^2
  Message[14] = 0x10;
  Message[15] = 0x27;
  Message[16] = 0x00;
  Message[17] = 0x00;
  // Minimum elevation = 0 degrees
  Message[18] = 0x00;
  // Reserved
  Message[19] = UBX_RESERVED;
  // Position DOP Mask to use
  Message[20] = 0xFA;
  Message[21] = 0x00;
  // Time DOP Mask to use
  Message[22] = 0xFA;
  Message[23] = 0x00;
  // Position Accuracy Mask
  Message[24] = 0x64;
  Message[25] = 0x00;
  // Time Accuracy Mask
  Message[26] = 0x2C;
  Message[27] = 0x01;
  // Static hold threshold = 0 cm/s
  Message[28] = 0x00;
  // DGPS timeout = 60 seconds
  Message[29] = 0x3C;
  // Number of satellites required to have C/N0 above cnoThresh for
  // a fix to be attempted = 0
  Message[30] = 0;
  // C/N0 threshold for deciding whether to attempt a fix = 0
  Message[31] = 0;
  // Reserved
  Message[32] = UBX_RESERVED;
  Message[33] = UBX_RESERVED;
  // Static hold distance threshold = 200 m
  Message[34] = 0xC8;
  Message[35] = 0x00;
  // UTC standard to be used = Unspecified
  Message[36] = 0;
  // Reserved
  Message[37] = UBX_RESERVED;
  Message[38] = UBX_RESERVED;
  Message[39] = UBX_RESERVED;
  Message[40] = UBX_RESERVED;
  Message[41] = UBX_RESERVED;

  // Calculate Checksum (this failed, I had to use the checksum from the
  // development board)
  Message[42] = 0x15;
  Message[43] = 0xB1;

  // Advance the buffer so we know we are getting a fresh message
  UBX_MSG_Buffer.Start = UBX_MSG_Buffer.End;

  // Send the command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for transmission to complete
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  if (!GetAcknowledge(CL_CFG, ID_NAV5))
  {
    UBX_Status = UBX_STATUS_ERROR;
  }

  // Block so we can't immediately send another command
  Wait(UBX_WAIT_CH, 50);
  while (GetWaitFlagStatus(UBX_WAIT_CH) == SET);
} // ConfigureNavEngine

// **************************************************************************
//
//  FUNCTION  : ConfigureNavEngineExpert
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the navigation engine minimum and maximum SV info
//
//  UPDATED   : 2015-11-09 JHM
//              20220204  jww
//
// **************************************************************************
static void ConfigureNavEngineExpert(void)
{
  uint8_t Message[48];
  uint16_t Checksum;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_NAVX5;

  // Payload Length (Little Endian) = 40 bytes
  Message[4] = 0x28;
  Message[5] = 0x00;

  // Payload
  // Version = set to zero for this version
  Message[6] = 0x00;
  Message[7] = 0x00;

  // Mask 1 = Set all possible masks to load each configuration option
  Message[8] = 0x4C;
  Message[9] = 0x66;

  // Mask 2 = all zeros
  Message[10] = 0;
  Message[11] = 0;
  Message[12] = 0;
  Message[13] = 0;

  // Reserved bytes
  Message[14] = UBX_RESERVED;
  Message[15] = UBX_RESERVED;

  // Minimum number of satellites for navigation = 4
  Message[16] = 0x04;
  // Maximum number of satellites = 16
  Message[17] = 0x10;
  // Minimum CNO = default
  Message[18] = 0x06;

  // Reserved
  Message[19] = UBX_RESERVED;

  // Initial fix must be 3D
  Message[20] = 0x01;

  // Reserved
  Message[21] = UBX_RESERVED;
  Message[22] = UBX_RESERVED;

  // Ack Aiding = do not issue acknowledgements for ack aiding
  Message[23] = 0;

  // GPS week rollover = default
  Message[24] = 0xDC;
  Message[25] = 0x06;

  // Reserved
  Message[26] = UBX_RESERVED;
  Message[27] = UBX_RESERVED;
  Message[28] = UBX_RESERVED;
  Message[29] = UBX_RESERVED;
  Message[30] = UBX_RESERVED;
  Message[31] = UBX_RESERVED;

  // Do not use PPP
  Message[32] = 0;
  // Do not use AssistNow
  Message[33] = 0;

  Message[34] = UBX_RESERVED;
  Message[35] = UBX_RESERVED;

  // Maximum acceptable orbit error = default
  Message[36] = 0x64;
  Message[37] = 0x00;

  Message[38] = UBX_RESERVED;
  Message[39] = UBX_RESERVED;
  Message[40] = UBX_RESERVED;
  Message[41] = UBX_RESERVED;
  Message[42] = UBX_RESERVED;
  Message[43] = UBX_RESERVED;
  Message[44] = UBX_RESERVED;

  // Do not use address
  Message[45] = 0;

  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 45);
  Message[46] = (uint8_t)(Checksum & 0xFF);
  Message[47] = (uint8_t)(Checksum >> 8);

  // Advance the buffer so we know we are getting a fresh message
  UBX_MSG_Buffer.Start = UBX_MSG_Buffer.End;

  // Send the command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for transmission to complete
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  if (!GetAcknowledge(CL_CFG, ID_NAVX5))
  {
    UBX_Status = UBX_STATUS_ERROR;
  }

  // Block so we can't immediately send another command
  Wait(UBX_WAIT_CH, 50);
  while (GetWaitFlagStatus(UBX_WAIT_CH) == SET);
} // ConfigureNavEngineExpert

// **************************************************************************
//
//  FUNCTION  : ConfigureMeasurementRate
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the measurement rate to 4 Hz. Each individual
//              message can still be configured to less than this.
//
//  UPDATED   : 2016-04-06 JHM
//              20220204  jww
//
// **************************************************************************
static void ConfigureMeasurementRate(void)
{
  uint8_t Message[14];
  //uint16_t Checksum;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_RATE;

  // Payload Length (Little Endian) = 6 bytes
  Message[4] = 0x06;
  Message[5] = 0x00;

  // Payload
  // Measurement Rate = 250 ms
  Message[6] = 0xFA;
  Message[7] = 0x00;
  // Navigation Rate = must be set to 1
  Message[8] = 0x01;
  Message[9] = 0;
  // Alignment to reference time = GPS time
  Message[10] = 1;
  Message[11] = 0;

  // Calculate Checksum - this does not match the development board. I had
  // to hard code this from the development board values
  //Checksum = Fletcher8Bit(Message, 2, 11);
  //Message[12] = (uint8_t)(Checksum & 0xFF);
  //Message[13] = (uint8_t)(Checksum >> 8);
  Message[12] = 0x10;
  Message[13] = 0x96;

  // Advance the buffer so we know we are getting a fresh message
  UBX_MSG_Buffer.Start = UBX_MSG_Buffer.End;

  // Send the command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for transmission to complete
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  if (!GetAcknowledge(CL_CFG, ID_RATE))
  {
    UBX_Status = UBX_STATUS_ERROR;
  }

  // Block so we can't immediately send another command
  Wait(UBX_WAIT_CH, 50);
  while (GetWaitFlagStatus(UBX_WAIT_CH) == SET);
} // ConfigureMeasurementRate

// **************************************************************************
//
//  FUNCTION  : ConfigureMessage
//
//  I/P       : USART_TypeDef* USARTx - Pointer to GPS UART port
//              uint8_t Class - UBX protocol message class
//              uint8_t ID - UBX protocol message ID
//              uint8_t Interval - Date report rate in seconds
//
//  O/P       : None.
//
//  OPERATION : Configures the messages to report PVT at 1Hz.
//
//  UPDATED   : 2015-10-09 JHM
//              20220204  jww
//
// **************************************************************************
static void ConfigureMessage(uint8_t Class, uint8_t ID, uint8_t Rate)
{
  uint8_t Message[16];
  uint16_t Checksum;

  // Construct Packet
  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_MSG;

  // Payload Length (Little Endian) = 20 bytes
  Message[4] = 0x08;
  Message[5] = 0x00;

  // Payload
  Message[6] = Class; // msgClass
  Message[7] = ID; // PVT message
  Message[8] = 0x00; // Nothing on DDC port
  Message[9] = Rate; // 1 sec on UART port
  Message[10] = 0x00; // Nothing else past here
  Message[11] = 0x00;
  Message[12] = 0x00;
  Message[13] = 0x00;

  // Calculate Checksum
  Checksum = Fletcher8Bit(Message, 2, 13);
  Message[14] = (uint8_t)(Checksum & 0xFF);
  Message[15] = (uint8_t)(Checksum >> 8);

  // Advance the buffer so we know we are getting a fresh message
  UBX_MSG_Buffer.Start = UBX_MSG_Buffer.End;

  // Send the command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for transmission to complete
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  if (!GetAcknowledge(CL_CFG, ID_MSG))
  {
    UBX_Status = UBX_STATUS_ERROR;
  }

  // Block so we can't immediately send another command
  Wait(UBX_WAIT_CH, 50);
  while (GetWaitFlagStatus(UBX_WAIT_CH) == SET);
} // ConfigureMessage


// **************************************************************************
//
//  FUNCTION  : DecodeSAT
//
//  I/P       : sGPS_Port* GPSPort - Pointer to GPS UART port
//
//  O/P       : None.
//
//  OPERATION : Takes the SAT binary message and moves it into the
//              appropriate locations of the TASK packet data structure.
//
//  UPDATED   : 2016-11-16 JHM
//
// **************************************************************************
static void DecodeSAT(sUBX_NAV_SAT* ptrSAT, sUBX_MSG* ptrMsg)
{
  uint8_t* ptr;
  int i;
  uint8_t GPS_Counter = 0;

  if ((ptrMsg->Class != CL_NAV) || (ptrMsg->ID != ID_SAT))
  {
    return;
  }

  // Start the pointer at the start of the payload, skipping Headers[2], Class,
  // ID, and Length[2]
  ptr = &ptrMsg->Message[6];

  // GPS time of week
  memcpy(&ptrSAT->iTOW, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);

  // Version (should always be 1)
  memcpy(&ptrSAT->version, ptr, sizeof(uint8_t));
  ptr++;

  // numSvs
  memcpy(&ptrSAT->numSvs, ptr, sizeof(uint8_t));
  ptr++;

  // Move past two reserved bytes
  ptr++;
  ptr++;

  // Start of repeated block
  for (i = 0; (GPS_Counter < MAX_GPS_SATELLITES) && (i < ptrSAT->numSvs); i++)
  {
    // gnssId
    memcpy(&ptrSAT->Satellites[GPS_Counter].gnssId, ptr, sizeof(uint8_t));
    ptr++;

    // svId
    memcpy(&ptrSAT->Satellites[GPS_Counter].svId, ptr, sizeof(uint8_t));
    ptr++;

    // cno
    memcpy(&ptrSAT->Satellites[GPS_Counter].cno, ptr, sizeof(uint8_t));
    ptr++;

    // elev
    memcpy(&ptrSAT->Satellites[GPS_Counter].elev, ptr, sizeof(int8_t));
    ptr++;

    // azim
    memcpy(&ptrSAT->Satellites[GPS_Counter].azim, ptr, sizeof(int16_t));
    ptr += sizeof(int16_t);

    // Pseudo range residual
    memcpy(&ptrSAT->Satellites[GPS_Counter].prRes, ptr, sizeof(int16_t));
    ptr += sizeof(int16_t);

    // Flags
    memcpy(&ptrSAT->Satellites[GPS_Counter].flags, ptr, sizeof(uint32_t));
    ptrSAT->Satellites[GPS_Counter].Flag_TASK = FormatSatFlags(ptrSAT->Satellites[GPS_Counter].flags);
    ptr += sizeof(uint32_t);

    if (ptrSAT->Satellites[GPS_Counter].gnssId == 0)
    {
      GPS_Counter++;
    }
  }

  // Update the satellites to only include GPS satellites
  ptrSAT->numSvs = GPS_Counter;


  // Fill in remaining block with zero if necessary
  for (i = ptrSAT->numSvs; i < MAX_GPS_SATELLITES; i++)
  {
    ptrSAT->Satellites[i].gnssId = 1;
    ptrSAT->Satellites[i].svId = 0;
    ptrSAT->Satellites[i].cno = 0;
    ptrSAT->Satellites[i].elev = 0;
    ptrSAT->Satellites[i].azim = 0;
    ptrSAT->Satellites[i].prRes = 0;
    ptrSAT->Satellites[i].flags = 0;
  }

} // DecodeSAT


// **************************************************************************
//
//  FUNCTION  : DecodeSOL
//
//  I/P       : sGPS_Port* GPSPort - Pointer to GPS UART port
//              sUBX_NAV_SOL* ptrMsg - Pointer to the destination structure
//
//  O/P       : None.
//
//  OPERATION : Takes the SOL binary message and moves it into the
//              solution message structure
//
//  UPDATED   : 2015-11-05 JHM
//
// **************************************************************************
static void DecodeSOL(sUBX_NAV_SOL* ptrSOL, sUBX_MSG* ptrMsg)
{
  uint8_t* ptr;

  if ((ptrMsg->Class != CL_NAV) || (ptrMsg->ID != ID_SOL))
  {
    return;
  }

  // Start the pointer at the start of the payload, skipping Header[2], Class,
  // ID, and Length[2]
  ptr = &ptrMsg->Message[6];
  // GPS time of week
  memcpy(&ptrSOL->iTOW, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
  // Fractional part of iTOW
  memcpy(&ptrSOL->fTOW, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
  // GPS week
  memcpy(&ptrSOL->week, ptr, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // gpsFix
  memcpy(&ptrSOL->gpsFix, ptr, sizeof(uint8_t));
  ptr++;
  // flags
  memcpy(&ptrSOL->flags, ptr, sizeof(uint8_t));
  ptr++;
  // ecef positions
  memcpy(&ptrSOL->ecefX, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  memcpy(&ptrSOL->ecefY, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  memcpy(&ptrSOL->ecefZ, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // position accuracy
  memcpy(&ptrSOL->pAcc, ptr, sizeof(uint32_t));
  ptr += sizeof(int32_t);
  // ecef velocities
  memcpy(&ptrSOL->ecefVX, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  memcpy(&ptrSOL->ecefVY, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  memcpy(&ptrSOL->ecefVZ, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // velocity accuracy
  memcpy(&ptrSOL->sAcc, ptr, sizeof(uint32_t));
  ptr += sizeof(int32_t);
  // Position DOP
  memcpy(&ptrSOL->pDOP, ptr, sizeof(uint16_t));
  ptr += sizeof(uint16_t);
  // Move past reserved
  ptr++;
  // Number of SVs used in solution
  memcpy(&ptrSOL->numSV, ptr, sizeof(uint8_t));
} // DecodeSOL

// **************************************************************************
//
//  FUNCTION  : DecodePOSLLH
//
//  I/P       : sUBX_NAV_POSLLH* ptrPOS = Pointer to destination message
//              sUBX_MSG* ptrMsg = Pointer to UBX message to decode
//
//  O/P       : None.
//
//  OPERATION : Takes the SOL binary message and moves it into the
//              solution message structure
//
//  UPDATED   : 2015-11-05 JHM
//
// **************************************************************************
static void DecodePOSLLH(sUBX_NAV_POSLLH* ptrPOS, sUBX_MSG* ptrMsg)
{
  uint8_t* ptr;

  if ((ptrMsg->Class != CL_NAV) || (ptrMsg->ID != ID_POSLLH))
  {
    return;
  }

  // Start the pointer at the start of the payload, skipping Header[2], Class,
  // ID, and Length[2]
  ptr = &ptrMsg->Message[6];
  // GPS time of week
  memcpy(&ptrPOS->iTOW, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
  // Longitude
  memcpy(&ptrPOS->lon, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // Latitude
  memcpy(&ptrPOS->lat, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // Height EES
  memcpy(&ptrPOS->hEES, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // Height MSL
  memcpy(&ptrPOS->hMSL, ptr, sizeof(int32_t));
  ptr += sizeof(int32_t);
  // Horizontal Accuracy
  memcpy(&ptrPOS->hAcc, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
  // Vertical Accuracy
  memcpy(&ptrPOS->hAcc, ptr, sizeof(uint32_t));
  ptr += sizeof(uint32_t);
} // DecodePOSLLH

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Ublox_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the CAM-M8Q data structure and configures the
//              engine.
//
//  UPDATED   : 2015-10-08 JHM
//
// **************************************************************************
void Ublox_Init(void)
{
  DelayMs(100);
  uint8_t ErrorCode = 0;

  // Update the status
  UBX_Status = UBX_STATUS_OFFLINE;

  // Initialize the buffers
  ClearTxBuffer(&GPS_TX_Buffer);
  ClearRxBuffer(&GPS_RX_Buffer);
  ClearMessageBuffers();

  // Initialize the hardware
  InitGPSUART();

  // Reset the engine. This is a hard reset that will start spewing out NMEA
  // messages
  Ublox_WatchdogReset();

  // Disable the GNSS - this will turn off all the messages and leave the
  // USART free for configuration messages
  Ublox_GNSS_Stop();

  // Clear the buffers here again for good measure
  ClearRxBuffer(&GPS_RX_Buffer);
  ClearMessageBuffers();

  // Configure the port for UBX and disable NMEA
  ConfigurePort();

  // Reset the status to INIT in case there were any problems in the
  // previous steps
  UBX_Status = UBX_STATUS_INIT;

  // Configure the navigation engine
  ConfigureNavEngine();

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_NAV5;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Configure the navigation engine (expert)
  ConfigureNavEngineExpert();

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_NAVX5;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Configure the navigation rate to 4 Hz
  ConfigureMeasurementRate();

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_RATE;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Configure the POSLLH message for 1Hz
  ConfigureMessage(CL_NAV, ID_POSLLH, 4);

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_POS;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Configure the SAT message for 1Hz
  ConfigureMessage(CL_NAV, ID_SAT, 4);

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_SAT;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Configure the SOL message for 4Hz
  ConfigureMessage(CL_NAV, ID_SOL, 1);

  if (UBX_Status == UBX_STATUS_ERROR)
  {
    ErrorCode |= ERROR_BIT_SOL;
    UBX_Status = UBX_STATUS_INIT;
  }

  // Turn the GNSS on so the engine begins transmitting messages
  Ublox_GNSS_Start();

  // Make sure the first SOL message has arrived
  if (!ErrorCode)
  {
    while (!UBX_SOL_Buffer.End){ Ublox_Handler(); }
    // Notify the user we have successfully configured the engine
    Mfg_TransmitMessage("Ublox CAM-M8Q configured successfully.\r\n");
  }
  else
  {
    UBX_Status = UBX_STATUS_OFFLINE;
    string[0] = 0;
    strcat(string, "Ublox CAM-M8Q initialization error. Code : 0x");
    sprintHex(variable, ErrorCode, 2);
    strcat(string, variable);
    strcat(string, "\r\n");

    Mfg_TransmitMessage(string);
  }
} // Ublox_Init

// **************************************************************************
//
//  FUNCTION  : Ublox_WatchdogReset
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Sends the command to perform a watchdog reset of the GPS
//              engine. This is currently set to do a cold start. Once the
//              reset has completed, the engine will be set to 9600 baud with
//              several NMEA messages coming from the engine. These can all
//              be disabled by using the Ublox_GNSS_Disable() routine.
//
//  UPDATED   : 2016-12-10 JHM
//
// **************************************************************************
void Ublox_WatchdogReset(void)
{
  uint8_t Message[12];

  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_RST;

  // Payload Length (Little Endian) = 4 bytes
  Message[4] = 0x04;
  Message[5] = 0x00;

  // Payload
  Message[6] = 0xFF;  // Hot Start 1
  Message[7] = 0x87;  // Hot Start 2
  Message[8] = 0x00;  // Hardware Reset
  Message[9] = 0x00;  // Reserved

  //Checksum
  Message[10] = 0x94;
  Message[11] = 0xF5;

  // Send Reset Command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for the command to be sent
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  // Wait for the engine to reset
  DelayMs(100);
} // Ublox_WatchdogReset

// **************************************************************************
//
//  FUNCTION  : Ublox_GNSS_Start
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine send the command to start the GNSS engine. It is
//              similar to the watchdog reset, except the configuration is
//              maintained. I currently have this set to hotstart.
//
//  UPDATED   : 2016-12-10 JHM
//
// **************************************************************************
void Ublox_GNSS_Start(void)
{
  uint8_t Message[12];

  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_RST;

  // Payload Length (Little Endian) = 4 bytes
  Message[4] = 0x04;
  Message[5] = 0x00;

  // Payload
  Message[6] = 0x00;  // Hot Start 1
  Message[7] = 0x00;  // Hot Start 2
  Message[8] = 0x09;  // Hardware Reset
  Message[9] = 0x00;  // Reserved

  //Checksum
  Message[10] = 0x17;
  Message[11] = 0x76;

  // Send Reset Command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for the command to be sent
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  // Wait for the engine to reset
  DelayMs(100);
} // Ublox_GNSS_Start

// **************************************************************************
//
//  FUNCTION  : Ublox_GNSS_Stop
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine send the command to stop the GNSS engine.This
//              will stop all communication from the engine. This allows the
//              engine to be configured without all the unnecessary traffic.
//
//  UPDATED   : 2016-12-10 JHM
//
// **************************************************************************
void Ublox_GNSS_Stop(void)
{
  uint8_t Message[12];

  // Header info
  Message[0] = UBX_HEADER1;
  Message[1] = UBX_HEADER2;

  // Message ID (start checksum here)
  Message[2] = CL_CFG;
  Message[3] = ID_RST;

  // Payload Length (Little Endian) = 4 bytes
  Message[4] = 0x04;
  Message[5] = 0x00;

  // Payload
  Message[6] = 0xFF;  // Hot Start 1
  Message[7] = 0x87;  // Hot Start 2
  Message[8] = 0x08;  // Hardware Reset
  Message[9] = 0x00;  // Reserved

  //Checksum
  Message[10] = 0x9C;
  Message[11] = 0x05;

  // Send Reset Command
  Ublox_Transmit(Message, sizeof(Message));

  // Wait for the command to be sent
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  // Wait for the engine to reset
  DelayMs(100);
} // Ublox_GNSS_Stop

// **************************************************************************
//
//  FUNCTION  : Ublox_TxHandler
//
//  I/P       : sUBX_TX* Buffer = Pointer to the GPS UART data buffer
//
//  O/P       : None.
//
//  OPERATION : This routine handles the transmitter empty (TXE) interrupt
//              for the GPS UART.
//
//  UPDATED   : 2016-04-04 JHM
//
// **************************************************************************
void Ublox_TxHandler(sUBX_TX* Buffer)
{
  // Send the data
  USART_SendData(UBX_USART, Buffer->Buffer[Buffer->Start]);

  // Wait for the transmission to end
  while (USART_GetFlagStatus(UBX_USART, USART_FLAG_TXE == RESET));

  // Increment the start index (safe)
  Buffer->Start = (Buffer->Start + 1) % UBX_TX_SIZE;

  // End the transmission if the start and end index are the same
  if (Buffer->Start == Buffer->End)
  {
    // Disable the interrupt
    USART_ITConfig(UBX_USART, USART_IT_TXE, DISABLE);
  }
} // Ublox_TxHandler

// **************************************************************************
//
//  FUNCTION  : Ublox_RxHandler
//
//  I/P       : sUBX_RX* Buffer = Pointer to the GPS UART data buffer
//
//  O/P       : None.
//
//  OPERATION : This routine handles the RXNE (receiver not empty) interrupt
//              on the Ublox USART port.  It moves the received data into the
//              buffer.
//
//  UPDATED   : 2016-04-04 JHM
//
// **************************************************************************
void Ublox_RxHandler(sUBX_RX* Buffer)
{
  // Move the received data into the buffer
  Buffer->Buffer[Buffer->End] = USART_ReceiveData(UBX_USART);

  // Handle the byte
  MessageHandler(&UBX_MSG_Buffer.Message[UBX_MSG_Buffer.End], Buffer->Buffer[Buffer->End]);

  // Increment the message index if the message has completed
  if (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.End].State == MSG_ST_READY)
  {
    UBX_MSG_Buffer.End = (UBX_MSG_Buffer.End + 1) % UBX_MSG_BUF_SIZE;
    UBX_MSG_Buffer.Message[UBX_MSG_Buffer.End].State = MSG_ST_HEADER1;
  }

  // Increment the end index (safe)
  //Buffer->End = (Buffer->End + 1) % UBX_RX_SIZE;
  Buffer->End++;
  if (UBX_RX_SIZE == Buffer->End)
  {
    Buffer->End = 0;
  }
} // Ublox_RxHandler

// **************************************************************************
//
//  FUNCTION  : Ublox_Transmit
//
//  I/P       : uint8_t* Data = Pointer to byte array
//              uint16_t Length = Length of the array
//
//  O/P       : None.
//
//  OPERATION : Moves a block of bytes of size Length into the transmission
//              queue to be sent.
//
//  UPDATED   : 2016-04-04 JHM
//
// **************************************************************************
void Ublox_Transmit(uint8_t* Data, uint16_t Length)
{
  uint16_t i;

  // If necessary, wait for a previous transmission to end
  while (GPS_TX_Buffer.Start != GPS_TX_Buffer.End);

  for (i = 0; i < Length; i++)
  {
    // Move the data to the buffer
    GPS_TX_Buffer.Buffer[GPS_TX_Buffer.End] = *Data;
    // Increment the pointer
    Data++;
    // Increment the buffer
    //GPS_TX_Buffer.End = (GPS_TX_Buffer.End + 1) % UBX_TX_SIZE;
    GPS_TX_Buffer.End++;
    if (UBX_TX_SIZE == GPS_TX_Buffer.End)
    {
      GPS_TX_Buffer.End = 0;
    }
  } // for

  // Enable the interrupt
  USART_ITConfig(UBX_USART, USART_IT_TXE, ENABLE);
} // Ublox_Transmit

// **************************************************************************
//
//  FUNCTION  : Ublox_SendByte
//
//  I/P       : uint8_t Byte = byte to send
//
//  O/P       : None.
//
//  OPERATION : Transmits a single byte out the Ublox USART port.
//
//  UPDATED   : 2016-05-12 JHM
//
// **************************************************************************
void Ublox_SendByte(uint8_t Byte)
{
  // Move the byte into the tx buffer
  GPS_TX_Buffer.Buffer[GPS_TX_Buffer.End] = Byte;
  // Increment the buffer
  //GPS_TX_Buffer.End = (GPS_TX_Buffer.End + 1) % UBX_TX_SIZE;
  GPS_TX_Buffer.End++;
  if (UBX_TX_SIZE == GPS_TX_Buffer.End)
  {
    GPS_TX_Buffer.End = 0;
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(UBX_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(UBX_USART, USART_IT_TXE, ENABLE);
  }
} // Ublox_SendByte

// **************************************************************************
//
//  FUNCTION  : Ublox_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Checks if a new Ublox message is available. If it is, it
//              decodes the message and places it in the appropriate NAV_SOL
//              or NAV_SAT structure.
//
//  UPDATED   : 2016-04-06 JHM
//
// **************************************************************************
void Ublox_Handler(void)
{
  // Process the message if there is one available
  if (UBX_MSG_Buffer.Start != UBX_MSG_Buffer.End)
  {
    if (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].Class == CL_NAV)
    {
      switch (UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start].ID)
      {
        case ID_SAT:
          // Decode the SAT message
          DecodeSAT(&UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.End], &UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start]);
          // Increment the SOL buffer index
          UBX_SAT_Buffer.Start = UBX_SAT_Buffer.End;
          UBX_SAT_Buffer.End = (UBX_SAT_Buffer.End + 1) % UBX_SAT_BUF_SIZE;
          // Set the SOL ready flag
          Flag_SAT_Ready = SET;
          break;

        case ID_SOL:
          // Decode the SOL message
          DecodeSOL(&UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.End], &UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start]);
          // Check GPS validity
          Flag_GPS_Valid = ValidateSOL(&UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.End]);
          // Increment the SOL buffer index
          UBX_SOL_Buffer.End = (UBX_SOL_Buffer.End + 1) % UBX_SOL_BUF_SIZE;
          // Set the SOL ready flag
          Flag_SOL_Ready = SET;
          break;

        case ID_POSLLH:
          // Decode the POSLLH message
          DecodePOSLLH(&UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.End], &UBX_MSG_Buffer.Message[UBX_MSG_Buffer.Start]);
          // Increment the POSLLH buffer index
          UBX_POSLLH_Buffer.End = (UBX_POSLLH_Buffer.End + 1) % UBX_POSLLH_BUF_SIZE;
          // Set the POSLLH ready flag
          Flag_POSLLH_Ready = SET;
          break;

        default:
          break;
      } // switch
    } // if

    // Increment the message index (safe)
    UBX_MSG_Buffer.Start = (UBX_MSG_Buffer.Start + 1) % UBX_MSG_BUF_SIZE;
  } // if
} // Ublox_Handler
