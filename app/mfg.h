#ifndef __MFG_H
#define __MFG_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   comms.h
//
//      CONTENTS    :   Header file for communication using the manufacturing
//                      UART port
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************

// Serial Port Peripheral Configuration
#define MFG_USART          USART1
#define MFG_USART_RCC      RCC_APB2Periph_USART1
#define MFG_USART_IRQ      USART1_IRQn

#define MFG_BAUDRATE       9600

// Port IO Hardware Configuration
#define MFG_PORT           GPIOA
#define MFG_TX_PIN         GPIO_Pin_9
#define MFG_RX_PIN         GPIO_Pin_10
#define MFG_PORT_RCC       RCC_AHBPeriph_GPIOA
#define MFG_TX_PIN_SRC     GPIO_PinSource9
#define MFG_RX_PIN_SRC     GPIO_PinSource10
#define MFG_TX_AF          GPIO_AF_7
#define MFG_RX_AF          GPIO_AF_7

// Buffer Sizes
#define MFG_TX_BUF_SIZE    255
#define MFG_RX_BUF_SIZE    255

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t TxBuffer[MFG_TX_BUF_SIZE];
} sMFG_TX;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t RxBuffer[MFG_RX_BUF_SIZE];
  uint8_t MsgAvailable;
} sMFG_RX;

typedef enum
{
  ASCII_OFF = 0,
  ASCII_RSD = 1,
  ASCII_I4 = 2
} Type_ASCII_Mode;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sMFG_TX TX_Buffer;
extern sMFG_RX RX_Buffer;
extern FlagStatus Flag_GPS_Loopback;
extern Type_ASCII_Mode ASCII_Mode;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Mfg_Init(void);
void Mfg_ClearTxBuffer(sMFG_TX* Buffer);
void Mfg_ClearRxBuffer(sMFG_RX* Buffer);
void Mfg_Handler(sMFG_RX* Buffer);
void Mfg_TxHandler(sMFG_TX* Buffer);
void Mfg_RxHandler(sMFG_RX* Buffer);
void Mfg_TransmitMessage(char* Message);
void Mfg_SendByte(uint8_t Byte);
void Mfg_MessageHandler(void);
void Mfg_TransmitASCII(void);

#endif
