// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   CC115L.c
//
//      CONTENTS    :   Routines for communication and control of the Texas
//                      Instruments CC115L transmitter IC
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// Command Strobe Register Locations
#define  SRES                 0x30
#define  SFSTXON              0x31
#define  SXOFF                0x32
#define  SCAL                 0x33
#define  STX                  0x35
#define  SIDLE                0x36
#define  SPWD                 0x39
#define  SFTX                 0x3B
#define  SNOP                 0x3D

// Configuration Register Locations
#define  IOCFG2               0x00
#define  IOCFG1               0x01
#define  IOCFG0               0x02
#define  FIFOTHR              0x03
#define  SYNC1                0x04
#define  SYNC0                0x05
#define  PKTLEN               0x06
#define  PKTCTRL0             0x08
#define  CHANNR               0x0A
#define  FSCTRL0              0x0C
#define  FREQ2                0x0D
#define  FREQ1                0x0E
#define  FREQ0                0x0F
#define  MDMCFG4              0x10
#define  MDMCFG3              0x11
#define  MDMCFG2              0x12
#define  MDMCFG1              0x13
#define  MDMCFG0              0x14
#define  DEVIATN              0x15
#define  MCSM1                0x17
#define  MCSM0                0x18
#define  FREND0               0x22
#define  FSCAL3               0x23
#define  FSCAL2               0x24
#define  FSCAL1               0x25
#define  FSCAL0               0x26
#define  TEST2                0x2C
#define  TEST1                0x2D
#define  TEST0                0x2E

// Status Register Locations
#define  PARTNUM              0x30
#define  VERSION              0x31
#define  MARCSTATE            0x35
#define  PKTSTATUS            0x38
#define  TXBYTES              0x3A

// PA Table register
#define  PATABLE              0x3E

// Miscellaneous bits
#define  SPI_READ_BIT         0x80
#define  SPI_BURST_BIT        0x40
#define  CHIP_RDYn_BIT        0x80

// PA Table Powers
#define  PA_10DBM             0xC0
#define  PA_0DBM              0x60
#define  PA_M10DBM            0x34
#define  PA_M20DBM            0x0E

// Frequency Register Values
#define FREQ0_4020            0x27
#define FREQ1_4020            0x76
#define FREQ2_4020            0x0F

#define FREQ0_4025            0x13
#define FREQ1_4025            0x7B
#define FREQ2_4025            0x0F

#define FREQ0_4030            0x00
#define FREQ1_4030            0x80
#define FREQ2_4030            0x0F

#define FREQ0_4035            0xEC
#define FREQ1_4035            0x84
#define FREQ2_4035            0x0F

#define FREQ0_4040            0xD8
#define FREQ1_4040            0x89
#define FREQ2_4040            0x0F

#define FREQ0_4045            0xC4
#define FREQ1_4045            0x8E
#define FREQ2_4045            0x0F

#define FREQ0_4050            0xB1
#define FREQ1_4050            0x93
#define FREQ2_4050            0x0F

#define FREQ0_4055            0x9D
#define FREQ1_4055            0x98
#define FREQ2_4055            0x0F

#define FREQ0_4060            0x89
#define FREQ1_4060            0x9D
#define FREQ2_4060            0x0F

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sCC115L Transmitter;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitRFPA(void);
static void InitSPI(void);
static uint8_t WriteRegister(uint8_t Address, uint8_t Data);
static uint8_t ReadRegister(uint8_t Address);
static uint8_t CommandStrobe(uint8_t Command);
static void ManualReset(void);
static void ConfigureRegisters(void);
static void GetState(sCC115L* ptrTx);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitRFPA
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the power amplifier
//
//  UPDATED   : 2016-05-03 JHM
//
// **************************************************************************
static void InitRFPA(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(RFPA_G8_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(RFPA_G16_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(RFPA_VPD_RCC, ENABLE);

  // Pin configuration
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  // Amplifier control pins
  GPIO_InitStructure.GPIO_Pin = RFPA_G8_PIN;
  GPIO_Init(RFPA_G8_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RFPA_G16_PIN;
  GPIO_Init(RFPA_G16_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = RFPA_VPD_PIN;
  GPIO_Init(RFPA_VPD_PORT, &GPIO_InitStructure);

  // Turn the amplifier off for initialization
  GPIO_ResetBits(RFPA_G8_PORT, RFPA_G8_PIN);
  GPIO_ResetBits(RFPA_G16_PORT, RFPA_G16_PIN);
  GPIO_ResetBits(RFPA_VPD_PORT, RFPA_VPD_PIN);
} // InitRFPA

// **************************************************************************
//
//  FUNCTION  : InitSPI
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the SPI for the transmitter IC.
//
//  UPDATED   : 2016-05-02 JHM
//
// **************************************************************************
static void InitSPI(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  SPI_InitTypeDef SPI_InitStructure;

  // Enable SPI clock
  RCC_APB2PeriphClockCmd(CC115L_SPI_RCC, ENABLE);

  // Configure SPI Pins
  // Map pins
  GPIO_PinAFConfig(CC115L_SCK_PORT, CC115L_SCK_PIN_SRC, CC115L_SCK_AF);
  GPIO_PinAFConfig(CC115L_MOSI_PORT, CC115L_MOSI_PIN_SRC, CC115L_MOSI_AF);
  GPIO_PinAFConfig(CC115L_MISO_PORT, CC115L_MISO_PIN_SRC, CC115L_MISO_AF);

  // Configure port hardware
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_InitStructure.GPIO_Pin = CC115L_SCK_PIN;
  GPIO_Init(CC115L_SCK_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = CC115L_MOSI_PIN;
  GPIO_Init(CC115L_MOSI_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = CC115L_MISO_PIN;
  GPIO_Init(CC115L_MISO_PORT, &GPIO_InitStructure);

  // Reset SPI peripheral to default values
  SPI_Cmd(CC115L_SPI, DISABLE);
  SPI_I2S_DeInit(CC115L_SPI);

  // Configure the SPI
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  // Set the SPI clock to 48MHz / 256 = 187.5 kHz
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  // Initialize the data structure
  SPI_Init(CC115L_SPI, &SPI_InitStructure);

  // Set the FIFO threshold
  SPI_RxFIFOThresholdConfig(CC115L_SPI, SPI_RxFIFOThreshold_QF);

  // Enable the SPI interface
  SPI_Cmd(CC115L_SPI, ENABLE);
} // InitSPI

// **************************************************************************
//
//  FUNCTION  : WriteRegister
//
//  I/P       : uint8_t Address = 6-bit address
//              uint8_t Data = data byte to write
//
//  O/P       : uint8_t = status register value
//
//  OPERATION : Writes a byte of data to the 6-bit address using the SPI
//              interface and returns the status register value.
//
//  UPDATED   : 2016-04-18 JHM
//
// **************************************************************************
static uint8_t WriteRegister(uint8_t Address, uint8_t Data)
{
  uint8_t Header;
  uint8_t Status;

  // Move the address into the header packet
  Header = Address;

  // Clear read bit since we are writing
  Header &= ~SPI_READ_BIT;

  // Clear burst bit. We will only send 1 byte at a time
  Header &= ~SPI_BURST_BIT;

  // Set CSn low to enable the SPI
  GPIO_ResetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 6 microseconds, so we are ok

  // Send the header for the register
  SPI_SendData8(CC115L_SPI, Header);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(CC115L_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Clear the data from the register by reading
  SPI_ReceiveData8(CC115L_SPI);

  // Wait at least 55 ns per data sheet -
  // I measured this time as approximately 5 microseconds, so we are ok

  // Send the data to the register
  SPI_SendData8(CC115L_SPI, Data);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(CC115L_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Read the last value in the SPI receive buffer. This should have been sent
  // while the data was transmitting
  Status = SPI_ReceiveData8(CC115L_SPI);

  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 2.5 microseconds, so we are ok

  // Set CSn high to disable the SPI
  GPIO_SetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait 1 ms
  DelayMs(1);

  return Status;
} // WriteRegister

// **************************************************************************
//
//  FUNCTION  : ReadRegister
//
//  I/P       : uint8_t Address = 6-bit address
//
//  O/P       : uint8_t = register data
//
//  OPERATION : Reads a single byte of data from the specified address using
//              the SPI interface
//
//  UPDATED   : 2016-04-18 JHM
//
// **************************************************************************
static uint8_t ReadRegister(uint8_t Address)
{
  uint8_t Header;
  uint8_t RegValue;

  // Move the address into the header packet
  Header = Address;

  // Set the read bit
  Header |= SPI_READ_BIT;

  // Clear burst bit. We will only send 1 byte at a time
  Header &= ~SPI_BURST_BIT;

  // Set CSn low to enable the SPI
  GPIO_ResetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 6 microseconds, so we are ok

  // Send the header for the register
  SPI_SendData8(CC115L_SPI, Header);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(CC115L_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Receive the data (even though we wont use it)
  SPI_ReceiveData8(CC115L_SPI);

  // Wait at least 55 ns per data sheet
  // I measured this time as approximately 5 microseconds, so we are ok

  // Clear the receive flag so we can use it
  SPI_I2S_ClearFlag(CC115L_SPI, SPI_I2S_FLAG_RXNE);

  // Send null data since we are only reading
  SPI_SendData8(CC115L_SPI, 0x00);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(CC115L_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Read the last value in the SPI receive buffer. This should have been sent
  // while the data was transmitting
  RegValue = SPI_ReceiveData8(CC115L_SPI);

  // Wait at least 20 ns per data sheet
  // I measured this time as approximately 2.5 microseconds, so we are ok

  // Set CSn high to disable the SPI
  GPIO_SetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait 1 ms
  DelayMs(1);

  return RegValue;
}

// **************************************************************************
//
//  FUNCTION  : CommandStrobe
//
//  I/P       : SRES = Reset chip
//              SFSTXON = Enable and calibrate frequency synthesizer
//              SXOFF = Turn off crystal
//              SCAL = Calibrate frequency synthesizer and turn it off
//              STX = Enable TX
//              SIDLE = Enter IDLE state
//              SPWD = Enter power down mode when CSn goes high
//              SFTX = Flush the TX FIFO buffer
//              SNOP = No operation (for reading the chip status byte)
//
//  O/P       : uint8_t = status register value
//
//  OPERATION : Sends a command strobe via the SPI interface
//
//  UPDATED   : 2016-04-18 JHM
//
// **************************************************************************
static uint8_t CommandStrobe(uint8_t Command)
{
  uint8_t Header = 0;
  uint8_t Status = 0;

  // Clear read bit since we are writing
  Header &= ~SPI_READ_BIT;

  // Clear burst bit. We will only send 1 byte at a time
  Header &= ~SPI_BURST_BIT;

  // Move the address into the header packet
  Header |= Command;

  // Set CSn low to enable the SPI
  GPIO_ResetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait at least 20 ns per data sheet -
  // I measured this time as approximately 6 microseconds, so we are ok

  // Send the header for the register
  SPI_SendData8(CC115L_SPI, Header);

  // Wait for the transfer to complete
  while (SPI_I2S_GetFlagStatus(CC115L_SPI, SPI_I2S_FLAG_RXNE) == RESET);

  // Read the last value in the SPI receive buffer. This should have been sent
  // while the data was transmitting
  Status = SPI_ReceiveData8(CC115L_SPI);

  // Wait at least 20 ns per data sheet
  // I measured this time as approximately 2.5 microseconds, so we are ok

  // Set CSn high to disable the SPI
  GPIO_SetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Wait 1 ms
  DelayMs(1);

  return Status;
} // CommandStrobe

// **************************************************************************
//
//  FUNCTION  : ManualReset
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Performs the power on startup sequence described in
//              "5.10.1.2 Manual Reset" on page 28 of the data sheet. Upon
//              completion, the chip will be in the IDLE state with XOSC and
//              the voltage regulator switched ON
//
//  UPDATED   : 2016-05-03 JHM
//
// **************************************************************************
static void ManualReset(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable GPIO clocks
  RCC_AHBPeriphClockCmd(CC115L_SCK_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(CC115L_MOSI_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(CC115L_MISO_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(CC115L_CS_RCC, ENABLE);

  // Disable the SPI interface since we will have to use these lines
  SPI_Cmd(CC115L_SPI, DISABLE);
  SPI_I2S_DeInit(CC115L_SPI);

  // Set SCLK, SI, and CSn as outputs
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  // SCLK
  GPIO_InitStructure.GPIO_Pin = CC115L_SCK_PIN;
  GPIO_Init(CC115L_SCK_PORT, &GPIO_InitStructure);
  // MOSI
  GPIO_InitStructure.GPIO_Pin = CC115L_MOSI_PIN;
  GPIO_Init(CC115L_SCK_PORT, &GPIO_InitStructure);
  // CSn
  GPIO_InitStructure.GPIO_Pin = CC115L_CS_PIN;
  GPIO_Init(CC115L_CS_PORT, &GPIO_InitStructure);

  // Set MISO to input
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Pin = CC115L_MISO_PIN;
  GPIO_Init(CC115L_MISO_PORT, &GPIO_InitStructure);

  // Set outputs to initial state
  GPIO_SetBits(CC115L_SCK_PORT, CC115L_SCK_PIN);
  GPIO_ResetBits(CC115L_MOSI_PORT, CC115L_MOSI_PIN);
  GPIO_SetBits(CC115L_CS_PORT, CC115L_CS_PIN);

  // Delay 1 ms
  DelayMs(1);

  // Strobe CSn low to high
  GPIO_ResetBits(CC115L_CS_PORT, CC115L_CS_PIN);
  // Should I delay here?
  GPIO_SetBits(CC115L_CS_PORT, CC115L_CS_PIN);
  // Wait at least 40 us
  DelayMs(1);

  // Pull CSn low
  GPIO_ResetBits(CC115L_CS_PORT, CC115L_CS_PIN);
  // Wait for MISO to go low
  while (GPIO_ReadInputDataBit(CC115L_MISO_PORT, CC115L_MISO_PIN) == Bit_SET);

  // Reconfigure the pins for SPI
  InitSPI();

  // Issue the SRES strobe on the MOSI line
  CommandStrobe(SRES);

  // Wait for MISO to go low
  while (GPIO_ReadInputDataBit(CC115L_MISO_PORT, CC115L_MISO_PIN) == Bit_SET);

  // Update the state
  GetState(&Transmitter);
} // ManualReset

// **************************************************************************
//
//  FUNCTION  : ConfigureRegisters
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the configuration registers for the TASK
//              transmission scheme. The center frequency will be set to the
//              value in the global Transmitter data structure.
//
//  UPDATED   : 2016-06-22 JHM
//
// **************************************************************************
static void ConfigureRegisters(void)
{
  // This configures unused GDO2 as hi-Z
  WriteRegister(IOCFG2, 0x2E);

  // RF studio had me output the serial clock on GDO1. Is this necessary?
  WriteRegister(IOCFG1, 0x0B);

  // Set GDO0 to hi-Z, this is our data pin
  WriteRegister(IOCFG0, 0x2E);

  // RF studio had leave default value of 33 bytes
  WriteRegister(FIFOTHR, 0x47);

  // Leave sync 0 word as default
  WriteRegister(SYNC1, 0xD3);

  // Leave sync 1 word as default
  WriteRegister(SYNC0, 0x91);

  // Set packet length to maximum
  WriteRegister(PKTLEN, 0xFF);

  // Asynchronous serial mode, data in on GDO0
  // CRC calculation disabled
  // Infinite packet length
  WriteRegister(PKTCTRL0, 0x32);

  // Leave channel number as default
  WriteRegister(CHANNR, 0x00);

  // Leave synthesizer control as default
  WriteRegister(FSCTRL0, 0x00);

  // Configure the data rate for TASK or iMet-1
  if (ModulationMode == MODULATION_MODE_TASK)
  {
    // Configure modem for 4.09126 kBaud data rate exponent
    // Exponent symbol rate from RF studio
    WriteRegister(MDMCFG4, 0xF7);

    // Data rate mantissa from RF studio
    WriteRegister(MDMCFG3, 0x4A);
  }
  else if (ModulationMode == MODULATION_MODE_IMET1)
  {
    // Configure modem for 26.3824 kBaud data rate exponent. I selected this
    // value because it is a compromise data rate of 
    // 2200[SPACE] * 1200[MARK] = 2640000 
    // scaled down to 2640000 / 100 = 26400 Hz
    
    // Exponent symbol rate from RF studio
    WriteRegister(MDMCFG4, 0xFA);

    // Data rate mantissa from RF studio
    WriteRegister(MDMCFG3, 0x0A);
  }

  // Set to GFSK, manchester disabled, no preamble/sync
  WriteRegister(MDMCFG2, 0x10);

  // This deals with preamble, leave default values
  WriteRegister(MDMCFG1, 0x22);

  // Set channel spacing to 199.951172 kHz
  WriteRegister(MDMCFG0, 0xF8);

  // IDLE once a packet has been sent, this should not apply in async UART mode
  WriteRegister(MCSM1, 0x30);

  // Calibrate when going from IDLE to TX, timeout after XOSC start set to
  // 149-155 us, XOSC on in SLEEP state
  WriteRegister(MCSM0, 0x18);

  // Front end TX config from RF studio
  WriteRegister(FREND0, 0x10);

  // Frequency synthesizer calibration settings from RF studio
  WriteRegister(FSCAL3, 0xE9);
  WriteRegister(FSCAL2, 0x2A);
  WriteRegister(FSCAL1, 0x00);
  WriteRegister(FSCAL0, 0x1F);

  // Set the power to 0 dBm
  WriteRegister(PATABLE, PA_0DBM);

  // Let us read the value and make sure
  ReadRegister(PATABLE);
} // ConfigureRegisters

// **************************************************************************
//
//  FUNCTION  : GetState
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//
//  O/P       : None.
//
//  OPERATION : This routine strobes the NOP command and uses the status
//              register determine the state of the IC. Once the state is
//              determined it is immediately loaded into the data structure
//              input by the user.
//
//  UPDATED   : 2016-05-16 JHM
//
// **************************************************************************
static void GetState(sCC115L* ptrTx)
{
  uint8_t Status;

  // Strobe the NOP command to get the status
  Status = CommandStrobe(SNOP);

  // Red the CHIP_RDYn bit. If this bit is low, the chip is online and the
  // crystal has stabilized. If this bit is high, there is a hardware problem
  // with the transmitter IC
  if (Status & CHIP_RDYn_BIT)
  {
    ptrTx->Flag_Online = RESET;
  }
  else
  {
    ptrTx->Flag_Online = SET;
  }

  // Shift and mask to the the 3 bits to the LSB
  Status = (Status >> 4);

  ptrTx->State = (TypeDef_CC115L_State)Status;
} // GetState

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CC115L_Init
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the transmitter data structure and hardware.
//
//  UPDATED   : 2016-04-15 JHM
//
// **************************************************************************
void CC115L_Init(sCC115L* ptrTx)
{
  // Initialize some of the structure values
  ptrTx->Flag_Online = RESET;

  // Initialize the RFPA GPIO
  InitRFPA();

  // Reset the transmitter to a known state
  ManualReset();

  // Exit initialization if the manual reset has failed. Chip is offline.
  if (ptrTx->Flag_Online == RESET)
  {
    Mfg_TransmitMessage("CC115L Transmitter offline.\r\n");
    CC115L_SetAutoLevel(ptrTx, RESET);
    CC115L_SetPowerAmpLevel(ptrTx, RFPA_PWR_OFF);
    return;
  }

  // Configure the transmitter default settings
  ConfigureRegisters();

  // Set the configurable information
  CC115L_SetFrequency(ptrTx, FrequencyChannels.Frequencies[FrequencyChannels.Channel]);
  CC115L_SetDeviation(ptrTx, (uint8_t)iQ_Config.Deviation);
  CC115L_SetAutoLevel(ptrTx, (FlagStatus)iQ_Config.Flag_AutoPowerLevel);
  CC115L_SetPowerAmpLevel(ptrTx, (TypeDef_RFPA_PowerLevel)iQ_Config.PowerLevel);

  // Turn the transmitter on
  CC115L_SetState(ptrTx, ENABLE);

  // Notify the user
  Mfg_TransmitMessage("CC115L Transmitter online and enabled.\r\n");
  GetState(ptrTx);
} // CC115L_Init

// **************************************************************************
//
//  FUNCTION  : CC115L_SetState
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//              TypeDef_CC115L_State = The new state
//
//  O/P       : None.
//
//  OPERATION : Sets the programmable amplifier power level using the IO pins.
//
//  UPDATED   : 2016-05-13 JHM
//
// **************************************************************************
void CC115L_SetState(sCC115L* ptrTx, FunctionalState NewState)
{
  if (NewState == ENABLE)
  {
    CommandStrobe(STX);
  }
  else
  {
    CommandStrobe(SIDLE);
  }
} //CC115L_SetState

// **************************************************************************
//
//  FUNCTION  : CC115L_SetPowerAmpLevel
//
//  I/P       :   sCC115L* = Pointer to CC115L data structure
//                TypeDef_RFPA_PowerLevel :
//                  RFPA_PWR_OFF = IC and RFPA off
//                  RFPA_PWR_1 = ~5 dBm
//                  RFPA_PWR_2 = ~16 dBm
//                  RFPA_PWR_3 = ~23 dBm
//                  RFPA_PWR_4 = ~28 dBm
//
//  O/P       : None.
//
//  OPERATION : Sets the programmable amplifier power level using the IO pins.
//
//  UPDATED   : 2016-05-13 JHM
//
// **************************************************************************
void CC115L_SetPowerAmpLevel(sCC115L* ptrTx, TypeDef_RFPA_PowerLevel PowerLevel)
{
  if (PowerLevel == RFPA_PWR_OFF)
  {
    // Drive all the RFPA bits low
    GPIO_ResetBits(RFPA_VPD_PORT, RFPA_VPD_PIN);
    GPIO_ResetBits(RFPA_G8_PORT, RFPA_G8_PIN);
    GPIO_ResetBits(RFPA_G16_PORT, RFPA_G16_PIN);
  } // if
  else
  {
    // Enable the RFPA
    GPIO_SetBits(RFPA_VPD_PORT, RFPA_VPD_PIN);

    switch (PowerLevel)
    {
      case RFPA_PWR_1:
        GPIO_ResetBits(RFPA_G8_PORT, RFPA_G8_PIN);
        GPIO_ResetBits(RFPA_G16_PORT, RFPA_G16_PIN);
        break;

      case RFPA_PWR_2:
        GPIO_SetBits(RFPA_G8_PORT, RFPA_G8_PIN);
        GPIO_ResetBits(RFPA_G16_PORT, RFPA_G16_PIN);
        break;

      case RFPA_PWR_3:
        GPIO_ResetBits(RFPA_G8_PORT, RFPA_G8_PIN);
        GPIO_SetBits(RFPA_G16_PORT, RFPA_G16_PIN);
        break;

      case RFPA_PWR_4:
        GPIO_SetBits(RFPA_G8_PORT, RFPA_G8_PIN);
        GPIO_SetBits(RFPA_G16_PORT, RFPA_G16_PIN);
        break;

      default:
        break;
    } // switch
  } // else

  // Update the data structure
  ptrTx->PowerLevel = PowerLevel;
} // CC115L_SetPowerAmpLevel

// **************************************************************************
//
//  FUNCTION  : CC115L_SetAutoLevel
//
//  I/P       :   sCC115L* = Pointer to CC115L data structure
//                FlagStatus AutoLevelOn
//                      SET = The power level is automatically controlled by
//                            flight status
//                      RESET = The power level is forced to the current
//                              power level.
//
//  O/P       : None.
//
//  OPERATION : Turn the automatic power level function on or off.
//
//  UPDATED   : 2016-10-14 JHM
//
// **************************************************************************
void CC115L_SetAutoLevel(sCC115L* ptrTx, FlagStatus AutoLevelOn)
{
  if (AutoLevelOn)
  {
    ptrTx->Flag_AutoPowerLevel = SET;
  }
  else
  {
    ptrTx->Flag_AutoPowerLevel = RESET;
  }
} // CC115L_SetAutoLevel

// **************************************************************************
//
//  FUNCTION  : CC115L_SetFrequency
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//              uint16_t Frequency = 4020 to 4060 in tenths of MegaHertz
//
//  O/P       : None.
//
//  OPERATION : Sets the transmitter to transmit at the frequency specified.
//              In order for this method to function properly, the transmitter
//              must be in the IDLE state. Use CC115L_SetStatoe() prior to
//              using this function.
//
//  UPDATED   : 2016-10-14 JHM
//
// **************************************************************************
void CC115L_SetFrequency(sCC115L* ptrTx, uint16_t Frequency)
{
  uint32_t FrequencyRegister;
  uint8_t Freq0Val;
  uint8_t Freq1Val;
  uint8_t Freq2Val;
  TypeDef_CC115L_State CurrentState;

  // Check for good frequencies so we don't screw up the transmitter IC
  if ((Frequency < 4000) || (Frequency > 4100))
  {
    return;
  }

  // Use the equation from p.49 of the CC115L datasheet. Solving for the carrier
  // frequency, the equation is:
  //            N = fcarrier * 2^16 / fosc
  // where fosc = 260 [MHz/10]
  // *Note that multiplying by 2^16 is bitshift left 16 operation
  FrequencyRegister = Frequency << 16;
  FrequencyRegister /= 260;

  // I measure the bias to be approximately +0.0075 MHz, so calculate the
  // shift as:
  //               N = 0.00750 MHz * 2^16 / 26.0 MHz = 18.90
  if (iQ_Config.ModulationMode == MODULATION_MODE_IMET1)
  {
    FrequencyRegister -= 32;
  }
  else if (iQ_Config.ModulationMode == MODULATION_MODE_TASK)
  {
    FrequencyRegister -= 19;
  }


  Freq2Val = (uint8_t)(FrequencyRegister >> 16);
  Freq1Val = (uint8_t)((FrequencyRegister >> 8) & 0xFF);
  Freq0Val = (uint8_t)(FrequencyRegister & 0xFF);

  // Check the state. Once we have made the frequency register changes, we
  // will want to preserve this state
  GetState(ptrTx);
  CurrentState = ptrTx->State;

  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, DISABLE);
  }

  // Write the values to the registers
  WriteRegister(FREQ0, Freq0Val);
  WriteRegister(FREQ1, Freq1Val);
  WriteRegister(FREQ2, Freq2Val);

  // Turn the transmitter back on if it was on when this routine was called
  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, ENABLE);
  }
} // CC115L_SetFrequency

// **************************************************************************
//
//  FUNCTION  : CC115L_SetDeviation
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//              uint32_t Hertz = the deviation value in Hertz
//
//  O/P       : None.
//
//  OPERATION : Sets the transmitter deviation to the value specified.
//              In order for this method to function properly, the transmitter
//              must be in the IDLE state. Use CC115L_SetState().
//
//  UPDATED   : 2016-09-09 JHM
//
// **************************************************************************
void CC115L_SetDeviation(sCC115L* ptrTx, uint8_t kiloHertz)
{
  uint8_t Exponent = 0;
  uint8_t Mantissa = 0;
  uint8_t RegValue = 0;
  uint32_t Deviation = 0;
  uint32_t Hertz;
  TypeDef_CC115L_State CurrentState;

  // Scale up the units
  Hertz = kiloHertz * 100000;

  for (Exponent = 0; Exponent < 8; Exponent++)
  {
    for (Mantissa = 0; Mantissa < 8; Mantissa++)
    {
      // From the datasheet:
      // fdev = fxosc / 2^17 * (8 + DEVIATION_M) * 2^DEVIATION_E
      // fxosc = 26.0 MHz
      // fxosc / 2^17 = 198.3643 ---> scale this up 2 decimal places
      Deviation = 19836 * (8 + Mantissa);
      Deviation = Deviation << Exponent;

      if (Deviation > Hertz)
      {
        goto Register;
      }
    }
  }

Register:
  RegValue = (Exponent << 4);
  RegValue |= Mantissa;

  // Check the state. Once we have made the frequency register changes, we
  // will want to preserve this state
  GetState(ptrTx);
  CurrentState = ptrTx->State;

  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, DISABLE);
  }

  // Set the new register values
  WriteRegister(DEVIATN, RegValue);

  // Turn the transmitter back on if it was on when this routine was called
  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, ENABLE);
  }
} // CC115L_SetDeviation

// **************************************************************************
//
//  FUNCTION  : CC115L_SetModulationFormat
//
//  I/P       : sCC115L* = Pointer to CC115L data structure
//              TypeDef_CC115L_ModulationFormat = Modulation Format
//
//  O/P       : None.
//
//  OPERATION : Sets the transmitter modulation format to the value specified.
//              In order for this method to function properly, the transmitter
//              must be in the IDLE state. Use CC115L_SetState().
//
//  UPDATED   : 2017-04-06 JHM
//
// **************************************************************************
void CC115L_SetModulationFormat(sCC115L* ptrTx, TypeDef_CC115L_ModulationFormat ModulationFormat)
{
  TypeDef_CC115L_State CurrentState;
  uint8_t RegValue;

  // Check the state. Once we have made the frequency register changes, we
  // will want to preserve this state
  GetState(ptrTx);
  CurrentState = ptrTx->State;

  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, DISABLE);
  }

  // Get the current register settings
  RegValue = ReadRegister(MDMCFG2);
  // Clear the bytes that set the format
  RegValue = RegValue & (~0b01110000);
  // Set the appropriate bits
  RegValue = RegValue | (ModulationFormat << 4);

  // Set the new register values
  WriteRegister(MDMCFG2, RegValue);

  if (ModulationFormat == CC115L_MF_OOK)
  {
    RegValue = 0x11;
  }
  else
  {
    RegValue = 0x10;
  }

  // Write the PATABLE register
  WriteRegister(FREND0, RegValue);

  // Turn the transmitter back on if it was on when this routine was called
  if (CurrentState == CC115L_ST_TX)
  {
    CC115L_SetState(ptrTx, ENABLE);
  }
} // CC115L_SetModulationFormat
