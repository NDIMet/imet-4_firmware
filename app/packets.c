// **************************************************************************
//
//      International Met Systems
//
//      iMet-1 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   packets.c
//
//      CONTENTS    :   Routines for initialization of the binary data
//                      packets.
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
//ping pong pang pung buffer for transmitting the data
uTASK_Packet TXmsgOut[4];

uint8_t *TXmsgPointer = (uint8_t*)TXmsgOut[0].Byte;
uint32_t TXmsgByteNumber = 0;
uint32_t TXmsgNumber = 0;
uint32_t packetMessageCounter = 0;

uint8_t iMet1_PTU_Packet[IMET1_PTU_SIZE];
uint8_t iMet1_PTUX_Packet[IMET1_PTUX_SIZE];
uint8_t iMet1_GPS_Packet[IMET1_GPS_SIZE];
uint16_t iMet1_PacketNumber = 0;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitTASK(sTASK_Packet* Packet);
static uint16_t crc16_msb(uint8_t *buf, uint32_t len);
static uint8_t CalculateBattery(uint16_t milliVolts);
static uint16_t Flip16(uint16_t Number);
static uint32_t Flip32(uint32_t Number);
//static void Build_iMet1_PTU(uint8_t* Bytes);
static void Build_iMet1_PTUX(uint8_t* Bytes);
static void Build_iMet1_GPS(uint8_t* Bytes);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitTASK
//
//  I/P       : sTASK_Packet* Packet - Pointer to TASK packet data structure.
//
//  O/P       : None.
//
//  OPERATION : Initializes the TASK packet values.      
//
//  UPDATED   : 2015-10-14 JHM
//
// **************************************************************************
static void InitTASK(sTASK_Packet* Packet)
{
  Packet-> SyncWord = Flip32(0x0F5A36C9);
  Packet-> PacketNumber = 0;
  Packet-> Pressure = 0;
  Packet-> Temperature = 0;
  Packet-> Humidity = 0;

  Packet-> Time = 0;
  Packet-> XPosition = 0;
  Packet-> YPosition = 0;
  Packet-> ZPosition = 0;
  Packet-> XVelocity = 0;
  Packet-> YVelocity = 0;
  Packet-> ZVelocity = 0;
  Packet-> Checksum = 0;
} // InitTASK

// **************************************************************************
//
//  FUNCTION  : crc16_msb
//
//  I/P       : 
//
//  O/P       : 
//
//  OPERATION :       
//
//  UPDATED   : 2015-10-14 JHM
//
// **************************************************************************
static uint16_t crc16_msb(uint8_t *buf, uint32_t len)
{
   uint32_t i;
   uint16_t data, crc;

   crc = 0xFFFF;
   if (len == 0)
       return 0;
   do
   {
     data = (uint16_t)0x00FF & *buf++;
     crc = crc ^ (data<<8);
     for (i = 8; i > 0; i--)
     {
       if (crc & 0x8000)
       {
         crc = (crc << 1) ^ 0x1021;
       }
       else
       {
         crc <<= 1;
       }
     } // for
   }
   while (--len);
   crc ^= 0xFFFF;
   return (crc);
}// crc16_msb

// **************************************************************************
//
//  FUNCTION  : CalculateBattery
//
//  I/P       : uint16_t milliVolts = Battery voltage in millivolts
//
//  O/P       : uint8_t = Byte representing N for 8-bit ADC. This is what is
//                        sent down via transmitter.
//
//  OPERATION : Calculates the byte value representing N to be sent down. This
//              is based upon the equation
//                             V = (N/255)*6
//              from the QinetiQ radiosonde ICD.
//
//  UPDATED   : 2016-05-31 JHM
//
// **************************************************************************
static uint8_t CalculateBattery(uint16_t milliVolts)
{
  uint32_t Value;

  Value = (uint32_t)milliVolts * 255;
  Value /= 6;
  Value /= 1000;

  if (Value > 255)
  {
    Value = 255;
  }

  return (uint8_t)Value;
} // CalculateBattery

// **************************************************************************
//
//  FUNCTION  : Flip16
//
//  I/P       : uint16_t Number = Number to swap
//
//  O/P       : uint16_t = Number after the bytes are swapped
//
//  OPERATION : Changes the endianess of a 2 byte number
//
//  UPDATED   : 2015-11-03 JHM
//
// **************************************************************************
static uint16_t Flip16(uint16_t Number)
{
  uint16_t Value;
  Value = Number >> 8;
  Value |= (Number << 8);
  return Value;
} // Flip16

// **************************************************************************
//
//  FUNCTION  : Flip32
//
//  I/P       : uint32_t Number = Number to swap
//
//  O/P       : uint32_t = Number after the bytes are swapped
//
//  OPERATION : Changes the endianess of a 4 byte number
//
//  UPDATED   : 2015-11-03 JHM
//
// **************************************************************************
static uint32_t Flip32(uint32_t Number)
{
  uint32_t Value;
  Value = 0;

  Value = Number >> 24;
  Value |= (Number & 0x00FF0000) >> 8;
  Value |= (Number & 0x0000FF00) << 8;
  Value |= (Number & 0xFF) << 24;
  return Value;
} // Flip32

/*
// **************************************************************************
//
//  FUNCTION  : Build_iMet1_PTU
//
//  I/P       : int8_t* Bytes = Pointer to byte data array
//
//  O/P       : None.
//
//  OPERATION : Builds an iMet-1 PTU packet from the met data structures.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
static void Build_iMet1_PTU(uint8_t* Bytes)
{
  uint8_t* ptr;
  uint8_t* ptrStart;
  uint16_t Checksum;
  uint16_t Humidity;

  // Initialize the pointers
  ptrStart = Bytes;
  ptr = Bytes;

  // SOH
  *ptr = 0x01;
  ptr++;
  // Packet ID
  *ptr = 0x01;
  ptr++;
  // Packet number
  memcpy(ptr, &iMet1_PacketNumber, sizeof(uint16_t));
  ptr += sizeof(uint16_t);
  // Pressure
  memcpy(ptr, &PressureSensor.Buffer.AveragePressure, 3);
  ptr += 3;
  // Temperature
  memcpy(ptr, &Thermistor.TemperatureC, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Humidity
  Humidity = HumiditySensor.Humidity * 10;
  memcpy(ptr, &Humidity, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Battery Voltage
  *ptr = (Battery.milliVoltsBattery / 100);
  ptr++;
  // Initialize checksums
  *ptr = 0;
  ptr++;
  *ptr = 0;

  Checksum = calc_crc(ptrStart, 0, IMET1_PTU_SIZE, 0);

  ptr--;
  *ptr = (uint8_t)((Checksum >> 8) & 0xFF);
  ptr++;
  *ptr = (uint8_t)(Checksum & 0xFF);
} // Build_iMet1_PTU
*/

// **************************************************************************
//
//  FUNCTION  : Build_iMet1_PTUX
//
//  I/P       : int8_t* Bytes = Pointer to byte data array
//
//  O/P       : None.
//
//  OPERATION : Builds an iMet-1 PTUX packet from the met data structures.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
static void Build_iMet1_PTUX(uint8_t* Bytes)
{
  uint8_t* ptr;
  uint8_t* ptrStart;
  uint16_t Checksum;
  uint16_t Humidity;
  int16_t Temperature;

  // Initialize the pointers
  ptrStart = Bytes;
  ptr = Bytes;

  // SOH
  *ptr = 0x01;
  ptr++;
  // Packet ID
  *ptr = 0x04;
  ptr++;
  // Packet number
  memcpy(ptr, &iMet1_PacketNumber, sizeof(uint16_t));
  ptr += sizeof(uint16_t);
  // Pressure
  memcpy(ptr, &PressureSensor.CorrectedPressure, 3);
  ptr += 3;
  // Temperature
  memcpy(ptr, &Thermistor.TemperatureC, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Humidity
  Humidity = HumiditySensor.RawHumidity * 10;
  memcpy(ptr, &Humidity, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Battery Voltage
  *ptr = (Battery.milliVoltsBattery/ 100);
  ptr++;
  // Internal Temperature (same as pressure temp)
  Temperature = (int16_t)PressureSensor.SensorData.Temperature;
  memcpy(ptr, &Temperature, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Pressure Temperature (same as internal temp)
  memcpy(ptr, &Temperature, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Humidity Temperature
  memcpy(ptr, &HumiditySensor.CorrectedTemperature, sizeof(int16_t));
  ptr += sizeof(int16_t);

  // Initialize checksums
  *ptr = 0;
  ptr++;
  *ptr = 0;

  Checksum = calc_crc(ptrStart, 0, IMET1_PTUX_SIZE, 0);

  ptr--;
  *ptr = (uint8_t)((Checksum >> 8) & 0xFF);
  ptr++;
  *ptr = (uint8_t)(Checksum & 0xFF);
} // Build_iMet1_PTUX

// **************************************************************************
//
//  FUNCTION  : Build_iMet1_GPS
//
//  I/P       : int8_t* Bytes = Pointer to byte data array
//
//  O/P       : None.
//
//  OPERATION : Builds an iMet-1 GPS packet from the Ublox data structures.
//
//  UPDATED   : 2016-07-21 JHM
//
// **************************************************************************
static void Build_iMet1_GPS(uint8_t* Bytes)
{
  uint8_t* ptr;
  uint8_t* ptrStart;
  uint16_t Checksum;
  float fValue;
  int16_t Altitude;
  uint32_t Time;
  uint8_t hms;

  // Initialize the pointers
  ptrStart = Bytes;
  ptr = Bytes;

  // SOH
  *ptr = 0x01;
  ptr++;
  // Packet ID
  *ptr = 0x02;
  ptr++;
  // Latitude
  fValue = (float)(UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lat);
  fValue *= 1e-7;
  memcpy(ptr, &fValue, sizeof(float));
  ptr += sizeof(float);
  // Longitude
  fValue = (float)(UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lon);
  fValue *= 1e-7;
  memcpy(ptr, &fValue, sizeof(float));
  ptr += sizeof(float);
  // Altitude
  Altitude = (int16_t)(UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].hMSL / 1000);
  Altitude += 5000;
  memcpy(ptr, &Altitude, sizeof(int16_t));
  ptr += sizeof(int16_t);
  // Number of satellites
  *ptr = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].numSV;
  ptr++;
  // Calculate hh:mm:ss
  // Convert from milliseconds to seconds
  Time = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].iTOW / 1000;
  // Hours
  hms = (Time / 3600) % 24;
  *ptr = hms;
  ptr++;
  // Minutes
  hms = (Time / 60) % 60;
  *ptr = hms;
  ptr++;
  // Seconds
  hms = Time % 60;
  *ptr = hms;
  ptr++;

  // Initialize checksums
  *ptr = 0;
  ptr++;
  *ptr = 0;

  Checksum = calc_crc(ptrStart, 0, IMET1_GPS_SIZE, 0);

  ptr--;
  *ptr = (uint8_t)((Checksum >> 8) & 0xFF);
  ptr++;
  *ptr = (uint8_t)(Checksum & 0xFF);
} // Build_iMet1_GPS

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Packets_Init
//
//  I/P       : Type_PacketType Type
//                PACKET_BELL202,
//                PACKET_TASK
//
//  O/P       : None.
//
//  OPERATION : Initializes the packet structure based on the selected type.       
//
//  UPDATED   : 2015-10-14 JHM
//
// **************************************************************************
void Packets_Init(TypeDef_ModulationMode Mode)
{
  // Initialize local variables
  TXmsgPointer = (uint8_t*)TXmsgOut[0].Byte;
  TXmsgByteNumber = 0;
  TXmsgNumber = 0;
  packetMessageCounter = 0;

  // Initialize data structures
  InitTASK(&TXmsgOut[0].Packet);
  InitTASK(&TXmsgOut[0].Packet);
  InitTASK(&TXmsgOut[0].Packet);
  InitTASK(&TXmsgOut[0].Packet);
} // Packets_Init

// **************************************************************************
//
//  FUNCTION  : Packets_Build_TASK
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Loads the GPS information into the global Bell202 packet
//              data structure.
//
//  UPDATED   : 2015-10-16 JHM
//
// **************************************************************************
void Packets_Build_TASK(sUBX_NAV_SOL* ptrSOL)
{
  uint16_t crc;
  int i;
  int j;
  uTASK_Packet msgOut;

	msgOut.Packet.SyncWord = 0xC9365A0F; //SYNC WORD
  msgOut.Packet.PacketNumber = Flip16(packetMessageCounter);
  //msgOut.Packet.Pressure = Flip16((uint16_t)(PressureSensor.Buffer.AveragePressure / 10));
  msgOut.Packet.Pressure = Flip16((uint16_t)(PressureSensor.CorrectedPressure / 10));
  msgOut.Packet.Temperature = Flip16(Thermistor.TemperatureK);
  msgOut.Packet.Humidity = Flip16((uint16_t)(HumiditySensor.CorrectedHumidity * 10));

  if (packetMessageCounter % 4)
  {
    msgOut.Packet.Info[0] = 0;
    msgOut.Packet.Info[1] = 0;
    msgOut.Packet.Info[2] = (uint8_t)((iQ_Config.SerialNumber >> 8) & 0xFF);
    msgOut.Packet.Info[3] = (uint8_t)(iQ_Config.SerialNumber & 0xFF);

    // Canned values
    //msgOut.Packet.Info[0] = 0x40;
    //msgOut.Packet.Info[1] = 0x20;
    //msgOut.Packet.Info[2] = 0x01;
    //msgOut.Packet.Info[3] = 0x09;
  }
  else
  {
    msgOut.Packet.Info[0] = (uint8_t)(ptrSOL->numSV << 4);
    msgOut.Packet.Info[1] = (uint8_t)(ptrSOL->pDOP / 10);
    msgOut.Packet.Info[2] = (uint8_t)(ptrSOL->week >> 8);
    msgOut.Packet.Info[3] = (uint8_t)(ptrSOL->week & 0xFF);
  }
  
  j = (packetMessageCounter % 4) * 4;

  for (i = 0; i < 4; i++,j++)
  {
    msgOut.Packet.GPSSatellite[i].SVID = UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[j].svId;
    msgOut.Packet.GPSSatellite[i].Flags = (uint8_t)UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[j].Flag_TASK;
    msgOut.Packet.GPSSatellite[i].CNO = UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[j].cno;
    msgOut.Packet.GPSSatellite[i].EL =UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[j].elev;
    msgOut.Packet.GPSSatellite[i].AZ = (uint8_t)UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[j].azim;
  }

  // PCB temperature is the pressure sensor temperature(for now)
  msgOut.Packet.PCB_Temp = Flip16((int16_t)(PressureSensor.SensorData.Temperature / 10));
  // Built-In test bytes
  msgOut.Packet.Battery = CalculateBattery(Battery.milliVoltsBattery);
  msgOut.Packet.GPS_Fix = ptrSOL->gpsFix;

  msgOut.Packet.Time = Flip32(ptrSOL->iTOW);
  msgOut.Packet.XPosition = Flip32(ptrSOL->ecefX);
  msgOut.Packet.YPosition = Flip32(ptrSOL->ecefY);
  msgOut.Packet.ZPosition = Flip32(ptrSOL->ecefZ);
  msgOut.Packet.XVelocity = Flip16(ptrSOL->ecefVX);
  msgOut.Packet.YVelocity = Flip16(ptrSOL->ecefVY);
  msgOut.Packet.ZVelocity = Flip16(ptrSOL->ecefVZ);

  crc = crc16_msb(&msgOut.Byte[4], 58);
  msgOut.Packet.Checksum = Flip16(crc);

  memcpy(&TXmsgOut[packetMessageCounter % 4], &msgOut.Byte, TASK_SIZE);

  packetMessageCounter++;
} // Packets_Build_TASK

// **************************************************************************
//
//  FUNCTION  : Packets_Build_iMet1
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Builds an iMet-1 packet from the data located in the Ublox
//              data structure and met data structures.
//
//  UPDATED   : 2016-07-20 JHM
//
// **************************************************************************
void Packets_Build_iMet1(void)
{
  Build_iMet1_PTUX(iMet1_PTUX_Packet);
  Build_iMet1_GPS(iMet1_GPS_Packet);
} // Packets_Build_iMet1

