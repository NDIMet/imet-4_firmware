// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   user.c
//
//      CONTENTS    :   Pushbutton, power, and LED routines
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sPushButton PushButton;
sLEDS UserLEDs;
sFrequencyChannels FrequencyChannels;

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitLEDs(void);
static void SetLEDs(uint8_t Number);
static void InitPushbuttonInput(void);
static void InitPowerOnOutput(void);
static void InitUserTimer(void);
static void PushButtonHandler(void);
static void LEDHandler(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitLEDs
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LEDs
//
//  UPDATED   : 2017-06-21 JHM
//
// **************************************************************************
static void InitLEDs(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for each LED
  RCC_AHBPeriphClockCmd(LED1_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(LED2_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(LED3_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(LED4_RCC, ENABLE);

  // Configure the LED output pin(s)
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;

  GPIO_InitStructure.GPIO_Pin = LED1_PIN;
  GPIO_Init(LED1_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = LED2_PIN;
  GPIO_Init(LED2_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = LED3_PIN;
  GPIO_Init(LED3_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = LED4_PIN;
  GPIO_Init(LED4_PORT, &GPIO_InitStructure);

  // Turn off all the LEDS
  SetLEDs(LED_NONE);
} // InitLEDs

// **************************************************************************
//
//  FUNCTION  : SetLEDs
//
//  I/P       : uint8_t Number = the number (in binary) of which LEDs to turn
//                               on
//                               0x00 = all off
//                               0x0F = all on
//
//  O/P       : None.
//
//  OPERATION : Turns on the LEDs indicated by Number.
//
//  UPDATED   : 2017-06-21 JHM
//
// **************************************************************************
static void SetLEDs(uint8_t Number)
{
  if (Number & 0x01)
  {
    GPIO_SetBits(LED1_PORT, LED1_PIN);
  }
  else
  {
    GPIO_ResetBits(LED1_PORT, LED1_PIN);
  }

  if (Number & 0x02)
  {
    GPIO_SetBits(LED2_PORT, LED2_PIN);
  }
  else
  {
    GPIO_ResetBits(LED2_PORT, LED2_PIN);
  }

  if (Number & 0x04)
  {
    GPIO_SetBits(LED3_PORT, LED3_PIN);
  }
  else
  {
    GPIO_ResetBits(LED3_PORT, LED3_PIN);
  }

  if (Number & 0x08)
  {
    GPIO_SetBits(LED4_PORT, LED4_PIN);
  }
  else
  {
    GPIO_ResetBits(LED4_PORT, LED4_PIN);
  }
} // SetLEDs

static void ToggleLEDs(uint8_t Number)
{
  if (Number & LED1)
  {
    LED1_PORT->ODR ^= LED1_PIN;
  }

  if (Number & LED2)
  {
    LED2_PORT->ODR ^= LED2_PIN;
  }

  if (Number & LED3)
  {
    LED3_PORT->ODR ^= LED3_PIN;
  }

  if (Number & LED4)
  {
    LED4_PORT->ODR ^= LED4_PIN;
  }
} // ToggleLEDs

// **************************************************************************
//
//  FUNCTION  : InitPushbuttonInput
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes pushbutton as a weak pull-up input
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
static void InitPushbuttonInput(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for each LED
  RCC_AHBPeriphClockCmd(BUTTON_RCC, ENABLE);

  // Configure the input pin and initialize
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_10MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = BUTTON_PIN;
  GPIO_Init(BUTTON_PORT, &GPIO_InitStructure);
} // InitPushbuttonInput

// **************************************************************************
//
//  FUNCTION  : InitPushbuttonInput
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the power on output port. This is the line that -
//              once pulled high - will keep the radiosonde turned on after
//              the user has released the button.
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
static void InitPowerOnOutput(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clock for each LED
  RCC_AHBPeriphClockCmd(POWER_RCC, ENABLE);

  // Configure the output pin and initialize
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = POWER_PIN;
  GPIO_Init(POWER_PORT, &GPIO_InitStructure);

  // Start with the line low
  GPIO_ResetBits(POWER_PORT, POWER_PIN);
} // InitPowerOnOutput

// **************************************************************************
//
//  FUNCTION  : InitUserTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the user timer. This must be a basic timer
//              TIM6/7/18
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
static void InitUserTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(USER_TIM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = USER_TIM_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Make this lowest priority
  //NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 15;
  NVIC_Init(&NVIC_InitStructure);

  // Set the prescaler value for 10 kHz
  Prescaler = (uint16_t)(SystemCoreClock / USER_TIM_FREQ) - 1;
  TIM_PrescalerConfig(USER_TIM, Prescaler, TIM_PSCReloadMode_Immediate);

  // Set the Auto-Reload value to 10 - 1, for 1 ms interrupt
  TIM_SetAutoreload(USER_TIM, 9);

  TIM_SetCounter(USER_TIM, 0);

  // Enable the Update (overflow) interrupt
  TIM_ITConfig(USER_TIM, TIM_IT_Update, ENABLE);

  // Start the timer
  TIM_Cmd(USER_TIM, ENABLE);
} // InitUserTimer

// **************************************************************************
//
//  FUNCTION  : PushButtonHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called by the interrupt each ms to
//              handle the state machine of the pushbutton.
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
static void PushButtonHandler(void)
{
  if (GPIO_ReadInputDataBit(BUTTON_PORT, BUTTON_PIN) == Bit_SET)
  {
    if (PushButton.Counter)
    {
      PushButton.Counter = 0;
    }
  }
  else
  {
    PushButton.Counter++;
  }

  switch (PushButton.State)
  {
    case PB_ST_BYPASSED:
      PushButton.State = PB_ST_WAIT;
      break;

    case PB_ST_ON_DETECT:
      if (PushButton.Counter >= PWR_ON_TICS)
      {
        // Turn the power line on
        GPIO_SetBits(POWER_PORT, POWER_PIN);

        // Turn on all the LEDs to signal start of initialization
        UserInterface_SetLED(&UserLEDs, LED_ALL, LED_ON);

        // Wait for the user to release the button
        PushButton.State = PB_ST_WAIT;
      }
      break;

    case PB_ST_WAIT:
      // Wait for the initialization to complete
      if (PushButton.Counter == 0)
      {
        // Advance the state machine
        PushButton.State = PB_ST_ON;

      }
      break;

    case PB_ST_ON:
      if (GPIO_ReadInputDataBit(BUTTON_PORT, BUTTON_PIN) == Bit_RESET)
      {
        // Light the LED
        SetLEDs(UserLEDs.LEDs_On);

        // Disable the interrupt
        UserInterface_EnableLED(&UserLEDs, DISABLE);

        if (PushButton.Counter >= FREQ_TICS)
        {
          // Increment the state machine
          PushButton.State = PB_ST_FREQ;
        }
      }
      else
      {
        UserInterface_EnableLED(&UserLEDs, ENABLE);
        PushButton.Counter = 0;
      }
      break;

    case PB_ST_FREQ:
      // Check if the user has released the button
      if (PushButton.Counter == 0)
      {
        // Disable the peripherals during change
        Peripherals_Disable();

        // Increment the tx channel
        FrequencyChannels.Channel = (FrequencyChannels.Channel + 1) % USER_CHANNELS;

        // Change frequency channel
        UserInterface_ChangeChannel(FrequencyChannels.Channel);

        // Enable the peripherals again
        Peripherals_Enable();

        // Button has been released, go back to on state
        PushButton.State = PB_ST_ON;
      }
      else if (PushButton.Counter >= PWR_OFF_TICS)
      {
        PushButton.State = PB_ST_OFF;
      }
      break;

    case PB_ST_OFF:
      // Turn the LEDs off
      GPIO_ResetBits(LED1_PORT, LED1_PIN);
      GPIO_ResetBits(LED2_PORT, LED2_PIN);
      GPIO_ResetBits(LED3_PORT, LED3_PIN);
      GPIO_ResetBits(LED4_PORT, LED4_PIN);
      // Disconnect the power line
      GPIO_ResetBits(POWER_PORT, POWER_PIN);
      // Wait for shutdown
      while (1);
      break;
  } // switch
} // PushButtonHandler

// **************************************************************************
//
//  FUNCTION  : LEDHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called by the interrupt each ms to
//              handle the state machine of the pushbutton.
//
//  UPDATED   : 2017-06-21 JHM
//
// **************************************************************************
static void LEDHandler(void)
{
  // Disable the LEDs if the radiosonde has been launched
  if (FlightStatus.Status == FLIGHT_ST_LAUNCHED)
  {
    SetLEDs(LED_OFF);
    UserInterface_EnableLED(&UserLEDs, DISABLE);
    return;
  }

  if (Flag_GPS_Valid)
  {
    UserLEDs.State = LED_ON;
  }
  else
  {
    UserLEDs.State = LED_BLINK;
  }

  switch (UserLEDs.State)
  {
  case LED_OFF:
    SetLEDs(LED_NONE);
    break;

  case LED_ON:
    SetLEDs(UserLEDs.LEDs_On);
    break;

  case LED_BLINK:
    if (UserLEDs.LED_Counter >= LED_BLINK_TICS)
    {
      // Toggle the LEDs
      ToggleLEDs(UserLEDs.LEDs_On);
      UserLEDs.LED_Counter = 0;
    }
    else
    {
      UserLEDs.LED_Counter++;
    }
    break;

  case LED_FAST_BLINK:
    if (UserLEDs.LED_Counter >= LED_FAST_TICS)
    {
      // Toggle the LEDs
      ToggleLEDs(UserLEDs.LEDs_On);
      UserLEDs.LED_Counter = 0;
    }
    else
    {
      UserLEDs.LED_Counter++;
    }
    break;
  }
} // LEDHandler

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : UserInterface_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : 
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
void UserInterface_Init(void)
{
  PushButton.State = PB_ST_ON_DETECT;
  PushButton.Counter = 0;

  // Initialize the frequency channels
  FrequencyChannels.Channel = 0;
  FrequencyChannels.Frequencies[0] = iQ_Config.TxChannel0;
  FrequencyChannels.Frequencies[1] = iQ_Config.TxChannel1;
  FrequencyChannels.Frequencies[2] = iQ_Config.TxChannel2;
  FrequencyChannels.Frequencies[3] = iQ_Config.TxChannel3;
  FrequencyChannels.Frequencies[4] = iQ_Config.TxChannel4;
  FrequencyChannels.Frequencies[5] = iQ_Config.TxChannel5;
  FrequencyChannels.Frequencies[6] = iQ_Config.TxChannel6;

  // Initialize the LED structure
  UserLEDs.LED_Counter = 0;
  UserLEDs.Flag_Enabled = RESET;

  // Initialize the LED GPIO
  InitLEDs();

  // Initialize the pushbutton
  InitPushbuttonInput();

  // Initialize the power output
  InitPowerOnOutput();

  // Initialize the user timer
  InitUserTimer();

  if (GPIO_ReadInputDataBit(BUTTON_PORT, BUTTON_PIN))
  {
    // Power was applied without battery through the manufacturing port, set
    // the state to bypass
    PushButton.State = PB_ST_BYPASSED;
  }
  else
  {
    PushButton.State = PB_ST_ON_DETECT;
    while (PushButton.State != PB_ST_WAIT);
  }

  // All LEDs on to signal initialization
  SetLEDs(LED_ALL);
} // UserInterface_Init

// **************************************************************************
//
//  FUNCTION  : UserInterface_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the pushbutton. This routine
//              is called each 1 ms when the timer tics.
//
//  UPDATED   : 2016-08-17 JHM
//
// **************************************************************************
void UserInterface_Handler(void)
{
  // Handle the pushbutton
  PushButtonHandler();
  // Handle the LED if it hasn't been disabled
  if (UserLEDs.Flag_Enabled)
  {
    LEDHandler();
  }
} // UserInterface_Handler

// **************************************************************************
//
//  FUNCTION  : UserInterface_EnableLED
//
//  I/P       : sLEDS* ptrLEDS = Pointer to LED data structure
//              Flagstatus Enabled =
//                             SET = Enabled
//                           RESET = Disabled
//
//  O/P       : None.
//
//  OPERATION : Enables or disables the automatic LED control
//
//  UPDATED   : 2017-07-31 JHM
//
// **************************************************************************
void UserInterface_EnableLED(sLEDS* ptrLEDS, FunctionalState NewState)
{
  if (NewState == ENABLE)
  {
    ptrLEDS->Flag_Enabled = SET;
  }
  else
  {
    ptrLEDS->Flag_Enabled = RESET;
  }
} // UserInterface_EnableLED

// **************************************************************************
//
//  FUNCTION  : UserInterface_SetLED
//
//  I/P       : sLEDS* ptrLEDS = Pointer to LED data structure
//              uint8_t LED_Number = The LEDs to turn on (0x0 to 0xF)
//              Type_LED_State State = LED_OFF, LED_ON, LED_BLINK
//
//  O/P       : None.
//
//  OPERATION : Allows the programmer to turn LEDS on and off and set the
//              blinking state.
//
//  UPDATED   : 2017-06-21 JHM
//
// **************************************************************************
void UserInterface_SetLED(sLEDS* ptrLEDS, uint8_t LED_Number, Type_LED_State State)
{
  // Turn off all the LEDs
  SetLEDs(LED_Number);

  // Reset the Counter
  UserLEDs.LED_Counter = 0;

  // Update the structure
  ptrLEDS->LEDs_On = LED_Number;
  ptrLEDS->State = State;
} // UserInterface_SetLED

// **************************************************************************
//
//  FUNCTION  : UserInterface_ChangeChannel
//
//  I/P       : uint8_t NewChannel
//
//  O/P       : None.
//
//  OPERATION : Updates the LED state based on the user frequency channel
//              configuration.
//
//  UPDATED   : 2017-06-21 JHM
//
// **************************************************************************
void UserInterface_ChangeChannel(uint8_t NewChannel)
{
  uint8_t NewLEDNumber = 0;

  // Update the structure value
  FrequencyChannels.Channel = NewChannel;

  // Set the new frequency value
  if (Transmitter.Flag_Online == SET)
  {
    // Set the new value
    CC115L_SetFrequency(&Transmitter, FrequencyChannels.Frequencies[FrequencyChannels.Channel]);
  }

  switch (FrequencyChannels.Channel)
  {
    case 0:
      NewLEDNumber = LED1;
      break;

    case 1:
      NewLEDNumber = LED1 + LED2;
      break;

    case 2:
      NewLEDNumber = LED2;
      break;

    case 3:
      NewLEDNumber = LED2 + LED3;
      break;

    case 4:
      NewLEDNumber = LED3;
      break;

    case 5:
      NewLEDNumber = LED3 + LED4;
      break;

    case 6:
      NewLEDNumber = LED4;
      break;
  } // switch

  // Update the LED
  UserInterface_SetLED(&UserLEDs, NewLEDNumber, LED_BLINK);
} // UserInterface_ChangeChannel
