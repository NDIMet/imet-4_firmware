// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   flight.c
//
//      CONTENTS    :   Routines for querying and updating the flight status
//                      of the radiosonde
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// None.


// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sFLIGHT_STATUS FlightStatus;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static uint32_t SlantRange = 0;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void ClearFlightBuffer(sFLIGHT_STATUS* ptrStatus);
static uint32_t CalculateLOS(sFLIGHT_STATUS* ptrStatus, sUBX_NAV_SOL* ptrSOL);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : ClearFlightBuffer
//
//  I/P       : sFLIGHT_STATUS* ptrStatus = Pointer to the flight status
//                                          data structure
//
//  O/P       : None.
//
//  OPERATION : Resets the flight buffer for launch detect/burst detect.
//
//  UPDATED   : 2016-05-26 JHM
//
// **************************************************************************
static void ClearFlightBuffer(sFLIGHT_STATUS* ptrStatus)
{
  int i;
  
  for (i = 0; i < FLIGHT_BUF_SIZE; i++)
  {
    ptrStatus->AltitudeBuffer[i] = 0;
  } // for
  
  ptrStatus->Flag_BufferFull = RESET;
  ptrStatus->AltitudeIndex = 0;
  ptrStatus->GoodDataCounter = 0;
} // ClearFlightBuffer

// **************************************************************************
//
//  FUNCTION  : CalculateLOS
//
//  I/P       : sFLIGHT_STATUS* ptrStatus = Pointer to flight status data
//                                          structure
//              sUBX_NAV_SOL* ptrSOL = Pointer to NAV SOL message
//
//  O/P       : uint32_t = Line of site in meters
//
//  OPERATION : Calculates the line-of-site (slant range) from the launch
//              location in ptrStatus to the current position in ptrSOL.
//
//  UPDATED   : 2016-06-27 JHM
//
// **************************************************************************
static uint32_t CalculateLOS(sFLIGHT_STATUS* ptrStatus, sUBX_NAV_SOL* ptrSOL)
{
  int32_t DeltaX;
  int32_t DeltaY;
  int32_t DeltaZ;
  float LineOfSite;

  // Get the differences
  DeltaX = ptrSOL->ecefX - ptrStatus->LaunchLocation.X;
  DeltaY = ptrSOL->ecefY - ptrStatus->LaunchLocation.Y;
  DeltaZ = ptrSOL->ecefZ - ptrStatus->LaunchLocation.Z;

  // Convert the distances from centimeters to meters
  DeltaX /= 100;
  DeltaY /= 100;
  DeltaZ /= 100;

  // Square the results
  DeltaX *= DeltaX;
  DeltaY *= DeltaY;
  DeltaZ *= DeltaZ;

  LineOfSite = sqrt((float)(DeltaX + DeltaY + DeltaZ));

  return (uint32_t)LineOfSite;
} // CalculateLOS

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Flight_Init
//
//  I/P       : sFLIGHT_STATUS* ptrStatus = Pointer to flight status data
//                                          structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the flight status to default values and resets
//              the flight buffer.
//
//  UPDATED   : 2016-05-26 JHM
//
// **************************************************************************
void Flight_Init(sFLIGHT_STATUS* ptrStatus)
{
  // Reset variables
  ptrStatus->LineOfSite = 0;
  ptrStatus->Status = FLIGHT_ST_INIT;
  ptrStatus->LaunchLocation.X = 0;
  ptrStatus->LaunchLocation.Y = 0;
  ptrStatus->LaunchLocation.Z = 0;
  ptrStatus->LineOfSite = 0;
  ptrStatus->AltitudeIndex = 0;
  ptrStatus->Flag_BufferFull = RESET;
  ptrStatus->GoodDataCounter = 0;

  // Reset the flight buffer. This also resets the buffer full flag
  ClearFlightBuffer(ptrStatus);

  // If the sonde is connected to the manufacturing port, set to ground
  if (PushButton.State == PB_ST_BYPASSED)
  {
    ptrStatus->Status = FLIGHT_ST_GROUND;
  }
} // Flight_Init

// **************************************************************************
//
//  FUNCTION  : Flight_StatusHandler
//
//  I/P       : sFLIGHT_STATUS* ptrStatus = Pointer to flight status data
//                                          structure
//              sUBX_NAV_SOL* ptrSOL = Pointer to NAV_SOL decoded message from
//                                     Ublox.h
//
//  O/P       : None.
//
//  OPERATION : Updates the flight status based on the GPS information provided
//              by NAV_SOL and NAV_POSLLH. In summary, this routine:
//              1) Waits to acquire GPS with the transmitter set to the
//                 lowest power setting. Once GPS is acquired, the position
//                 is saved.
//              2) Detects launch by waiting for the altitude to be 1 km AGL
//                 and the slant range (LOS) to be greater than 1 km from
//                 acquisition position in (1). Once launch is detected, the
//                 transmitter power increases to level 2.
//              3) If the slant range is greater than 10 km, the transmitter
//                 power goes to level 3.
//              4) If the slant range is greater than 100 km, the transmitter
//                 power goes to level 4.
//
//  UPDATED   : 2016-12-30 JHM
//
// **************************************************************************
void Flight_StatusHandler(sFLIGHT_STATUS* ptrStatus)
{
  if (ptrStatus->AltitudeIndex == (FLIGHT_BUF_SIZE - 1))
  {
    ptrStatus->Flag_BufferFull = SET;
  }

  // Add the next altitude (in meters) to the buffer. Native format is millimeters, so convert units.
  ptrStatus->AltitudeBuffer[ptrStatus->AltitudeIndex] = UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].hMSL / 1000;

  switch (ptrStatus->Status)
  {
    case FLIGHT_ST_INIT:
      if (Transmitter.Flag_AutoPowerLevel == SET)
      {
        // Set to the lowest ON level
        CC115L_SetPowerAmpLevel(&Transmitter, RFPA_PWR_2);
      }
      // Advance the state machine
      ptrStatus->Status = FLIGHT_ST_ACQUIRE;
      break;

    case FLIGHT_ST_ACQUIRE:
      if (Flag_GPS_Valid == SET)
      {
        ptrStatus->GoodDataCounter++;
      }
      else
      {
        ptrStatus->GoodDataCounter = 0;
      }

      // If GPS is good and buffer is full, advance to RFL
      if ((ptrStatus->Flag_BufferFull == SET) &&
          (ptrStatus->GoodDataCounter >= FLIGHT_GOOD_THRESHOLD))
      {
        // Lock in the launch location
        ptrStatus->LaunchLocation.X = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefX;
        ptrStatus->LaunchLocation.Y = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefY;
        ptrStatus->LaunchLocation.Z = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefZ;
        ptrStatus->LaunchLocation.Latitude = UBX_POSLLH_Buffer.Buffer[UBX_SOL_Buffer.Start].lat;
        ptrStatus->LaunchLocation.Longitude = UBX_POSLLH_Buffer.Buffer[UBX_SOL_Buffer.Start].lon;
        ptrStatus->LaunchLocation.HeightEES = UBX_POSLLH_Buffer.Buffer[UBX_SOL_Buffer.Start].hEES / 1000; // Convert from millimeters to meters
        ptrStatus->LaunchLocation.HeightMSL = UBX_POSLLH_Buffer.Buffer[UBX_SOL_Buffer.Start].hMSL / 1000; // Convert from millimeters to meters

        // Advance the state machine
        ptrStatus->Status = FLIGHT_ST_RFL;
      }
      break;

    case FLIGHT_ST_RFL:
      // Keep checking GPS, go back to INIT if we lose lock
      if (Flag_GPS_Valid == RESET)
      {
        // Reset the state machine to initial state
        ptrStatus->Status = FLIGHT_ST_ACQUIRE;
        // Reset the data counter
        ptrStatus->GoodDataCounter = 0;
      }
      else
      {
        // Launch is detected if we are 1 km above our launch location and the line-of-site is greater than 1 km
        if (ptrStatus->AltitudeBuffer[ptrStatus->AltitudeIndex] > (ptrStatus->LaunchLocation.HeightMSL + FLIGHT_ALT_THRESHOLD) &&
        		(CalculateLOS(ptrStatus, &UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start]) > FLIGHT_LOS_LEVEL2))
        {
          // Advance the state machine
          ptrStatus->Status = FLIGHT_ST_LAUNCHED;
          if (Transmitter.Flag_AutoPowerLevel == SET)
          {
            // Step up to the next power level
            CC115L_SetPowerAmpLevel(&Transmitter, RFPA_PWR_2);
            // Turn off the LEDs to save power
            UserInterface_SetLED(&UserLEDs, LED_NONE, LED_OFF);
          }
        }
      }
      break;

    case FLIGHT_ST_LAUNCHED:
      // Calculate the slant range
      SlantRange = CalculateLOS(ptrStatus, &UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start]);

      if (RFPA_PWR_2 == Transmitter.PowerLevel)
      {
        if (SlantRange > FLIGHT_LOS_LEVEL3)
        {
          CC115L_SetPowerAmpLevel(&Transmitter, RFPA_PWR_3);
        }
      }
      else if (RFPA_PWR_3 == Transmitter.PowerLevel)
      {
        if (SlantRange > FLIGHT_LOS_LEVEL4)
        {
          CC115L_SetPowerAmpLevel(&Transmitter, RFPA_PWR_4);
        }
      }
      break;

    case FLIGHT_ST_DESCEND:
      break;

    case FLIGHT_ST_GROUND:
      break;
  } // switch

  // Increment the altitude index (safe)
  ptrStatus->AltitudeIndex = (ptrStatus->AltitudeIndex + 1) % FLIGHT_BUF_SIZE;
} // Flight_StatusHandler
