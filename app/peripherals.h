#ifndef __PERIPHERALS_H
#define __PERIPHERALS_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-X Control Board Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.h
//
//      CONTENTS    :   Header file for iMet-X Control Board Peripherals
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Wait Timers for Handler routines
// Accurate time delay timer
#define WAIT_TIM               TIM13
#define WAIT_RCC               RCC_APB1Periph_TIM13
#define WAIT_FREQ              100000  // 100 kHZ

// Pressure I2C Peripherals
#define PRESS_I2C              I2C1
#define PRESS_RCC_SYSCLK       RCC_I2C1CLK_SYSCLK
#define PRESS_RCC              RCC_APB1Periph_I2C1

#define PRESS_SCL_PORT_RCC     RCC_AHBPeriph_GPIOB
#define PRESS_SCL_PORT         GPIOB
#define PRESS_SCL_PIN          GPIO_Pin_8
#define PRESS_SCL_PINSRC       GPIO_PinSource8
#define PRESS_SCL_AF           GPIO_AF_4

#define PRESS_SDA_PORT_RCC     RCC_AHBPeriph_GPIOB
#define PRESS_SDA_PORT         GPIOB
#define PRESS_SDA_PIN          GPIO_Pin_9
#define PRESS_SDA_PINSRC       GPIO_PinSource9
#define PRESS_SDA_AF           GPIO_AF_4

// Boom I2C Peripherals
#define BOOM_I2C               I2C2
#define BOOM_RCC_SYSCLK        RCC_I2C2CLK_SYSCLK
#define BOOM_RCC               RCC_APB1Periph_I2C2

#define BOOM_SCL_PORT_RCC      RCC_AHBPeriph_GPIOF
#define BOOM_SCL_PORT          GPIOF
#define BOOM_SCL_PIN           GPIO_Pin_6
#define BOOM_SCL_PINSRC        GPIO_PinSource6
#define BOOM_SCL_AF            GPIO_AF_4

#define BOOM_SDA_PORT_RCC      RCC_AHBPeriph_GPIOF
#define BOOM_SDA_PORT          GPIOF
#define BOOM_SDA_PIN           GPIO_Pin_7
#define BOOM_SDA_PINSRC        GPIO_PinSource7
#define BOOM_SDA_AF            GPIO_AF_4

// SDADC Peripherals
#define SDADC_INIT_TIMEOUT     20
#define SDADC_CAL_TIMEOUT_MS   60
#define SDADC1_GAIN            SDADC_Gain_1
#define SDADC_VADC1_DEFAULT    3000
#define SDADC_VACC1_DEFAULT    3000

// Battery SDADC Hardware
#define BATTV_SDADC            SDADC1
#define BATTV_RCC              RCC_AHBPeriph_GPIOB
#define BATTV_PORT             GPIOB
#define BATTV_PIN              GPIO_Pin_1
#define BATTV_SDADC_CH         SDADC_Channel_5

// PT100 SDADC Hardware
#define PT100_SDADC            SDADC1
#define PT100_RCC              RCC_AHBPeriph_GPIOB
#define PT100_PORT             GPIOB
#define PT100_PIN              GPIO_Pin_0
#define PT100_SDADC_CH         SDADC_Channel_6

// Default ADC calibration values
#define SDADC_VDD_DEFAULT        3000 // VDD in millivolts

#define SDADC1_GAIN_VAL          1

// Watchdog peripherals
#define WWDG_MAX_CNT           0x7F

// Wait channel assignments
#define WAIT_CH_CNT            10

#define PRESSURE_WAIT_CH       0
#define HUMIDITY_WAIT_CH       1
#define BATTERY_WAIT_CH        2
#define NTC_WAIT_CH            4
#define HVAC_WAIT_CH           5
#define UBX_WAIT_CH            6
#define SDADC1_WAIT_CH         7
#define DF_WAIT_CH             8
#define GP_WAIT_CH             9

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint16_t VDD;
  uint16_t VAccurate1;
  uint16_t VADC1;
} sADC_COEFFICIENTS;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern uint16_t Wait_Tics;
extern sADC_COEFFICIENTS ADC_Coefficients;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Peripherals_Init(void);
uint16_t Peripherals_CalculateADCVoltage(int16_t Count);
void Peripherals_ADC1_Handler(void);
void Peripherals_Disable(void);
void Peripherals_Enable(void);
// Precise sleep functions
void Wait(uint8_t Channel, uint16_t ms);
FlagStatus GetWaitFlagStatus(uint8_t Channel);
void WaitHandler(void);

#endif /* __PERIPHERALS_H */
