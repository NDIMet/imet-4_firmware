// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   peripherals.c
//
//      CONTENTS    :   I2C, Timer, and other miscellaneous peripheral
//                      routines.
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
typedef struct
{
  FlagStatus Flag_Active;
  uint16_t TicValue;
} sWAIT_CHANNEL;

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
uint16_t Wait_Tics = 0;
sADC_COEFFICIENTS ADC_Coefficients;

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
static sWAIT_CHANNEL WaitChannels[WAIT_CH_CNT];

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitPSS(void);
static void InitWaitChannels(void);
static void InitWaitTimer(void);
static void InitPressureI2C(void);
static void InitBoomI2C(void);
static void InitSDADC1(void);
//static void InitWatchdog(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitPSS
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the power supply supervisor
//
//  UPDATED   : 2015-06-03 JHM
//
// **************************************************************************
static void InitPSS(void)
{
  // Set the PVD to
  // Rising = 2.76V to 3.00V
  // Falling = 2.66V to 2.90V
  PWR_PVDLevelConfig(PWR_PVDLevel_7);
  PWR_PVDCmd(ENABLE);

  // Wait until voltage is above the threshold level
  while (PWR_GetFlagStatus(PWR_FLAG_PVDO) == SET);
} // InitPSS

// **************************************************************************
//
//  FUNCTION  : InitWaitChannels
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the local wait timer structures.
//
//  UPDATED   : 2016-04-12 JHM
//
// **************************************************************************
static void InitWaitChannels(void)
{
  int i;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    WaitChannels[i].Flag_Active = RESET;
    WaitChannels[i].TicValue = 0;
  }
} // InitWaitChannels

// **************************************************************************
//
//  FUNCTION  : InitWaitTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the wait timer for precise time delays.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void InitWaitTimer(void)
{
  uint16_t Prescaler;
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(WAIT_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM13_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Make this low priority
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;

  NVIC_Init(&NVIC_InitStructure);

  // Configure timer clock for 10 kHz with 1 kHz tic
  Prescaler = (uint16_t)(SystemCoreClock / WAIT_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_Prescaler = Prescaler;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 100 - 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(WAIT_TIM, &TIM_TimeBaseStructure);

  // Enable the overflow interrupt
  TIM_ITConfig(WAIT_TIM, TIM_IT_Update, ENABLE);

  // External timer is on and constantly running
  TIM_Cmd(WAIT_TIM, ENABLE);
} // InitWaitTimer

// **************************************************************************
//
//  FUNCTION  : InitPressureI2C
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the I2C peripheral for pressure sensor.
//
//  UPDATED   : 2016-05-04 JHM
//
// **************************************************************************
static void InitPressureI2C(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef  I2C_InitStructure;

  // Select the clock to be used for I2C operation - use the SYSCLK
  RCC_I2CCLKConfig(PRESS_RCC_SYSCLK);

  // Enable GPIO Peripheral clock for I2C2 and SCL and SDA)
  RCC_APB1PeriphClockCmd(PRESS_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(PRESS_SCL_PORT_RCC | PRESS_SDA_PORT_RCC, ENABLE);

  GPIO_PinAFConfig(PRESS_SCL_PORT, PRESS_SCL_PINSRC, PRESS_SCL_AF);
  GPIO_PinAFConfig(PRESS_SDA_PORT, PRESS_SDA_PINSRC, PRESS_SDA_AF);

  // Configure the I2C SDA and SCL pins
  GPIO_InitStructure.GPIO_Pin = PRESS_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(PRESS_SCL_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = PRESS_SDA_PIN;
  GPIO_Init(PRESS_SCL_PORT, &GPIO_InitStructure);

  // Disable
  I2C_Cmd(PRESS_I2C, DISABLE);
  I2C_DeInit(PRESS_I2C);

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  // Use I2C_Timing_Configuration_V1.0.1.xls to obtain the value for the I2C_Timing element
  I2C_InitStructure.I2C_Timing = 0x10805E89; // 100 kHz

  I2C_Init(PRESS_I2C, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(PRESS_I2C, ENABLE);
} // InitPressureI2C

// **************************************************************************
//
//  FUNCTION  : InitBoomI2C
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the I2C peripheral for humidity sensor.
//
//  UPDATED   : 2016-05-04 JHM
//
// **************************************************************************
static void InitBoomI2C(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  I2C_InitTypeDef  I2C_InitStructure;

  // Select the clock to be used for I2C operation - use the SYSCLK
  RCC_I2CCLKConfig(BOOM_RCC_SYSCLK);

  // Enable GPIO Peripheral clock for I2C2 and SCL and SDA)
  RCC_APB1PeriphClockCmd(BOOM_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(BOOM_SCL_PORT_RCC | BOOM_SDA_PORT_RCC, ENABLE);

  GPIO_PinAFConfig(BOOM_SCL_PORT, BOOM_SCL_PINSRC, BOOM_SCL_AF);
  GPIO_PinAFConfig(BOOM_SDA_PORT, BOOM_SDA_PINSRC, BOOM_SDA_AF);

  // Configure the I2C SDA and SCL pins
  GPIO_InitStructure.GPIO_Pin = BOOM_SCL_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_Init(BOOM_SCL_PORT, &GPIO_InitStructure);
  GPIO_InitStructure.GPIO_Pin = BOOM_SDA_PIN;
  GPIO_Init(BOOM_SCL_PORT, &GPIO_InitStructure);

  // Disable
  I2C_Cmd(BOOM_I2C, DISABLE);
  I2C_DeInit(BOOM_I2C);

  // Configuration of I2C
  I2C_InitStructure.I2C_Mode = I2C_Mode_I2C;
  I2C_InitStructure.I2C_DigitalFilter = 0x00;
  I2C_InitStructure.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
  I2C_InitStructure.I2C_OwnAddress1 = 0x00;
  I2C_InitStructure.I2C_Ack = I2C_Ack_Enable;
  I2C_InitStructure.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
  // Use I2C_Timing_Configuration_V1.0.1.xls to obtain the value for the I2C_Timing element
  I2C_InitStructure.I2C_Timing = 0x10805E89; // 100 kHz

  I2C_Init(BOOM_I2C, &I2C_InitStructure);

  // Enable I2C
  I2C_Cmd(BOOM_I2C, ENABLE);
} // InitBoomI2C


// **************************************************************************
//
//  FUNCTION  : InitSDADC1
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes SDADC1 for analog-to-digital conversion of the
//              battery voltage and PT100 circuits.
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
static void InitSDADC1(void)
{
  SDADC_AINStructTypeDef SDADC_AINStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  uint32_t SDADCTimeout = 0;

  // Shut down the SDADC and set back to default values
  SDADC_Cmd(SDADC1, DISABLE);
  SDADC_DeInit(SDADC1);

  // Enable the GPIO clocks
  RCC_AHBPeriphClockCmd(BATTV_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(PT100_RCC, ENABLE);

  // Configure the pins
  GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  // Battery voltage
  GPIO_InitStructure.GPIO_Pin = BATTV_PIN;
  GPIO_Init(BATTV_PORT, &GPIO_InitStructure);
  // PT100
  GPIO_InitStructure.GPIO_Pin = PT100_PIN;
  GPIO_Init(PT100_PORT, &GPIO_InitStructure);

  // Enable the PWR interface clock
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  // Enable the SDADC analog interface
  PWR_SDADCAnalogCmd(PWR_SDADCAnalog_1, ENABLE);

  // Enable SDADC1 clock
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SDADC1, ENABLE);

  // Set the clock to slow mode (1.0 MHz);
  RCC_SDADCCLKConfig(RCC_SDADCCLK_SYSCLK_Div48);

  // Select the reference voltage
  SDADC_VREFSelect(SDADC_VREF_Ext);

  // Set the clock to slow clock
  SDADC_SlowClockCmd(SDADC1, ENABLE);

  DelayMs(10);

  // Enable the SDADC peripheral
  SDADC_Cmd(SDADC1, ENABLE);

  // Enter initialization mode
  SDADC_InitModeCmd(SDADC1, ENABLE);

  SDADCTimeout = SDADC_INIT_TIMEOUT;

  // Wait for INITRDY flag to be set
  while((SDADC_GetFlagStatus(SDADC1, SDADC_FLAG_INITRDY) == RESET) && (--SDADCTimeout != 0));

  if(SDADCTimeout == 0)
  {
    return;
  }

  // Set the first configuration for SDADC1 (conf0)
  SDADC_AINStructure.SDADC_InputMode = SDADC_InputMode_SEZeroReference;
  SDADC_AINStructure.SDADC_Gain = SDADC_Gain_1;
  SDADC_AINStructure.SDADC_CommonMode = SDADC_CommonMode_VSSA;
  SDADC_AINStructure.SDADC_Offset = 0;
  SDADC_AINInit(SDADC1, SDADC_Conf_0, &SDADC_AINStructure);

  // Set the channels to configuration 0
  SDADC_ChannelConfig(SDADC1, BATTV_SDADC_CH | PT100_SDADC_CH, SDADC_Conf_0);

  // Select the SDADC channels to be used for injected conversion
  SDADC_InjectedChannelSelect(SDADC1, BATTV_SDADC_CH | PT100_SDADC_CH);

  // Set the injected channels to single-shot mode, not continuous
  SDADC_InjectedContinuousModeCmd(SDADC1, DISABLE);

  // Exit initialization mode
  SDADC_InitModeCmd(SDADC1, DISABLE);

  // Calibrate the first configuration
  SDADC_CalibrationSequenceConfig(SDADC1, SDADC_CalibrationSequence_1);
  // Start the calibration
  SDADC_StartCalibration(SDADC1);
  // Set calibration timeout: 5.12 ms at 6 MHz in a single calibration sequence
  SDADCTimeout = SDADC_CAL_TIMEOUT_MS;
  // wait for POT_SDADC Calibration process to end
  while((SDADC_GetFlagStatus(SDADC1, SDADC_FLAG_EOCAL) == RESET) && (SDADCTimeout))
  {
    SDADCTimeout--;
    DelayMs(1);
  }

  if(SDADCTimeout == 0)
  {
    // Calibration has failed, exit immediately
    return;
  }

  // Interrupt configuration
  NVIC_InitStructure.NVIC_IRQChannel = SDADC1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Make this low priority
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 14;
  NVIC_Init(&NVIC_InitStructure);

  // Enable the end of injected conversion interrupt
  SDADC_ITConfig(SDADC1, SDADC_IT_JEOC, ENABLE);
} // InitSDADC1

/*
// **************************************************************************
//
//  FUNCTION  : InitWatchdog
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the watchdog timer. Does not start the timer yet.
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void InitWatchdog(void)
{
  // Enable the Watchdog Peripheral
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);

  // Set Prescaler to 8
  WWDG_SetPrescaler(WWDG_Prescaler_8);

  // Set window value to maximum
  // (i.e. We can refresh at any time - the upper window limit is effectively disabled)
  WWDG_SetWindowValue(WWDG_MAX_CNT);
} // InitWatchdog
*/


// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Peripherals_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the LED and Port Configuration Peripherals
//
//  UPDATED   : 2014-11-10 JHM
//
// **************************************************************************
void Peripherals_Init(void)
{
  if (Flag_Config_Defaults == SET)
  {
    ADC_Coefficients.VDD = SDADC_VDD_DEFAULT;
    ADC_Coefficients.VAccurate1 = SDADC_VACC1_DEFAULT;
    ADC_Coefficients.VADC1 = SDADC_VADC1_DEFAULT;
  }
  else
  {
    ADC_Coefficients.VDD = iQ_Config.VDD;
    ADC_Coefficients.VAccurate1 = iQ_Config.VAccurate1;
    ADC_Coefficients.VADC1 = iQ_Config.VADC1;
  }

  // Initialize the power control
  InitPSS();

  // Configure the interrupt group as group 1
  // 4 bit for preemption
  // 0 bits for sub-priority
  // This means all the interrupts are handled by preemption bits
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  // Initialize the internal sensor wait timer
  InitWaitChannels();
  InitWaitTimer();

  // Initialize the I2C channels
  InitPressureI2C();
  InitBoomI2C();

  // Initialize the SDADC
  InitSDADC1();

  // Initialize the watchdog
  //InitWatchdog();
} // Peripherals_Init

// **************************************************************************
//
//  FUNCTION    :  Peripherals_CalculateADCVoltage
//
//  I/P         :  int16_t Count = ADC count
//
//  O/P         :  uint16_t = Voltage in milliVolts
//
//  OPERATION   :  Calculates the ADC voltage based on the specified count,
//                 coefficients, and gain settings.
//
//  UPDATED     :  2016-05-04 JHM
//
// **************************************************************************
uint16_t Peripherals_CalculateADCVoltage(int16_t Count)
{
  int32_t Value;

  Value = (int32_t)Count + 32767;
  Value *= (int32_t)ADC_Coefficients.VDD;
  Value /= 65535;

  Value /= SDADC1_GAIN_VAL;
  Value *= (int32_t)ADC_Coefficients.VAccurate1;
  Value /= (int32_t)ADC_Coefficients.VADC1;

  return (uint16_t)Value;
} // Periph_CalculateADCVoltage

// **************************************************************************
//
//  FUNCTION    :  Peripherals_ADC1_Handler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Starts an SDADC1 conversion every 100 ms. This routine
//                 should be called continuously from main.c.
//
//  UPDATED     :  2016-08-30 JHM
//
// **************************************************************************
void Peripherals_ADC1_Handler(void)
{
  if (GetWaitFlagStatus(SDADC1_WAIT_CH) == SET)
  {
    return;
  }

  // Select both the battery and PT100 ADC channels
  //SDADC_InjectedChannelSelect(SDADC1, BATTV_SDADC_CH | PT100_SDADC_CH);

  // Select the battery channel
  SDADC_InjectedChannelSelect(SDADC1, BATTV_SDADC_CH);

  // Start the conversions
  SDADC_SoftwareStartInjectedConv(SDADC1);

  // Wait 100 ms for the next conversion
  Wait(SDADC1_WAIT_CH, 100);
} // Peripherals_ADC1_Handler

// **************************************************************************
//
//  FUNCTION    :  Peripherals_Disable
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Disables all the peripherals. This routine should be
//                 called when flash is being written so that no interrupts
//                 will trigger.
//
//  UPDATED     :  2016-08-30 JHM
//
// **************************************************************************
void Peripherals_Disable(void)
{
  USART_Cmd(UBX_USART, DISABLE);
  USART_Cmd(MFG_USART, DISABLE);
  TIM_Cmd(MOD0_TIM, DISABLE);
  TIM_Cmd(IMET1_TIM, DISABLE);
  TIM_Cmd(NTC_TIM, DISABLE);
  TIM_Cmd(USER_TIM, DISABLE);
  TIM_Cmd(WAIT_TIM, DISABLE);
  TIM_Cmd(PWM_TIM, DISABLE);
  SDADC_Cmd(SDADC1, DISABLE);
} // Periph_Disable

// **************************************************************************
//
//  FUNCTION    :  Peripherals_Enable
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Enables all the peripherals. This routine should be
//                 called after the Peripherals_Disable() routine in order
//                 to turn all the peripherals back on. This only works
//                 if all the peripherals have been properly initialized.
//
//  UPDATED     :  2016-08-30 JHM
//
// **************************************************************************
void Peripherals_Enable(void)
{
  // Restart clocks
  USART_Cmd(UBX_USART, ENABLE);
  USART_Cmd(MFG_USART, ENABLE);
  TIM_Cmd(MOD0_TIM, ENABLE);
  TIM_Cmd(IMET1_TIM, ENABLE);
  TIM_Cmd(NTC_TIM, ENABLE);
  TIM_Cmd(USER_TIM, ENABLE);
  TIM_Cmd(WAIT_TIM, ENABLE);
  TIM_Cmd(PWM_TIM, ENABLE);
  SDADC_Cmd(SDADC1, ENABLE);
} // Periph_Enable

// **************************************************************************
//
//  FUNCTION    :  Wait
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//                 uint16_t ms - time to wait in milliseconds
//
//  O/P         :  None.
//
//  OPERATION   :  Sets the global variable ChannelX_Delay_Flag for the
//                 duration of time specified in milliseconds.  Once the time
//                 has expired it resets the flag.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
void Wait(uint8_t Channel, uint16_t ms)
{
  WaitChannels[Channel].TicValue = Wait_Tics + ms;
  WaitChannels[Channel].Flag_Active = SET;
} // Wait

// **************************************************************************
//
//  FUNCTION    :  GetWaitFlagStatus
//
//  I/P         :  uint8_t Channel - 1, 2, 3, 4
//
//  O/P         :  FlagStatus - SET (waiting) RESET (idle)
//
//  OPERATION   :  Returns the flag status of the wait channel selected.
//
//  UPDATED     :  2015-04-30 JHM
//
// **************************************************************************
FlagStatus GetWaitFlagStatus(uint8_t Channel)
{
  return WaitChannels[Channel].Flag_Active;
} // GetWaitFlagStatus

// **************************************************************************
//
//  FUNCTION    :  WaitHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the 1ms wait timer interrupt
//
//  UPDATED     :  2015-05-28 JHM
//
// **************************************************************************
void WaitHandler(void)
{
  int i;

  for (i = 0; i < WAIT_CH_CNT; i++)
  {
    if (WaitChannels[i].Flag_Active == SET)
    {
      if (WaitChannels[i].TicValue == Wait_Tics)
      {
        WaitChannels[i].Flag_Active = RESET;
      }
    }
  } // for
} // WaitHandler
