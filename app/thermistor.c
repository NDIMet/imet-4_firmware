// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   thermistor.c
//
//      CONTENTS    :   Routines for thermistor state machine and calculations
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  INVALID_TEMP         9999
#define  COUNTER_MIN          10000
#define  REF_LO_THRESHOLD     450000
#define  REF_HI_THRESHOLD     700000
#define  N_CAL                5
#define  CAL_MAX              1020000
#define  CAL_MIN              989999

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
typedef enum
{
  REF_VREF = 0,
  REF_3_4 = 1,
  REF_1_2 = 2,
  REF_1_4 = 3
}Type_RefLevel;

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sThermistor Thermistor;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
uint32_t CounterValue = 0;

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void InitTimer(void);
static void InitComparator(void);
static void SetComparator(Type_RefLevel RefLevel);
static void SetReference(void);
static void SetNTC(void);
static void Discharge(void);
static void FilterCounts(sThermistor* ThermistorStructure);
static void CalculateResistance(sThermistor* ThermistorStructure);
static void CorrectResistance(sThermistor* ThermistorStructure);
static void CalculateTemperature(sThermistor* ThermistorStructure);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the thermistor GPIO pins as high-impedance
//              inputs. This will be continuously changed while the NTC
//              advances through the state machine.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clocks
  RCC_AHBPeriphClockCmd(NTC_REF_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(NTC_COMMON_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(NTC_DRIVE_RCC, ENABLE);

  // Configure the LED hi-Z inputs to start
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  // Reference Pin
  GPIO_InitStructure.GPIO_Pin = NTC_REF_PIN;
  GPIO_Init(NTC_REF_PORT, &GPIO_InitStructure);
  // Common Pin
  GPIO_InitStructure.GPIO_Pin = NTC_COMMON_PIN;
  GPIO_Init(NTC_COMMON_PORT, &GPIO_InitStructure);
  // Drive Pin
  GPIO_InitStructure.GPIO_Pin = NTC_DRIVE_PIN;
  GPIO_Init(NTC_DRIVE_PORT, &GPIO_InitStructure);
} // InitGPIO

// **************************************************************************
//
//  FUNCTION  : InitTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the thermistor timer with the capture register
//              wired internally to comparator COMP1.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void InitTimer(void)
{
  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
  TIM_ICInitTypeDef TIM_ICInitStructure;

  // Reset the timer peripheral to defaults
  TIM_DeInit(NTC_TIM);

  // TIMX clock enable
  RCC_APB1PeriphClockCmd(NTC_TIM_RCC, ENABLE);

  // Disable the timer for configuration
  TIM_Cmd(NTC_TIM, DISABLE);

  TIM_TimeBaseStructure.TIM_Prescaler = 1;
  TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  TIM_TimeBaseStructure.TIM_Period = 0xFFFFFFFF; // Maximum value
  TIM_TimeBaseInit(NTC_TIM, &TIM_TimeBaseStructure);

  // Input capture
  TIM_ICInitStructure.TIM_Channel = TIM_Channel_4;
  TIM_ICInitStructure.TIM_ICPolarity = TIM_ICPolarity_Rising;
  TIM_ICInitStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
  TIM_ICInitStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;
  TIM_ICInitStructure.TIM_ICFilter = 0;
  TIM_ICInit(NTC_TIM, &TIM_ICInitStructure);

  TIM_SetCounter(NTC_TIM, 0);
} // InitTimer

// **************************************************************************
//
//  FUNCTION  : InitComparator
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the thermistor comparator with the output set to
//              capture register 4 of Timer 5.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void InitComparator(void)
{
  COMP_InitTypeDef COMP_InitStructure;

  // Initialize the comparator clock
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

  // Comparator settings
  COMP_InitStructure.COMP_Hysteresis = COMP_Hysteresis_No;
  COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_VREFINT;
  COMP_InitStructure.COMP_Mode = COMP_Mode_UltraLowPower;
  COMP_InitStructure.COMP_Output = COMP_Output_TIM5IC4;
  COMP_InitStructure.COMP_OutputPol = COMP_OutputPol_NonInverted;
  COMP_Init(NTC_COMP, &COMP_InitStructure);

  // Disable comparator window mode
  COMP_WindowCmd(DISABLE);

  // Enable the comparator
  COMP_Cmd(NTC_COMP, ENABLE);
} // InitComparator

// **************************************************************************
//
//  FUNCTION  : SetComparator
//
//  I/P       : Type_RefLevel RefLevel
//              REF_3_4 = 3/4 VCC
//              REF_1_2 = 1/2 VCC
//              REF_1_4 = 1/4 VCC
//
//  O/P       : None.
//
//  OPERATION : Configures the I/O pin to the comparator state with a high
//              impedance input with the reference level specified by
//              Type_RefLevel. This also immediately enables the capture/compare
//              register interrupt to look for a high-to-low transition, since
//              the circuit will be discharging.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void SetComparator(Type_RefLevel RefLevel)
{
  GPIO_InitTypeDef GPIO_InitStructure;
  COMP_InitTypeDef COMP_InitStructure;

  // Switch the pin to hi-Z analog mode
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_Pin = NTC_COMMON_PIN;
  GPIO_Init(NTC_COMMON_PORT, &GPIO_InitStructure);

  // Configure the comparator
  COMP_InitStructure.COMP_Hysteresis = COMP_Hysteresis_High;
  COMP_InitStructure.COMP_Mode = COMP_Mode_HighSpeed;
  COMP_InitStructure.COMP_Output = COMP_Output_TIM5IC4;
  COMP_InitStructure.COMP_OutputPol = COMP_OutputPol_NonInverted;
  COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_VREFINT;

  switch (RefLevel)
  {
    case REF_VREF:
      COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_VREFINT;
      break;
    case REF_3_4:
      COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_3_4VREFINT;
      break;
    case REF_1_2:
      COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_1_2VREFINT;
      break;
    case REF_1_4:
      COMP_InitStructure.COMP_InvertingInput = COMP_InvertingInput_1_4VREFINT;
      break;
  } // switch


  COMP_Init(NTC_COMP, &COMP_InitStructure);
  COMP_Cmd(NTC_COMP, ENABLE);

} // SetComparator

// **************************************************************************
//
//  FUNCTION  : SetReference
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the I/O to charge the reference resistor. Once
//              the pins are configured, the RC circuit will take approximately
//              40ms to charge from 1/4VREF to 3/4VREF.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void SetReference(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Set the NTC drive pin to hi-Z input
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = NTC_DRIVE_PIN;
  GPIO_Init(NTC_DRIVE_PORT, &GPIO_InitStructure);

  // Set the reference pin to output high
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = NTC_REF_PIN;
  GPIO_Init(NTC_REF_PORT, &GPIO_InitStructure);
  GPIO_SetBits(NTC_REF_PORT, NTC_REF_PIN);
} // SetReference

// **************************************************************************
//
//  FUNCTION  : SetNTC
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Changes the configuration of the IO pins to charge the NTC
//              circuit. This consists of a resistor in parallel 1.0Mohms and
//              the value of the NTC itself. The maximum amount of time to
//              charge with a 0.47uF capacitor should be approximately 500 ms.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void SetNTC(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Set the reference pin to hi-Z input
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = NTC_REF_PIN;
  GPIO_Init(NTC_REF_PORT, &GPIO_InitStructure);

  // Set the drive pin to a output high
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = NTC_DRIVE_PIN;
  GPIO_Init(NTC_DRIVE_PORT, &GPIO_InitStructure);
  GPIO_SetBits(NTC_DRIVE_PORT, NTC_DRIVE_PIN);
} // SetNTC

// **************************************************************************
//
//  FUNCTION  : Discharge
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Discharges the capacitor for the temperature circuit.
//
//  UPDATED   : 2017-07-07 JHM
//
// **************************************************************************
static void Discharge(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Set the drive pin to output low
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = NTC_DRIVE_PIN;
  GPIO_Init(NTC_DRIVE_PORT, &GPIO_InitStructure);
  GPIO_ResetBits(NTC_DRIVE_PORT, NTC_DRIVE_PIN);

  // Set the reference pin to output low
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = NTC_REF_PIN;
  GPIO_Init(NTC_REF_PORT, &GPIO_InitStructure);
  GPIO_ResetBits(NTC_REF_PORT, NTC_REF_PIN);

  // Set the common pin to output low
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Pin = NTC_COMMON_PIN;
  GPIO_Init(NTC_COMMON_PORT, &GPIO_InitStructure);
  GPIO_ResetBits(NTC_COMMON_PORT, NTC_COMMON_PIN);
} // QuickCharge

// **************************************************************************
//
//  FUNCTION  : ValidateMeasurement
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Algorithm for detecting bad count data so it won't be used.
//              This in intended to remove the temperature spikes I am seeing
//              approximately 1/10000 measurements where the comparator seems
//              to fail. If the counts are bad, the Flag_Error flag is set in
//              the thermistor data structure.
//
//  UPDATED   : 2017-06-07 JHM
//
// **************************************************************************
/*
static void ValidateMeasurement(sThermistor* ThermistorStructure)
{

  // Throw out if the counts are the same
  if (ThermistorStructure->NTC_Count == ThermistorStructure->Reference_Count)
  {
    ThermistorStructure->Flag_Error = SET;
    // Increment so we can't stay stuck in this state
    ThermistorStructure->NTC_Count++;
    return;
  }
  // Reference count too high
  if (ThermistorStructure->Reference_Count > REF_HI_THRESHOLD)
  {
    ThermistorStructure->Flag_Error = SET;
    return;
  }
  // Reference count too low
  if (ThermistorStructure->Reference_Count < REF_LO_THRESHOLD)
  {
    ThermistorStructure->Flag_Error = SET;
    return;
  }
} // ValidateMeasurement
*/

// **************************************************************************
//
//  FUNCTION  : FilterCounts
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Filters the count values.
//
//  UPDATED   : 2017-05-17 JHM
//
// **************************************************************************
static void FilterCounts(sThermistor* ThermistorStructure)
{
  double lfCount;

  lfCount = (double)ThermistorStructure->Filtered_Reference;
  lfCount = lfCount * (1.0 - ThermistorStructure->Reference_FilterK) + ((double)ThermistorStructure->Reference_Count * ThermistorStructure->Reference_FilterK);
  ThermistorStructure->Filtered_Reference = (uint32_t)lfCount;

  lfCount = (double)ThermistorStructure->Filtered_NTC;
  lfCount = lfCount * (1.0 - ThermistorStructure->NTC_FilterK) + ((double)ThermistorStructure->NTC_Count * ThermistorStructure->NTC_FilterK);
  ThermistorStructure->Filtered_NTC = (uint32_t)lfCount;
} // FilterCounts

// **************************************************************************
//
//  FUNCTION  : CalculateResistance
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the resistance of the thermistor using the counts
//              pointed to by sThermistor*. This routine should be run once
//              the reference hi/lo counts and NTC hi/lo counts have been
//              retrieved.
//
//  UPDATED   : 2016-08-26 JHM
//
// **************************************************************************
static void CalculateResistance(sThermistor* ThermistorStructure)
{
  double EquivalentResistance;
  double Resistance;
  double FilteredRef;
  double FilteredNTC;

  FilteredRef = (double)ThermistorStructure->Filtered_Reference;
  FilteredNTC = (double)ThermistorStructure->Filtered_NTC;

  // Calculate the equivalent resistance
  EquivalentResistance = NTC_REF_RESISTOR;
  EquivalentResistance *= FilteredNTC ;
  EquivalentResistance /= FilteredRef;

  // Move the equivalent resistance into the data structure
  ThermistorStructure->EquivalentResistance = EquivalentResistance;

  // Calculate the thermistor resistance
  Resistance = ThermistorStructure->ParallelResistance * EquivalentResistance;
  Resistance /= (ThermistorStructure->ParallelResistance - EquivalentResistance);

  // Move the resistance into the buffer
  ThermistorStructure->Resistance = Resistance;
} // CalculateResistance


// **************************************************************************
//
//  FUNCTION  : CorrectResistance
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Corrects the resistance measurement value through a method
//              TBD.
//
//  UPDATED   : 2017-06-19 JHM
//
// **************************************************************************
static void CorrectResistance(sThermistor* ThermistorStructure)
{
  double Correction = 0.0;
  double Resistance = 0.0;

  // Get the measured resistance from the data structure
  Resistance = ThermistorStructure->Resistance;

  // Correct the values
  if (Resistance <= 25.0e3)
  {
    Correction = NTC_LO_A0;
    Correction += NTC_LO_A1 * Resistance;
    Correction += NTC_LO_A2 * Resistance * Resistance;
  }
  else
  {
    Correction = 0.0;
  }

  Resistance += Correction;

  // Move the data into the structure
  ThermistorStructure->CorrectedResistance = Resistance;
} // CorrectResistance

// **************************************************************************
//
//  FUNCTION  : CalculateTemperature
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the temperature of the thermistor from the
//              resistance value and Steinhart-Hart coefficients.
//
//  UPDATED   : 2015-09-15 JHM
//
// **************************************************************************
static void CalculateTemperature(sThermistor* ThermistorStructure)
{
  double fTemperature;
  double LogR;
  int16_t dTemperature;

  LogR = log(ThermistorStructure->CorrectedResistance);

  // Calculate the temperature in Kelvin
  fTemperature = 1.0 / (ThermistorStructure->Coefficients.A0 +
                       ThermistorStructure->Coefficients.A1 * LogR +
                       ThermistorStructure->Coefficients.A2 * LogR * LogR +
                       ThermistorStructure->Coefficients.A3 * LogR * LogR * LogR);

  dTemperature = (int16_t)(fTemperature * 100.0);

  ThermistorStructure->TemperatureK = dTemperature;
  dTemperature -= 27315;
  ThermistorStructure->TemperatureC = dTemperature;
} // CalculateTemperature

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Thermistor_Init
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Initializes the values of a thermistor data structure,
//              including the state machine and configuration.
//
//  UPDATED   : 2017-06-05 JHM
//
// **************************************************************************
void NTC_Init(sThermistor* ThermistorStructure)
{
  // Initialize the structure data
  ThermistorStructure->State = NTC_ST_START;
  ThermistorStructure->Flag_Error = RESET;
  ThermistorStructure->Resistance = 0;
  ThermistorStructure->Reference_Count = 600000;
  ThermistorStructure->NTC_Count = 0;
  ThermistorStructure->Reference_FilterK = 1.0;
  ThermistorStructure->NTC_FilterK = 1.0;

  // Initialize the coefficients
  ThermistorStructure->Coefficients.A0 = iQ_Config.ST0;
  ThermistorStructure->Coefficients.A1 = iQ_Config.ST1;
  ThermistorStructure->Coefficients.A2 = iQ_Config.ST2;
  ThermistorStructure->Coefficients.A3 = iQ_Config.ST3;
  ThermistorStructure->ParallelResistance = (double)iQ_Config.ParallelResistorT;

  // Initialize GPIO
  InitGPIO();

  // Initialize the NTC timer
  InitTimer();

  // Initialize the comparator
  InitComparator();

  // Discharge the capacitor
  Discharge();

  // Turn the filters on
  ThermistorStructure->Reference_FilterK = NTC_REF_K;
  ThermistorStructure->NTC_FilterK = NTC_K;
} // Thermistor_Init

// **************************************************************************
//
//  FUNCTION  : Thermistor_Handler
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Handles the thermistor state machine.
//
//  UPDATED   : 2017-06-05 JHM
//
// **************************************************************************
void NTC_Handler(sThermistor* ThermistorStructure)
{
  // Return if we are waiting
  if (GetWaitFlagStatus(NTC_WAIT_CH) == SET)
  {
    return;
  }

  switch (ThermistorStructure->State)
  {
    case NTC_ST_START:
      // Advance the state machine
      ThermistorStructure->State = NTC_ST_REF_CHG;
      // Clear the error flag in case it has been set
      ThermistorStructure->Flag_Error = RESET;
      // Discharge the capacitor
      Discharge();
      // Wait for the capacitor to discharge
      Wait(NTC_WAIT_CH, 10);
      break;

    case NTC_ST_REF_CHG:
      // Switch common pin to comparator mode
      SetComparator(REF_VREF);
      // Reset the counter and start
      TIM_SetCounter(NTC_TIM, 0);
      TIM_Cmd(NTC_TIM, ENABLE);
      // Set the reference to start charging
      SetReference();
      // Advance the state machine
      ThermistorStructure->State = NTC_ST_REF_MEAS;
      Wait(NTC_WAIT_CH, 2);
      break;

    case NTC_ST_REF_MEAS:
      if (COMP_GetOutputLevel(NTC_COMP) == COMP_OutputLevel_High)
      {
        // Capture the value
        CounterValue = TIM_GetCapture4(NTC_TIM);
        // Get the capture value
        ThermistorStructure->Reference_Count = CounterValue;
        // Stop the timer
        TIM_Cmd(NTC_TIM, DISABLE);
        // Advance the state machine
        ThermistorStructure->State = NTC_ST_NTC_CHG;
        // Discharge the capacitor
        Discharge();
        // Wait for discharge
        Wait(NTC_WAIT_CH, 10);
      }
      break;

    case NTC_ST_NTC_CHG:
      // Switch common pin to comparator mode
      SetComparator(REF_VREF);
      // Reset the counter and start
      TIM_SetCounter(NTC_TIM, 0);
      TIM_Cmd(NTC_TIM, ENABLE);
      // Set the NTC drive to start charging
      SetNTC();
      // Advance the state machine
      ThermistorStructure->State = NTC_ST_NTC_MEAS;
      Wait(NTC_WAIT_CH, 2);
      break;

    case NTC_ST_NTC_MEAS:
      if (COMP_GetOutputLevel(NTC_COMP) == COMP_OutputLevel_High)
      {
        // Capture the value
        CounterValue = TIM_GetCapture4(NTC_TIM);
        // Get the capture value
        ThermistorStructure->NTC_Count = CounterValue;
        // Stop the timer
        TIM_Cmd(NTC_TIM, DISABLE);
        // Discharge the circuit
        Discharge();
        // Advance the state machine
        ThermistorStructure->State = NTC_ST_CALC;
      }
      break;

    case NTC_ST_CALC:
      // Validate the data
      //ValidateMeasurement(ThermistorStructure);

      if (!ThermistorStructure->Flag_Error)
      {
        // Filter the counts
        FilterCounts(ThermistorStructure);
        // Calculate the resistance
        CalculateResistance(ThermistorStructure);
        // Correct the resistance
        CorrectResistance(ThermistorStructure);
        // Calculate the temperature
        CalculateTemperature(ThermistorStructure);
      }
      else
      {
        Mfg_TransmitMessage("NTC Error\r\n");
      }

      // Reset the state machine
      ThermistorStructure->State = NTC_ST_START;
      Wait(NTC_WAIT_CH, 2);
      break;
  } // switch
} // NTC_Handler

// **************************************************************************
//
//  FUNCTION  : NTC_CalibrateParallelR
//
//  I/P       : sThermistor* ThermistorStructure - Pointer to thermistor
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calibrates the parallel resistor. The temperature probe must
//              be disconnected. If the process fails, the Flag_Error flag is
//              set in the thermistor structure. If it succeeds, the new
//              ParallelResistance value will be set. It won't be stored to
//              flash unless the /SAV command is sent.
//
//  UPDATED   : 2017-06-14 JHM
//
// **************************************************************************
void NTC_CalibrateParallelR(sThermistor* ThermistorStructure)
{
  double CalibratedR;

  // The parallel resistance is equal to the equivalent resistance if there
  // is not a probe attached
  CalibratedR = ThermistorStructure->EquivalentResistance;

  if ((CalibratedR> CAL_MAX ) || (CalibratedR< CAL_MIN))
  {
    ThermistorStructure->Flag_Error = SET;
  }
  else
  {
    ThermistorStructure->Flag_Error = RESET;
    // Move the calibration result into the structure as parallel R
    ThermistorStructure->ParallelResistance = CalibratedR;
    // Move into flash - this won't be saved yet
    iQ_Config.ParallelResistorT = (uint32_t)CalibratedR;
  }
} // NTC_CalibrateParellelR
