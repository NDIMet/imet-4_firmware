#ifndef CRC_H
#define CRC_H

// Calculate 16-bit CCITT CRC using 0x1021 polynomial
// If the CRC is located at the end of the packet (MSB first),
// and the packet is good, then the CRC returns 0 if ok
// use buf_size for circular queue, otherwise set buf_size=0
uint16_t calc_crc (uint8_t *pkt, uint16_t start, uint16_t length, uint16_t buf_size);

// Calculate 16-bit CCITT CRC using 0x1021 polynomial
// places the CRC in the last 2 bytes of the packet, MSB first
// use buf_size for circular queue, otherwise set buf_size=0
void update_crc (uint8_t *pkt, uint16_t start, uint16_t length, uint16_t buf_size);

#endif
