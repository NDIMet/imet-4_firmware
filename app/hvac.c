// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   hvac.c
//
//      CONTENTS    :   Routines for initializing and handling the internal
//                      heaters and boom defroster
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// None.

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sHEATER Heater;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void InitTimer(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Configures the heater and defrost pins as outputs
//
//  UPDATED   : 2016-05-05 JHM
//
// **************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(HTR_RCC, ENABLE);

  // Configure Heater
  GPIO_InitStructure.GPIO_Pin = HTR_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(HTR_PORT, &GPIO_InitStructure);
  
  // Start with both low
  GPIO_ResetBits(HTR_PORT, HTR_PIN);
} // InitGPIO

// **************************************************************************
//
//  FUNCTION  : InitTimer
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the PWM timer.
//
//  UPDATED   : 2016-12-07 JHM
//
// **************************************************************************
static void InitTimer(void)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

  // Disable the timer peripheral
  TIM_Cmd(PWM_TIM, DISABLE);

  // Reset the timer peripheral to defaults
  TIM_DeInit(PWM_TIM);

  // Enable the clock for the timer
  RCC_APB1PeriphClockCmd(PWM_RCC, ENABLE);

  // Enable the TIMX global Interrupt
  NVIC_InitStructure.NVIC_IRQChannel = TIM12_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set the interrupt priority to highest
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_Init(&NVIC_InitStructure);

  // Set the timer clock to 100 kHz
  TIM_TimeBaseStructure.TIM_Prescaler = (uint16_t)(SystemCoreClock / PWM_FREQ) - 1;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
  // Set period to maximum value
  TIM_TimeBaseStructure.TIM_Period = 0xFFFF;
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseInit(PWM_TIM, &TIM_TimeBaseStructure);

  // Start with the interrupts disabled
  TIM_ITConfig(PWM_TIM, TIM_IT_CC1, DISABLE);
  TIM_ITConfig(PWM_TIM, TIM_IT_CC2, DISABLE);

  // Start with the timer disabled
  TIM_Cmd(PWM_TIM, DISABLE);
} // InitTimer

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : HVAC_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the heaters and defroster.
//
//  UPDATED   : 2016-05-05 JHM
//
// **************************************************************************
void HVAC_Init(sHEATER* ptrHeater)
{
  // Initialize the GPIO
  InitGPIO();
  InitTimer();

  // Default to automatic setting
  ptrHeater->Mode = HTR_AUTO;

  // Heater starts in the off state
  ptrHeater->Flag_HeaterOn = RESET;

  // Don't disable
  ptrHeater->Flag_Disable = RESET;

  // Get the heater setpoint from flash
  ptrHeater->SetPoint = iQ_Config.HeaterSetPoint;

  // Get the PWM value from flash
  ptrHeater->PWM_Value = iQ_Config.PWM_Value;

  if ((ptrHeater->PWM_Value == 0) || (ptrHeater->PWM_Value == 100))
  {
    HVAC_DisablePWM();
  }
  else
  {
    HVAC_EnablePWM();
  }
} // HVAC_Init

// **************************************************************************
//
//  FUNCTION  : HVAC_Handler
//
//  I/P       : sHEATER* ptrHeater = Pointer to heater data structure
//
//  O/P       : None.
//
//  OPERATION : Handles the heater and defroster circuit. This routine should
//              be called continuously by main.
//
//  UPDATED   : 2016-05-05 JHM
//
// **************************************************************************
void HVAC_Handler(sHEATER* ptrHeater)
{
  if (GetWaitFlagStatus(HVAC_WAIT_CH) == SET)
  {
    return;
  }

  // Kill the heaters if the battery voltage is too low
  if (Battery.Flag_LowBattery == SET)
  {
    GPIO_ResetBits(HTR_PORT, HTR_PIN);
    ptrHeater->Flag_HeaterOn = RESET;
    return;
  }

  if (ptrHeater->Mode == HTR_AUTO)
  {
    if (ptrHeater->Flag_Disable)
    {
      // Escape if the heater is disabled
      return;
    }
    if (PressureSensor.State != MS5607_ST_OFFLINE)
    {
      // Force the heater of if the voltage is below 3.0V
      if (Battery.milliVoltsBattery < 3000)
      {
        // GPIO low
        GPIO_ResetBits(HTR_PORT, HTR_PIN);
        // Update the data structure
        ptrHeater->Flag_HeaterOn = RESET;
        // Disable the heaters with them set to off
        ptrHeater->Flag_Disable = SET;
        return;
      }
      /*
      else if (PressureSensor.SensorData.CorrectedPressure < 4000)
      {
        // Change the setpoint to -30C
        ptrHeater->SetPoint = -30;
        // Change the PWM to 50%
        ptrHeater->PWM_Value = 50;
      }
      */
      // Handle the heaters based on the pressure sensor temperature
      if (PressureSensor.SensorData.Temperature < (ptrHeater->SetPoint * 100))
      {
        // Turn the heaters on
        ptrHeater->Flag_HeaterOn = SET;

        if (ptrHeater->PWM_Value == 100)
        {
          GPIO_SetBits(HTR_PORT, HTR_PIN);
        }
        else if (ptrHeater->PWM_Value == 0)
        {
          GPIO_ResetBits(HTR_PORT, HTR_PIN);
        }
      }
      else
      {
        // Turn the heaters off
        GPIO_ResetBits(HTR_PORT, HTR_PIN);
        ptrHeater->Flag_HeaterOn = RESET;
      }
    } // if
  } // if (ptrHeater->Mode == HTR_AUTO)

  // Only do this every second
  Wait(HVAC_WAIT_CH, 1000);
} // HVAC_Handler

// **************************************************************************
//
//  FUNCTION  : HVAC_Handler
//
//  I/P       : sHEATER* ptrHeater = Pointer to heater data structure
//              TypeDef_HeaterMode Mode = The new mode for the heater circuit
//                HTR_OFF  = The heater is forced off
//                HTR_ON   = The heater is forced on
//                HTR_AUTO = The heater will update automatically when HVAC_Handler is called
//
//  O/P       : None.
//
//  OPERATION : Changes the mode of the heater circuit to the new mode specified.
//
//  UPDATED   : 2016-06-21 JHM
//
// **************************************************************************
void HVAC_SetHeaterMode(sHEATER* ptrHeater, TypeDef_HeaterMode Mode)
{
  // Update the mode
  ptrHeater->Mode = Mode;

  switch (Mode)
  {
    case HTR_OFF:
      // Drive heater pin low
      GPIO_ResetBits(HTR_PORT, HTR_PIN);
      // Turn the PWM timer off and disable the interrupt
      HVAC_DisablePWM();

       // Turn the PWM timer on and enable the interrupt
       if (ptrHeater->PWM_Value == 100)
       {
         HVAC_DisablePWM();
         GPIO_ResetBits(HTR_PORT, HTR_PIN);
       }
       else if (ptrHeater->PWM_Value > 0)
       {
         HVAC_EnablePWM();
       }
       else
       {
         HVAC_DisablePWM();
         GPIO_ResetBits(HTR_PORT, HTR_PIN);
       }
      // Reset the flag
      ptrHeater->Flag_HeaterOn = RESET;
      break;

    case HTR_ON:
      // Turn the PWM timer on and enable the interrupt
      if (ptrHeater->PWM_Value == 100)
      {
        HVAC_DisablePWM();
        GPIO_SetBits(HTR_PORT, HTR_PIN);
      }
      else if (ptrHeater->PWM_Value > 0)
      {
        HVAC_EnablePWM();
      }
      else
      {
        HVAC_DisablePWM();
        GPIO_ResetBits(HTR_PORT, HTR_PIN);
      }
      // Set the flag
      ptrHeater->Flag_HeaterOn = SET;
      break;

    case HTR_AUTO:
      // Don't do anything - the GPIO will be set the next time through the
      // HVAC_Handler() routine.
      break;
  } // switch
} // HVAC_SetHeaterMode

// **************************************************************************
//
//  FUNCTION  : HVAC_EnablePWM
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Enables the pulse-width-modulation timer and interrupts
//
//  UPDATED   : 2016-12-07 JHM
//
// **************************************************************************
void HVAC_EnablePWM(void)
{
  // Make sure timer is disabled
  TIM_Cmd(PWM_TIM, DISABLE);

  // Reset the counter
  TIM_SetCounter(PWM_TIM, 0);

  // Turn the heater on
  GPIO_SetBits(HTR_PORT, HTR_PIN);

  // Set the first compare value
  TIM_SetCompare1(PWM_TIM, PWM_TIC);
  TIM_SetCompare2(PWM_TIM, Heater.PWM_Value);

  TIM_ITConfig(PWM_TIM, TIM_IT_CC1, ENABLE);
  TIM_ITConfig(PWM_TIM, TIM_IT_CC2, ENABLE);

  // Enable the peripheral
  TIM_Cmd(PWM_TIM, ENABLE);
} // HVAC_EnablePWM

// **************************************************************************
//
//  FUNCTION  : HVAC_DisablePWM
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Disables the pulse-width-modulation timer and interrupts
//
//  UPDATED   : 2016-12-07 JHM
//
// **************************************************************************
void HVAC_DisablePWM(void)
{
  // Make sure timer is disabled
  TIM_Cmd(PWM_TIM, DISABLE);
  TIM_ITConfig(PWM_TIM, TIM_IT_CC1, DISABLE);
  TIM_ITConfig(PWM_TIM, TIM_IT_CC2, DISABLE);

  // Reset the counter
  TIM_SetCounter(PWM_TIM, 0);
} // HVAC_DisablePWM
