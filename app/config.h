#ifndef __CONFIG_H
#define __CONFIG_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   config.h
//
//      CONTENTS    :   Header file for radiosonde configuration
//
// **************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Flash Information
#define CFG_PAGE_LOCN               ((uint32_t)0x0801F800)

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint32_t SerialNumber;
  double ST0;
  double ST1;
  double ST2;
  double ST3;
  uint32_t ParallelResistorT;
  uint16_t VDD;
  uint16_t VAccurate1;
  uint16_t VADC1;
  int16_t PressureBiasCorrection;
  uint16_t TxChannel0;
  uint16_t TxChannel1;
  uint16_t TxChannel2;
  uint16_t TxChannel3;
  uint16_t TxChannel4;
  uint16_t TxChannel5;
  uint16_t TxChannel6;
  uint16_t ModulationMode;
  uint16_t Deviation;
  uint16_t PowerLevel;
  uint16_t Flag_AutoPowerLevel;
  int16_t HeaterSetPoint;
  uint8_t PWM_Value;
//  uint8_t Defrost_Mode;
  TypeDef_DF_Mode Defrost_Mode;
//  uint16_t ASCII_Mode;
  Type_ASCII_Mode ASCII_Mode;
  uint32_t DataCRC;
} sConfig;

typedef union
{
  sConfig   Config;
  uint16_t  Data[sizeof(sConfig) / sizeof(uint16_t)];
} uConfig;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sConfig iQ_Config;
extern sConfig iQ_Default;
extern FlagStatus Flag_Config_Defaults;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Config_Init(void);
FLASH_Status Config_Save(void);
void Config_Get(void);

#endif
