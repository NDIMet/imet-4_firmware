// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   PT100.C
//
//      CONTENTS    :   Routines for the PT100 and defrost circuits
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************


// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sPT100_Sensor PT100Sensor;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitGPIO(void);
static void CalculateResistance(sPT100_Sensor* ptrPT100);
static void CalculateTemperature(sPT100_Sensor* ptrPT100);
static void BenoitAlgorithm(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitGPIO
//
//  I/P       : None
//
//  O/P       : None.
//
//  OPERATION : Initializes the GPIO for the PT100 pins.
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
static void InitGPIO(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  // Enable the clocks
  RCC_AHBPeriphClockCmd(PT100_ON_RCC, ENABLE);
  RCC_AHBPeriphClockCmd(PT100_DEFROST_RCC, ENABLE);

  // Configure both pins for output with pull-up
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  // Drive Pin
  GPIO_InitStructure.GPIO_Pin = PT100_ON_PIN;
  GPIO_Init(PT100_ON_PORT, &GPIO_InitStructure);
  // Defrost Pin
  GPIO_InitStructure.GPIO_Pin = PT100_DEFROST_PIN;
  GPIO_Init(PT100_DEFROST_PORT, &GPIO_InitStructure);

  // Set both initial states to off
  GPIO_ResetBits(PT100_ON_PORT, PT100_ON_PIN);
  GPIO_ResetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
} // InitGPIO

// **************************************************************************
//
//  FUNCTION  : CalculateResistance
//
//  I/P       : sPT100_Sensor* ptrPT100 - Pointer to the PT100 structure
//
//  O/P       : None.
//
//  OPERATION : Calculates the resistance reading from the count buffer in
//              the PT100 structure and places it in the structure.
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
static void CalculateResistance(sPT100_Sensor* ptrPT100)
{
  int i;
  int32_t AverageCounts;
  uint32_t milliVolts;
  float Resistance;

  AverageCounts = 0;
  for (i = 0; i < PT100_BUF_SIZE; i++)
  {
    AverageCounts += ptrPT100->Counts[i];
  }
  AverageCounts /= PT100_BUF_SIZE;

  milliVolts = Peripherals_CalculateADCVoltage((int16_t)AverageCounts);
  Resistance = (float)milliVolts / PT100_GAIN;
  Resistance /= PT100_CURRENT;

  ptrPT100->mOhms = (uint32_t)(Resistance * 1000.0);
} // CalculateResistance

// **************************************************************************
//
//  FUNCTION  : CalculateTemperature
//
//  I/P       : sPT100_Sensor*- Pointer to PT100
//              data structure.
//
//  O/P       : None.
//
//  OPERATION : Calculates the temperature of the PT100 from the
//              resistance value
//
//  UPDATED   : 2015-09-20 JHM
//
// **************************************************************************
static void CalculateTemperature(sPT100_Sensor* ptrPT100)
{
  float TemperatureC;

  TemperatureC = (float)ptrPT100->mOhms / 1000.0;
  TemperatureC *= 2.5371;
  TemperatureC -= 253.52;

  ptrPT100->TemperatureC = (int16_t)(TemperatureC * 100.0);
  ptrPT100->TemperatureK = (uint16_t)(ptrPT100->TemperatureC + 27315);
} // CalculateTemperature

// **************************************************************************
//
//  FUNCTION  : BenoitAlgorithm
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Turns the defroster on or off based on Mark's algorithm
//
//  UPDATED   : 2016-05-05 JHM
//
// **************************************************************************
static void BenoitAlgorithm(void)
{
  float Dewpoint;
  float Frostpoint;
  float AirTemp;
  float Humidity;
  float HumTemp;

  // Calculate the AirTemp in C and the RH
  AirTemp = ((float)Thermistor.TemperatureK) / 100.0 - 273.15;
  Humidity = (float)HumiditySensor.CorrectedHumidity / 10.0;
  HumTemp = (float)HumiditySensor.CorrectedTemperature / 100.0;

  // Calculate the dewpoint
  Dewpoint = AirTemp - (100.0 - Humidity) / 5.0;

  // Calculate the frostpoint
  Frostpoint = Dewpoint / 1.1;

  if ((Frostpoint > AirTemp) && ((HumTemp - 1.0) < Frostpoint))
  {
    // Turn the defroster on
    GPIO_SetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
  }
  else
  {
    // Turn the defroster off
    GPIO_ResetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
  }
} // BenoitAlgorithm

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : PT100_Init
//
//  I/P       : sPT100_Sensor* ptrPT100 - Pointer to the PT100 structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the values and state machine of the PT100
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
void PT100_Init(sPT100_Sensor* ptrPT100)
{
  int i;

  // Initialize the structure values
  ptrPT100->Index = 0;
  ptrPT100->Flag_ConversionReady = RESET;

  // Initialize GPIO
  InitGPIO();

  if (HumiditySensor.State == HYT271_ST_OFFLINE)
  {
    PT100_SetMode(ptrPT100, PT100_Mode_Idle);
  }
  else
  {
    // Set the mode to auto
    PT100_SetMode(ptrPT100, PT100_Mode_Auto);
  }

  // Fill the buffer with battery values
  for (i = 0; i < PT100_BUF_SIZE; i++)
  {
    // Select the PT100 channel
    SDADC_InjectedChannelSelect(PT100_SDADC, PT100_SDADC_CH);
    // Start the conversion
    SDADC_SoftwareStartInjectedConv(SDADC1);
    // Wait for the conversion to become available
    while (ptrPT100->Flag_ConversionReady == RESET);
    // Reset the flag
    ptrPT100->Flag_ConversionReady = RESET;
    // The data is already in the buffer, so just increment the index
    ptrPT100->Index = (ptrPT100->Index + 1) % PT100_BUF_SIZE;
  }

  // Calculate the initial battery voltage
  CalculateResistance(ptrPT100);
} // PT100_Init

// **************************************************************************
//
//  FUNCTION  : PT100_Handler
//
//  I/P       : sPT100_Sensor* ptrPT100 - Pointer to the PT100 structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the PT100 sensor.
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
void PT100_Handler(sPT100_Sensor* ptrPT100)
{
  if (ptrPT100->Flag_ConversionReady == SET)
  {
    // Reset the flag
    ptrPT100->Flag_ConversionReady = RESET;

    // Calculate the resistance
    CalculateResistance(ptrPT100);

    // Calcualte the temperature
    CalculateTemperature(ptrPT100);

    // Increment the Index
    ptrPT100->Index = (ptrPT100->Index + 1) % PT100_BUF_SIZE;

    if ((HumiditySensor.State != HYT271_ST_OFFLINE) &&
        (Thermistor.State != NTC_ST_OFFLINE) &&
        (PT100Sensor.Mode == PT100_Mode_Auto))
    {
      // Handle the defroster
      BenoitAlgorithm();
    }
  }
} // PT100_Handler

// **************************************************************************
//
//  FUNCTION  : PT100_SetMode
//
//  I/P       : sPT100_Sensor* ptrPT100 = Pointer to the PT100 structure
//              TypeDef_PT100_Mode Mode =
//                              Available modes:
//                PT100_Mode_Idle = circuit is off
//                PT100_Mode_Measure = ADC is on and temperature is calculated
//                PT100_Mode_Heat = BJT is turned on and the circuit is
//                                  drawing enough current to heat
//                PT100_Mode_Auto = Allows FlightStatus() routine to determine
//                                  whether measuring or heating
//
//  O/P       : None.
//
//  OPERATION : Sets the mode
//
//  UPDATED   : 2016-08-30 JHM
//
// **************************************************************************
void PT100_SetMode(sPT100_Sensor* ptrPT100, TypeDef_PT100_Mode Mode)
{
  switch (Mode)
  {
    case PT100_Mode_Idle:
      // Turn defrost pin off
      GPIO_ResetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
      // Turn circuit off
      GPIO_ResetBits(PT100_ON_PORT, PT100_ON_PIN);
      // Update the structure
      ptrPT100->Mode = Mode;
      break;

    case PT100_Mode_Heat:
      // Turn defrost pin on
      GPIO_SetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
      // Turn PT100 circuit on
      GPIO_SetBits(PT100_ON_PORT, PT100_ON_PIN);
      // Update the structure
      ptrPT100->Mode = Mode;
      break;

    case PT100_Mode_Auto:
      // Turn the circuit on - only the defroster is toggled using Benoit
      // algorithm
      GPIO_SetBits(PT100_ON_PORT, PT100_ON_PIN);
      // Only update the structure. FlightStatus
      ptrPT100->Mode = Mode;
      break;

    case PT100_Mode_Measure:
      // Turn defrost pin off
      GPIO_ResetBits(PT100_DEFROST_PORT, PT100_DEFROST_PIN);
      // Turn PT100 circuit on
      GPIO_SetBits(PT100_ON_PORT, PT100_ON_PIN);
      // Update the structure
      ptrPT100->Mode = Mode;
      break;

    default:
      break;
  } // switch
} // PT100_SetMode
