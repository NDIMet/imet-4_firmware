#ifndef __packets_h__
#define __packets_h__
// **************************************************************************
//
//      International Met Systems
//
//      iMet-1 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   packets.h
//
//      CONTENTS    :   Header file for formatting binary packets
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
#define TASK_SIZE          64
#define IMET1_PTU_SIZE     14
#define IMET1_PTUX_SIZE    20
#define IMET1_GPS_SIZE     18

// *************************************************************************
// TYPES
// *************************************************************************
// Qinetiq Packet structures
typedef struct
{
  uint8_t SVID;
  uint8_t Flags;
  uint8_t AZ;
  uint8_t EL;
  uint8_t CNO;
} sTASK_SV; // 5 BYTES

typedef struct
{
 int16_t PCBtemp;
 uint8_t BattVolts;
 uint8_t GPSfix;
} sTASK_STATUS; // 4 BYTES

typedef struct
{
  uint32_t SyncWord;
  uint16_t PacketNumber;
  uint16_t Pressure;
  uint16_t Temperature;
  uint16_t Humidity;
  uint8_t Info[4];
  sTASK_SV  GPSSatellite[4];
  uint16_t PCB_Temp;
  uint8_t Battery;
  uint8_t GPS_Fix;
  uint32_t Time;
  int32_t XPosition;
  int32_t YPosition;
  int32_t ZPosition;
  int16_t XVelocity;
  int16_t YVelocity;
  int16_t ZVelocity;
  uint16_t Checksum;
} sTASK_Packet;

typedef union
{
  sTASK_Packet Packet;
  uint8_t Byte[TASK_SIZE];
} uTASK_Packet;

typedef struct
{
  uint8_t SOH;
  uint8_t PKT_ID;
  uint8_t PKT;
  uint32_t Pressure;
  int16_t Temperature;
  int16_t Humidity;
  uint8_t BatteryVoltage;
  uint16_t Checksum;
} sIMET1_PTU_Packet;

typedef struct
{
  uint8_t SOH;
  uint8_t PKT_ID;
  float Latitude;
  float Longitude;
  int16_t Altitude;
  uint8_t Satellites;
  uint32_t Time;
  uint16_t Checksum;
} sIMET1_GPS_Packet;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern uTASK_Packet TXmsgOut[4];
extern uint8_t *TXmsgPointer;
extern uint32_t TXmsgByteNumber;
extern uint32_t TXmsgNumber;

extern uint8_t iMet1_PTU_Packet[IMET1_PTU_SIZE];
extern uint8_t iMet1_PTUX_Packet[IMET1_PTUX_SIZE];
extern uint8_t iMet1_GPS_Packet[IMET1_GPS_SIZE];
extern uint16_t iMet1_PacketNumber;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Packets_Init(TypeDef_ModulationMode Mode);
void Packets_Build_TASK(sUBX_NAV_SOL* ptrSOL);
void Packets_Build_iMet1(void);

#endif
