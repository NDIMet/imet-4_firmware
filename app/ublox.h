#ifndef __UBLOX_H__
#define __UBLOX_H__
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   Ublox.h
//
//      CONTENTS    :   Header file for Ublox Protocol
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Sensor State Machine
#define  UBX_STATUS_OFFLINE    0
#define  UBX_STATUS_INIT       1
#define  UBX_STATUS_ONLINE     2
#define  UBX_STATUS_ERROR      3

// Serial Port Peripheral Configuration
#define  UBX_USART             USART2
#define  UBX_USART_RCC         RCC_APB1Periph_USART2
#define  UBX_USART_IRQ         USART2_IRQn
#define  UBX_BAUDRATE          9600

// Port IO Hardware Configuration
#define  UBX_PORT              GPIOA
#define  UBX_TX_PIN            GPIO_Pin_2
#define  UBX_RX_PIN            GPIO_Pin_3
#define  UBX_PORT_RCC          RCC_AHBPeriph_GPIOA
#define  UBX_TX_PIN_SRC        GPIO_PinSource2
#define  UBX_RX_PIN_SRC        GPIO_PinSource3
#define  UBX_TX_AF             GPIO_AF_7
#define  UBX_RX_AF             GPIO_AF_7

// Buffer and message information
#define  MAX_GPS_SATELLITES     16
#define  UBX_MSG_BUF_SIZE       6
#define  UBX_SOL_BUF_SIZE       4
#define  UBX_POSLLH_BUF_SIZE    2
#define  UBX_SAT_BUF_SIZE       2
#define  UBX_MSG_MAX            1024
#define  UBX_TX_SIZE            128
#define  UBX_RX_SIZE            1024

// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  uint8_t State;
  uint16_t Index;
  uint8_t Message[UBX_MSG_MAX];
  uint8_t Class;
  uint8_t ID;
  uint16_t PayloadLength;
  uint16_t Counter;
  uint16_t Checksum;
  FlagStatus Flag_LSB;
} sUBX_MSG;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t Buffer[UBX_TX_SIZE];
} sUBX_TX;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t Buffer[UBX_RX_SIZE];
} sUBX_RX;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  sUBX_MSG Message[UBX_MSG_BUF_SIZE];
} sUBX_MSG_BUF;

typedef struct
{
  uint8_t gnssId;
  uint8_t svId;
  uint8_t cno;
  int8_t elev;
  int16_t azim;
  int16_t prRes;
  uint32_t flags;
  uint8_t Flag_TASK;
} sUBX_SAT_BLOCK;

typedef struct
{
  uint32_t iTOW;
  uint8_t version;
  uint8_t numSvs;
  sUBX_SAT_BLOCK Satellites[16];
} sUBX_NAV_SAT;

typedef struct
{
  uint32_t iTOW;
  int32_t fTOW;
  int16_t week;
  uint8_t gpsFix;
  uint8_t flags;
  int32_t ecefX;
  int32_t ecefY;
  int32_t ecefZ;
  uint32_t pAcc;
  int32_t ecefVX;
  int32_t ecefVY;
  int32_t ecefVZ;
  uint32_t sAcc;
  uint16_t pDOP;
  uint8_t numSV;
} sUBX_NAV_SOL;

typedef struct
{
  uint32_t iTOW;
  int32_t lon;
  int32_t lat;
  int32_t hEES;
  int32_t hMSL;
  uint32_t hAcc;
  uint32_t vAcc;
} sUBX_NAV_POSLLH;

typedef struct
{
  uint8_t Start;
  uint8_t End;
  sUBX_NAV_SOL Buffer[UBX_SOL_BUF_SIZE];
} sUBX_SOL_Buffer;

typedef struct
{
  uint8_t Start;
  uint8_t End;
  sUBX_NAV_POSLLH Buffer[UBX_POSLLH_BUF_SIZE];
} sUBX_POSLLH_Buffer;

typedef struct
{
  uint8_t Start;
  uint8_t End;
  sUBX_NAV_SAT Buffer[UBX_SAT_BUF_SIZE];
} sUBX_SAT_Buffer;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern uint8_t UBX_Status;

extern sUBX_TX GPS_TX_Buffer;
extern sUBX_RX GPS_RX_Buffer;

extern FlagStatus Flag_SOL_Ready;
extern FlagStatus Flag_POSLLH_Ready;
extern FlagStatus Flag_GPS_Valid;
extern FlagStatus Flag_SAT_Ready;

extern sUBX_MSG_BUF UBX_MSG_Buffer;
extern sUBX_SOL_Buffer UBX_SOL_Buffer;
extern sUBX_POSLLH_Buffer UBX_POSLLH_Buffer;
extern sUBX_SAT_Buffer UBX_SAT_Buffer;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Ublox_Init(void);
void Ublox_WatchdogReset(void);
void Ublox_GNSS_Start(void);
void Ublox_GNSS_Stop(void);
void Ublox_TxHandler(sUBX_TX* Buffer);
void Ublox_RxHandler(sUBX_RX* Buffer);
void Ublox_Transmit(uint8_t* Data, uint16_t Length);
void Ublox_SendByte(uint8_t Byte);
void Ublox_Handler(void);

#endif
