// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   battery.c
//
//      CONTENTS    :   Routines for initializing and handling battery
//                      voltage measurement
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
// None.

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sBATTERY Battery;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void CalculateBatteryVoltage(sBATTERY* ptrBattery);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : CalculateBatteryVoltage
//
//  I/P       : int16_t Count = Conversion value from SDADC
//
//  O/P       : uint16_t = Battery voltage in millivolts
//
//  OPERATION : Calculates the battery voltage based on a conversion value
//
//  UPDATED   : 2016-04-14 JHM
//
// **************************************************************************
static void CalculateBatteryVoltage(sBATTERY* ptrBattery)
{
  int i;
  int32_t AverageCounts;
  uint16_t milliVolts;

  AverageCounts = 0;
  for (i = 0; i < BATT_BUF_SIZE; i++)
  {
    AverageCounts += ptrBattery->Counts[i];
  }
  AverageCounts /= BATT_BUF_SIZE;

  ptrBattery->milliVoltsADC = Peripherals_CalculateADCVoltage((int16_t)AverageCounts);

  milliVolts = ptrBattery->milliVoltsADC;
  milliVolts *= (BATT_R1 + BATT_R3);
  milliVolts /= BATT_R3;

  ptrBattery->milliVoltsBattery = milliVolts;
} // CalculateBatteryVoltage

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : Battery_Init
//
//  I/P       : sBATTERY* ptrBattery = Pointer to battery data structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the battery voltage data structure with values.
//
//  UPDATED   : 2016-04-15 JHM
//
// **************************************************************************
void Battery_Init(sBATTERY* ptrBattery)
{
  uint16_t i;
  
  // Initialize the structure variables
  ptrBattery->State = BATT_ST_OK;
  ptrBattery->Index = 0;
  ptrBattery->Counter = 0;
  ptrBattery->milliVoltsADC = 0;
  ptrBattery->milliVoltsBattery = 0;
  ptrBattery->Flag_ConversionReady = RESET;
  ptrBattery->Flag_LowBattery = RESET;
  
  // Fill the buffer with battery values
  for (i = 0; i < BATT_BUF_SIZE; i++)
  {
    // Select the injected channel
    SDADC_InjectedChannelSelect(SDADC1, BATTV_SDADC_CH);
    // Start the conversion
    SDADC_SoftwareStartInjectedConv(SDADC1);
    // Wait for the conversion to become available
    while (ptrBattery->Flag_ConversionReady == RESET);
    // Reset the flag
    ptrBattery->Flag_ConversionReady = RESET;
    // The data is already in the buffer, so just increment the index
    ptrBattery->Index = (ptrBattery->Index + 1) % BATT_BUF_SIZE;
  }
  
  // Calculate the initial battery voltage
  CalculateBatteryVoltage(ptrBattery);

} // Battery_Init

// **************************************************************************
//
//  FUNCTION  : Battery_Handler
//
//  I/P       : sBATTERY* ptrBattery = Pointer to battery data structure
//
//  O/P       : None.
//
//  OPERATION : Handles the battery voltage calculating and reporting. This 
//              routine should be called continuously by main. It will only
//              do something if the interrupt for the ADC has triggered and
//              set the conversion ready flag to HIGH.
//
//  UPDATED   : 2017-03-21 JHM
//
// **************************************************************************
void Battery_Handler(sBATTERY* ptrBattery)
{
  if (ptrBattery->Flag_ConversionReady == RESET)
  {
    return;
  }

  // Reset the flag since we are now handling
  ptrBattery->Flag_ConversionReady = RESET;

  // Calculate the battery voltage
  CalculateBatteryVoltage(ptrBattery);

  // Increment the Index
  ptrBattery->Index = (ptrBattery->Index + 1) % BATT_BUF_SIZE;

  switch (ptrBattery->State)
  {
    case BATT_ST_OK:
      if (ptrBattery->milliVoltsBattery < BATT_LOWBATTER_SP)
      {
        // Increment the counter
        ptrBattery->Counter++;
      }
      else
      {
        // Reset the counter
        ptrBattery->Counter = 0;
      }
      if (ptrBattery->Counter > BATT_TICKER_THRESHOLD)
      {
        // Advance the state machine
        ptrBattery->State = BATT_ST_LOW;
        // Reset the counter
        ptrBattery->Counter = 0;
        // Set the flag
        ptrBattery->Flag_LowBattery = SET;
      }
      break;

    case BATT_ST_LOW:
      if (ptrBattery->milliVoltsBattery < BATT_KILL_SP)
      {
        // Increment the counter
        ptrBattery->Counter++;
      }
      else
      {
        // Reset the counter
        ptrBattery->Counter = 0;
      }
      if (ptrBattery->Counter > BATT_TICKER_THRESHOLD )
      {
        ptrBattery->State = BATT_ST_KILL;
      }
      break;

    case BATT_ST_KILL:
      // Turn off the transmitter
      CC115L_SetPowerAmpLevel(&Transmitter, RFPA_PWR_OFF);
      // Hasta la vista baby - unplugging the battery
      GPIO_ResetBits(POWER_PORT, POWER_PIN);
      // Via con Dios
      while (1);
      break;
    } // switch
} // Battery_Init
