#ifndef __PT100_H
#define __PT100_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   PT100.H
//
//      CONTENTS    :   Header file for PT100 and defroster circuit routines
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
#define PT100_BUF_SIZE               10

// GPIO Hardware
#define PT100_ON_PORT                GPIOA
#define PT100_ON_PIN                 GPIO_Pin_6
#define PT100_ON_RCC                 RCC_AHBPeriph_GPIOA

#define PT100_DEFROST_PORT           GPIOA
#define PT100_DEFROST_PIN            GPIO_Pin_5
#define PT100_DEFROST_RCC            RCC_AHBPeriph_GPIOA

// Circuit Component values
#define PT100_R15_VALUE              2000
#define PT100_GAIN                   15.0
#define PT100_CURRENT                1.5       //mA
#define PT100_R0                     100.0
#define PT100_COEFFICIENTA           3.9083e-3
#define PT100_COEFFICIENTB           -5.775e-7
#define PT100_COEFFICIENTC           -4.183e-12

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  PT100_Mode_Idle = 0,
  PT100_Mode_Heat = 1,
  PT100_Mode_Auto = 2,
  PT100_Mode_Measure = 3
} TypeDef_PT100_Mode;

typedef struct
{
  TypeDef_PT100_Mode Mode;
  uint16_t Index;
  int16_t Counts[PT100_BUF_SIZE];
  FlagStatus Flag_ConversionReady;
  uint32_t mOhms;
  uint16_t TemperatureK;
  int16_t TemperatureC;
} sPT100_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sPT100_Sensor PT100Sensor;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void PT100_Init(sPT100_Sensor* ptrPT100);
void PT100_Handler(sPT100_Sensor* ptrPT100);
void PT100_SetMode(sPT100_Sensor* ptrPT100, TypeDef_PT100_Mode Mode);

#endif
