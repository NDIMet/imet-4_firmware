#ifndef __MODULATION_H
#define __MODULATION_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   modulation.h
//
//      CONTENTS    :   Header file for modulating the data to the
//                      transmitter
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// TASK Modulation info
#define FM0_CLK              4096

// Modulation Output Pin
#define MOD0_PORT            GPIOA
#define MOD0_PIN             GPIO_Pin_12
#define MOD0_GPIO_RCC        RCC_AHBPeriph_GPIOA

// Modulation Timer Hardware
#define MOD0_TIM             TIM2
#define MOD0_TIM_RCC         RCC_APB1Periph_TIM2

// iMet-1 Modulation Timer
#define IMET1_TIM            TIM3
#define IMET1_TIM_RCC        RCC_APB1Periph_TIM3
#define IMET1_TIM_CLK        26400
#define IMET1_BUF_SIZE       255

// Bell202 Frequencies
#define BELL202_MARKF        1200
#define BELL202_SPACEF       2200
#define BELL202_BAUD         1200

// BAUD_CCR_VAL = IMET1_TIM_CLK / BELL202_BAUD = 26400 / 1200 = 22
// MARK_CCR_VAL = IMET1_TIM_CLK / BELL202_MARKF / 2 = 26400 / 1200 / 2 = 11
// SPACE_CCR_VAL = IMET1_TIM_CLK / BELL202_SPACEF / 2 = 26400 / 2200 / 2 = 6
#define IMET1_BAUD_CCR_VAL   22
#define IMET1_MARK_CCR_VAL   11
#define IMET1_SPACE_CCR_VAL  6

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  MODULATION_MODE_OFF = 0,
  MODULATION_MODE_IMET1 = 1,
  MODULATION_MODE_TASK = 2
} TypeDef_ModulationMode;

typedef struct
{
  uint16_t Start;
  uint16_t End;
  uint8_t Buffer[IMET1_BUF_SIZE];
  uint16_t CCR_Value;
  uint8_t BitCounter;
} siMet1Modulation;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern FlagStatus Flag_Build_TASK;
extern TypeDef_ModulationMode ModulationMode;
extern siMet1Modulation iMet1Modulation;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Modulation_Init(void);
void Modulation_Enable(void);
void Modulation_Disable(void);
void Modulation_TASK_Handler(void);
void Modulation_iMet1_Handler(void);
void Modulation_iMet1_Send(uint8_t* Bytes, uint16_t Length);

#endif
