#ifndef __HYT217_H
#define __HYT217_H
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HYT217.H
//
//      CONTENTS    :   Header file for Innovative Sensor Technology HYT 271
//                      humidity sensor
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************

// I2C Address
//       0x28 << 1
#define  I2C_HYT271_ID          0x50


// Humidity Sensor State Machine
#define  HYT271_ST_OFFLINE      0
#define  HYT271_ST_START        1
#define  HYT271_ST_READ         2
#define  HYT271_ST_LOAD         3
#define  HYT271_ST_IDLE         4



// *************************************************************************
// TYPES
// *************************************************************************
typedef struct
{
  // I2C Channel
  I2C_TypeDef* I2Cx;
  // State Machine
  uint8_t State;
  // Wait Channel
  uint8_t WaitChannel;
  // ID
  uint8_t ID;
  // Communications Error Flag
  FlagStatus Flag_CommsFault;
  // Data Stale Flag
  FlagStatus Flag_Stale;
  // Counts
  uint16_t HCount;
  uint16_t TCount;
  // Raw sensor data
  int16_t RawHumidity;
  int16_t RawTemperature;
  // Corrected data
  int16_t CorrectedHumidity;
  int16_t CorrectedTemperature;
  // AT/UT Difference
  int16_t DeltaT;
} sHYT271_Sensor;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sHYT271_Sensor HumiditySensor;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void HYT271_Init(sHYT271_Sensor* ptrHumidity);
void HYT271_Handler(sHYT271_Sensor* ptrHumidity);

#endif
