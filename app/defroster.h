#ifndef __DEFROSTER_H
#define __DEFROSTER_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   DEFROSTER.H
//
//      CONTENTS    :   Header file for defroster circuit routines
//
// **************************************************************************

// *************************************************************************
// CONSTANTS
// *************************************************************************
// GPIO Hardware
#define DF_PORT           GPIOA
#define DF_PIN            GPIO_Pin_5
#define DF_RCC            RCC_AHBPeriph_GPIOA

#define DF_THRESHOLD      2.0

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  DF_Mode_Off = 0,
  DF_Mode_On = 1,
  DF_Mode_Benoit = 2,
  DF_Mode_Meulenberg = 3
} TypeDef_DF_Mode;


typedef struct
{
  TypeDef_DF_Mode Mode;
  FlagStatus Flag_DefrosterOn;
  FlagStatus Flag_Forced;
} sDEFROSTER;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sDEFROSTER Defroster;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void Defroster_Init(sDEFROSTER* ptrDF);
void Defroster_Handler(sDEFROSTER* ptrDF);
void Defroster_SetMode(sDEFROSTER* ptrDF, TypeDef_DF_Mode Mode);

#endif
