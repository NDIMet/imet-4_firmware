// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   mfg.c
//
//      CONTENTS    :   Routines for communication and control of the
//                      manufacturing UART port.
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//        CONSTANTS
// **************************************************************************

// *************************************************************************
//        TYPES
// *************************************************************************
#define  CMD_TYPE_NULL           0
#define  CMD_TYPE_GET            1
#define  CMD_TYPE_SET            2

// Error Messages
#define  ERR_COMMAND             "ERR01\r\n"
#define  ERR_TYPE                "ERR02\r\n"
#define  ERR_PARAM               "ERR03\r\n"
#define  ERR_FLASH               "ERR04\r\n"
#define  ERR_FAIL                "ERR05\r\n"

#define  BOOT_LOCATION           0x1FFFD800

// **************************************************************************
//
//       GLOBAL VARIABLES
//
// **************************************************************************
// *************************************************************************
sMFG_TX TX_Buffer;
sMFG_RX RX_Buffer;
FlagStatus Flag_GPS_Loopback = RESET;
Type_ASCII_Mode ASCII_Mode = ASCII_OFF;

// **************************************************************************
//
//       PRIVATE VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitUSART(void);
static void ClearTxBuffer(sMFG_TX* Buffer);
static void ClearRxBuffer(sMFG_RX* Buffer);
static void MessageHandler(sMFG_RX* Buffer);
static uint8_t GetCommandType(char* Message);
// Handler Routines
static void RST_Handler(char* Message);
static void SAV_Handler(char* Message);
static void VER_Handler(char* Message);
static void SWN_Handler(char* Message);
static void GPS_Handler(char* Message);
static void SRN_Handler(char* Message);
static void STX_Handler(char* Message, uint8_t CoefficientNumber);
static void CPB_Handler(char* Message);
static void CTP_Handler(char* Message);
static void SPP_Handler(char* Message);
static void SPT_Handler(char* Message);
static void SPH_Handler(char* Message);
static void SOL_Handler(char* Message);
static void LLH_Handler(char* Message);
static void SAT_Handler(char* Message);
static void BAT_Handler(char* Message);
static void VDD_Handler(char* Message);
static void TXP_Handler(char* Message);
static void TXF_Handler(char* Message);
static void TXX_Handler(char* Message, uint8_t Channel);
static void TXM_Handler(char* Message);
static void TXD_Handler(char* Message);
static void HTR_Handler(char* Message);
static void HSP_Handler(char* Message);
static void PWM_Handler(char* Message);
static void DFM_Handler(char* Message);
static void XDATA_Handler(char* Message);
static void DAT_Handler(char* Message);

// **************************************************************************
//
//        PRIVATE VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : InitUSART
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes User Interface Serial Port
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void InitUSART(void)
{
  USART_InitTypeDef USART_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  NVIC_InitTypeDef NVIC_InitStructure;

  // Enable Clocks
  RCC_AHBPeriphClockCmd(MFG_PORT_RCC, ENABLE);
  RCC_APB2PeriphClockCmd(MFG_USART_RCC, ENABLE);

  // Configure Pins
  GPIO_InitStructure.GPIO_Pin = MFG_TX_PIN | MFG_RX_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;

  GPIO_Init(MFG_PORT, &GPIO_InitStructure);

  // Set pins to alternate functions
  GPIO_PinAFConfig(MFG_PORT, MFG_TX_PIN_SRC, MFG_TX_AF);
  GPIO_PinAFConfig(MFG_PORT, MFG_RX_PIN_SRC, MFG_RX_AF);

  // Configure Serial Hardware
  USART_InitStructure.USART_BaudRate = MFG_BAUDRATE;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;

  USART_Init(MFG_USART, &USART_InitStructure);

  // Initialize Interrupts
  NVIC_InitStructure.NVIC_IRQChannel = MFG_USART_IRQ;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  // Set priority to low (still higher than LEDs and waiting)
  //NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
  NVIC_Init(&NVIC_InitStructure);

  // Disable transmit interrupt (we will enable prior to transmission)
  USART_ITConfig(MFG_USART, USART_IT_TXE, DISABLE);
  // Wait for transmission to become available
  while (USART_GetFlagStatus(MFG_USART, USART_FLAG_TC) == RESET);

  // Enable receive interrupt
  USART_ITConfig(MFG_USART, USART_IT_RXNE, ENABLE);
  // Wait for receiver to become available
  while (USART_GetFlagStatus(MFG_USART, USART_IT_RXNE) != RESET);

  // Enable USART
  USART_Cmd(MFG_USART, ENABLE);
} // InitUSART

// **************************************************************************
//
//  FUNCTION  : ClearTxBuffer
//
//  I/P       : sMFG_TX* Buffer = Pointer to the manufacturing port buffer
//
//  O/P       : None.
//
//  OPERATION : Clears the data in the manufacturing TX buffer and resets to
//              initial values.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void ClearTxBuffer(sMFG_TX* Buffer)
{
  int i;

  // Disable the UART peripheral
  USART_Cmd(MFG_USART, DISABLE);

  Buffer->Start = 0;
  Buffer->End = 0;

  for (i = 0; i < MFG_TX_BUF_SIZE; i++)
  {
    Buffer->TxBuffer[i] = 0;
  }

  // Enable the UART peripheral
  USART_Cmd(MFG_USART, ENABLE);
} // ClearTxBuffer

// **************************************************************************
//
//  FUNCTION  : ClearRxBuffer
//
//  I/P       : sMFG_RX* Buffer = Pointer to the manufacturing port buffer
//
//  O/P       : None.
//
//  OPERATION : Clears the data in the manufacturing RX buffer and resets to
//              initial values.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void ClearRxBuffer(sMFG_RX* Buffer)
{
  int i;

  // Disable the UART peripheral
  USART_Cmd(MFG_USART, DISABLE);

  // Set buffer values to defaults
  Buffer->Start = 0;
  Buffer->End = 0;
  Buffer->MsgAvailable = 0;

  for (i = 0; i < MFG_RX_BUF_SIZE; i++)
  {
    Buffer->RxBuffer[i] = 0;
  }

  // Enable the UART peripheral
  USART_Cmd(MFG_USART, ENABLE);
} // ClearRxBuffer

// **************************************************************************
//
//  FUNCTION  : MessageHandler
//
//  I/P       : sMFG_RX* Buffer = Pointer to manufacturing port RX buffer.
//
//  O/P       : None.
//
//  OPERATION : This routine should be called whenever there is a message in
//              the RX queue. It reads the next available message from the
//              buffer and directs it to the appropriate command handle
//              routine.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static void MessageHandler(sMFG_RX* Buffer)
{
  int i;
  char Message[MFG_RX_BUF_SIZE + 1];

  // Exit immediately if there are no messages available
  if (!Buffer->MsgAvailable)
  {
    return;
  }

  for (i = 0; i < sizeof(Message); i++)
  {
    Message[i] = Buffer->RxBuffer[Buffer->Start];
    if (Message[i] == 0)
    {
      // Increment the buffer start index
      Buffer->Start = (Buffer->Start + 1) % MFG_RX_BUF_SIZE;
      // Exit the loop
      break;
    }
    else
    {
      // Increment the buffer start index
      Buffer->Start = (Buffer->Start + 1) % MFG_RX_BUF_SIZE;
    }
  }

  // Convert string to all uppercase
  StrUpr(Message);

  if (strstr(Message, "/RST")){ RST_Handler(Message); }
  else if (strstr(Message, "/SAV")){ SAV_Handler(Message); }
  else if (strstr(Message, "/VER")){ VER_Handler(Message); }
  else if (strstr(Message, "/SWN")){ SWN_Handler(Message); }
  else if (strstr(Message, "/GPS")){ GPS_Handler(Message); }
  else if (strstr(Message, "/SRN")){ SRN_Handler(Message); }
  else if (strstr(Message, "/ST0")){ STX_Handler(Message, 0); }
  else if (strstr(Message, "/ST1")){ STX_Handler(Message, 1); }
  else if (strstr(Message, "/ST2")){ STX_Handler(Message, 2); }
  else if (strstr(Message, "/ST3")){ STX_Handler(Message, 3); }
  else if (strstr(Message, "/CPB")){ CPB_Handler(Message); }
  else if (strstr(Message, "/CTP")){ CTP_Handler(Message); }
  else if (strstr(Message, "/SPP")){ SPP_Handler(Message); }
  else if (strstr(Message, "/SPT")){ SPT_Handler(Message); }
  else if (strstr(Message, "/SPH")){ SPH_Handler(Message); }
  else if (strstr(Message, "/SOL")){ SOL_Handler(Message); }
  else if (strstr(Message, "/LLH")){ LLH_Handler(Message); }
  else if (strstr(Message, "/SAT")){ SAT_Handler(Message); }
  else if (strstr(Message, "/BAT")){ BAT_Handler(Message); }
  else if (strstr(Message, "/VDD")){ VDD_Handler(Message); }
  else if (strstr(Message, "/TXP")){ TXP_Handler(Message); }
  else if (strstr(Message, "/TXF")){ TXF_Handler(Message); }
  else if (strstr(Message, "/TX0")){ TXX_Handler(Message, 0); }
  else if (strstr(Message, "/TX1")){ TXX_Handler(Message, 1); }
  else if (strstr(Message, "/TX2")){ TXX_Handler(Message, 2); }
  else if (strstr(Message, "/TX3")){ TXX_Handler(Message, 3); }
  else if (strstr(Message, "/TX4")){ TXX_Handler(Message, 4); }
  else if (strstr(Message, "/TX5")){ TXX_Handler(Message, 5); }
  else if (strstr(Message, "/TX6")){ TXX_Handler(Message, 6); }
  else if (strstr(Message, "/TXM")){ TXM_Handler(Message); }
  else if (strstr(Message, "/TXD")){ TXD_Handler(Message); }
  else if (strstr(Message, "/HTR")){ HTR_Handler(Message); }
  else if (strstr(Message, "/HSP")){ HSP_Handler(Message); }
  else if (strstr(Message, "/PWM")){ PWM_Handler(Message); }
  else if (strstr(Message, "/DFM")){ DFM_Handler(Message); }
  else if (strstr(Message, "XDATA")){ XDATA_Handler(Message);}
  else if (strstr(Message, "/DAT")){ DAT_Handler(Message); }
  // else if (strstr(Message, "/STOP")){ while (1) {}; }               // used to test watchdog
  else
  {
    // Notify the user
    Mfg_TransmitMessage(ERR_COMMAND);
  }

} // Mfg_MessageHandler

// **************************************************************************
//
//  FUNCTION  : GetCommandType
//
//  I/P       : char* Message = Pointer to a string
//
//  O/P       : uint8_t = Message Type
//              MSG_TYPE_CMD = Command
//              MSG_TYPE_GET = Read parameter
//              MSG_TYPE_SET = Set parameter
//
//  OPERATION : Determines the message type from a string.
//
//  UPDATED   : 2016-04-01 JHM
//
// **************************************************************************
static uint8_t GetCommandType(char* Message)
{
  if (strchr(Message, '=') != NULL)
  {
    return CMD_TYPE_SET;
  }
  else if (strchr(Message, '?') != NULL)
  {
    return CMD_TYPE_GET;
  }
  else
  {
    return CMD_TYPE_NULL;
  }
} // GetCommandType

// **************************************************************************
//
//  FUNCTION  : RST_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Immediately resets the iMet-X
//
//  UPDATED   : 2015-06-11 JHM
//
// **************************************************************************
static void RST_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch (Type)
  {
    case CMD_TYPE_NULL:
      // Enable the Watchdog Peripheral
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_WWDG, ENABLE);
      // Enable the counter
      WWDG_Enable(WWDG_MAX_CNT);
      // Set watchdog timer to a value that will immediately cause reset
      WWDG_SetCounter(0x3F);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
  }
} // RST_Hander

// **************************************************************************
//
//  FUNCTION  : SAV_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the save to flash command
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SAV_Handler(char* Message)
{
  uint8_t Type;
  FLASH_Status FlashResult;

  // Get the command type
  Type = GetCommandType(Message);

  switch (Type)
  {
    case CMD_TYPE_NULL:
      // Disable the peripherals so that no code is executed during flash
      // write
      Peripherals_Disable();

      DelayMs(1);

      // Save the current flash configuration
      FlashResult = Config_Save();

      DelayMs(1);

      // Turn the peripherals back on
      Peripherals_Enable();

      if (FlashResult == FLASH_COMPLETE)
      {
        Mfg_TransmitMessage("OK\r\n");
      }
      else
      {
        Mfg_TransmitMessage(ERR_FLASH);
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SAV_Hander

// **************************************************************************
//
//  FUNCTION  : VER_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Polls the firmware version.
//
//  UPDATED   : 2016-11-16 JHM
//
// **************************************************************************
static void VER_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch (Type)
  {
    case CMD_TYPE_NULL:
      Mfg_TransmitMessage("VER=");
      Mfg_TransmitMessage(FirmwareVersion);
      Mfg_TransmitMessage("\r\n");
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
  }
} // VER_Hander

// **************************************************************************
//
//  FUNCTION  : SWN_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Polls the software part number.
//
//  UPDATED   : 2018-02-27 JHM
//
// **************************************************************************
static void SWN_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch (Type)
  {
    case CMD_TYPE_NULL:
      Mfg_TransmitMessage("SWN=");
      Mfg_TransmitMessage(SoftwareNumber);
      Mfg_TransmitMessage("\r\n");
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
  }
} // SWN_Hander

// **************************************************************************
//
//  FUNCTION  : GPS_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Puts the manufacturing port into GPS loopback mode. This
//              allows the manufacturing port to communicate directly with
//              the GPS engine and bypasses all other manufacturing commands.
//
//  UPDATED   : 2016-12-07 JHM
//
// **************************************************************************
static void GPS_Handler(char* Message)
{
  uint8_t Type;
  USART_InitTypeDef USART_InitStructure;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type != CMD_TYPE_NULL)
  {
    Mfg_TransmitMessage(ERR_TYPE);
    return;
  }

  // Set the flag
  Flag_GPS_Loopback = SET;

  // Disable manufacturing UART to change the baud rate
  USART_Cmd(MFG_USART, DISABLE);
  // Configure Serial Hardware
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
  USART_Init(MFG_USART, &USART_InitStructure);
  // Enable the manufacturing UART at the new baud rate
  USART_Cmd(MFG_USART, ENABLE);
}

// **************************************************************************
//
//  FUNCTION  : SRN_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the serial number command
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SRN_Handler(char* Message)
{
  uint32_t SerialNumber;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_GET:
      sprintUnsignedNumber(variable, iQ_Config.SerialNumber, 8);
      string[0] = 0;
      StrCat(string, "SRN=");
      StrCat(string, variable);
      StrCat(string, "\r\n");
      Mfg_TransmitMessage(string);
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      SerialNumber = (uint32_t)ReadInt(Message);

      if ((SerialNumber == 0) || (SerialNumber > 99999999))
      {
        Mfg_TransmitMessage(ERR_PARAM);
      }
      else
      {
        iQ_Config.SerialNumber = (uint32_t)SerialNumber;
        sprintUnsignedNumber(variable, iQ_Config.SerialNumber, 8);
        string[0] = 0;
        StrCat(string, "SRN=");
        StrCat(string, variable);
        StrCat(string, "\r\n");
        Mfg_TransmitMessage(string);
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SRN_Hander


// **************************************************************************
//
//  FUNCTION  : STX_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the steinhart-hart coefficient read/write commands.
//
//  UPDATED   : 2015-06-19 JHM
//
// **************************************************************************
static void STX_Handler(char* Message, uint8_t CoefficientNumber)
{
  double Coefficient;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Coefficient = ReadDouble(Message);

      if (CoefficientNumber == 0)
      {
        Thermistor.Coefficients.A0 = Coefficient;
        iQ_Config.ST0 = Thermistor.Coefficients.A0;
      }
      else if (CoefficientNumber == 1)
      {
        Thermistor.Coefficients.A1 = Coefficient;
        iQ_Config.ST1 = Thermistor.Coefficients.A1;
      }
      else if (CoefficientNumber == 2)
      {
        Thermistor.Coefficients.A2 = Coefficient;
        iQ_Config.ST2 = Thermistor.Coefficients.A2;
      }
      else if (CoefficientNumber == 3)
      {
        Thermistor.Coefficients.A3 = Coefficient;
        iQ_Config.ST3 = Thermistor.Coefficients.A3;
      }
      break;

    case CMD_TYPE_GET:
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  }

  // Initialize the string
  string[0] = 0;

  switch(CoefficientNumber)
  {
    case 0:
      StrCat(string, "ST0=");
      sprintScientific(variable, Thermistor.Coefficients.A0, 8);
      //sprintf(variable,"%+.8e",Thermistor.Coefficients.A0);
      StrCat(string, variable);
      StrCat(string, "\r\n");
      break;

    case 1:
      StrCat(string, "ST1=");
      sprintScientific(variable, Thermistor.Coefficients.A1, 8);
      StrCat(string, variable);
      StrCat(string, "\r\n");
      break;

    case 2:
      StrCat(string, "ST2=");
      sprintScientific(variable, Thermistor.Coefficients.A2, 8);
      StrCat(string, variable);
      StrCat(string, "\r\n");
       break;

    case 3:
      StrCat(string, "ST3=");
      sprintScientific(variable, Thermistor.Coefficients.A3, 8);
      StrCat(string, variable);
      StrCat(string, "\r\n");
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch (CoefficientNumber)

  Mfg_TransmitMessage(string);
} // STX_Handler


// **************************************************************************
//
//  FUNCTION  : CPB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the pressure bias adjustment command
//
//  UPDATED   : 2016-04-15 JHM
//
// **************************************************************************
static void CPB_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_GET:
      sprintSignedNumber(variable, iQ_Config.PressureBiasCorrection, 4);
      string[0] = 0;
      StrCat(string, "CPB=");
      StrCat(string, variable);
      StrCat(string, "\r\n");
      Mfg_TransmitMessage(string);
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      iQ_Config.PressureBiasCorrection = (int16_t)ReadInt(Message);
      PressureSensor.BiasCorrection = iQ_Config.PressureBiasCorrection;

      sprintSignedNumber(variable, iQ_Config.PressureBiasCorrection, 4);
      string[0] = 0;
      StrCat(string, "CPB=");
      StrCat(string, variable);
      StrCat(string, "\r\n");
      Mfg_TransmitMessage(string);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SRN_Hander

// **************************************************************************
//
//  FUNCTION  : CTP_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the command to calibrate the parallel resistor. In
//              order for this routine to return successful, the temperature
//              probe must be detached.
//
//  UPDATED   : 2016-06-14 JHM
//
// **************************************************************************
static void CTP_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch (Type)
  {
    case CMD_TYPE_NULL:
      // Calibrate the parallel resistance
      NTC_CalibrateParallelR(&Thermistor);
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      iQ_Config.ParallelResistorT = (uint32_t)ReadInt(Message);
      Thermistor.ParallelResistance = (double)iQ_Config.ParallelResistorT;
      break;

    case CMD_TYPE_GET:
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  // Notify success or failure
  if (Thermistor.Flag_Error)
  {
    // Clear the error flag since we are handling it
    Thermistor.Flag_Error = RESET;
    Mfg_TransmitMessage(ERR_FAIL);
  }
  else
  {
    string[0] = 0;
    StrCat(string, "CTP=");
    sprintUnsignedNumber(variable, (uint32_t)Thermistor.ParallelResistance, 7);
    StrCat(string, variable);
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
} // CTP_Hander

// **************************************************************************
//
//  FUNCTION  : SPP_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll pre
//
//  UPDATED   : 2015-06-12 JHM
//
// **************************************************************************
static void SPP_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_NULL:
      string[0] = 0;
      StrCat(string, "SPP=");
      sprintSignedNumber(variable, PressureSensor.SensorData.Pressure, 6);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintSignedNumber(variable, PressureSensor.SensorData.Temperature, 4);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintUnsignedNumber(variable, PressureSensor.SensorData.FilteredP, 8);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintUnsignedNumber(variable, PressureSensor.SensorData.FilteredT, 8);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintSignedNumber(variable, PressureSensor.CorrectedPressure, 6);
      StrCat(string, variable);

      StrCat(string, "\r\n");
      Mfg_TransmitMessage(string);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SPP_Hander

// **************************************************************************
//
//  FUNCTION  : SPT_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll temperature sensor command.
//
//  UPDATED   : 2016-06-16 JHM
//
// **************************************************************************
static void SPT_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type == CMD_TYPE_NULL)
  {
    string[0] = 0;
    StrCat(string, "SPT=");
    sprintSignedNumber(variable, (Thermistor.TemperatureK - 27315), 5);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintScientific(variable, Thermistor.Resistance, 4);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintScientific(variable, Thermistor.CorrectedResistance, 4);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintUnsignedNumber(variable,Thermistor.NTC_Count, 8);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintUnsignedNumber(variable,Thermistor.Reference_Count, 8);
    StrCat(string, variable);
    // TEST CODE
    StrCat(string, ",");
    sprintUnsignedNumber(variable,(uint32_t)Thermistor.EquivalentResistance, 7);
    StrCat(string, variable);
    // TEST CODE
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
  else
  {
    Mfg_TransmitMessage(ERR_TYPE);
  }
} // SPT_Handler

// **************************************************************************
//
//  FUNCTION  : SPH_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll humidity sensor command.
//
//  UPDATED   : 2016-04-13 JHM
//
// **************************************************************************
static void SPH_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_NULL:
      string[0] = 0;
      StrCat(string, "SPH=");
      // Add humidity
      sprintSignedNumber(variable, HumiditySensor.RawHumidity, 4);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintSignedNumber(variable, HumiditySensor.RawTemperature, 4);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintSignedNumber(variable, HumiditySensor.CorrectedHumidity, 4);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintSignedNumber(variable, HumiditySensor.CorrectedTemperature, 4);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintUnsignedNumber(variable, HumiditySensor.HCount, 5);
      StrCat(string, variable);
      StrCat(string, ",");
      sprintUnsignedNumber(variable, HumiditySensor.TCount, 5);
      StrCat(string, variable);
      StrCat(string, "\r\n");
      Mfg_TransmitMessage(string);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
} // SPH_Hander

// **************************************************************************
//
//  FUNCTION  : SOL_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll SOL command
//
//  UPDATED   : 2016-11-16 JHM
//
// **************************************************************************
static void SOL_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type == CMD_TYPE_NULL)
  {
    // Initialize the array
    string[0] = 0;
    StrCat(string, "SOL=");
    sprintSignedNumber(variable, UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefX, 10);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintSignedNumber(variable, UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefY, 10);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintSignedNumber(variable, UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].ecefZ, 10);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintUnsignedNumber(variable, UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].numSV, 2);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintUnsignedNumber(variable, UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].iTOW, 10);
    StrCat(string, variable);
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
  else
  {
    Mfg_TransmitMessage(ERR_TYPE);
  }
} // SOL_Handler

// **************************************************************************
//
//  FUNCTION  : LLH_Handler
//
//  I/P       : char* Message = pointer to the message to handle
//
//  O/P       : None.
//
//  OPERATION : Handles the poll latitude/longitude/height command.
//
//  UPDATED   : 2016-07-20 JHM
//
// **************************************************************************
static void LLH_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type == CMD_TYPE_NULL)
  {
    // Initialize the array
    string[0] = 0;
    StrCat(string, "LLH=");
    sprintSignedNumber(variable, UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lat, 10);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintSignedNumber(variable, UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lon, 10);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintSignedNumber(variable, UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].hEES, 8);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintSignedNumber(variable, UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].hMSL, 8);
    StrCat(string, variable);
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
  else
  {
    Mfg_TransmitMessage(ERR_TYPE);
  }
} // LLH_Handler

// **************************************************************************
//
//  FUNCTION  : SAT_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll GPS satellites command.
//
//  UPDATED   : 2016-11-17 JHM
//
// **************************************************************************
static void SAT_Handler(char* Message)
{
  uint8_t Type;
  int i;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type == CMD_TYPE_NULL)
  {
    // Initialize the array
    string[0] = 0;
    StrCat(string, "SAT=");
    // Time of week (ms)
    sprintUnsignedNumber(variable, UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].iTOW / 1000, 6);
    StrCat(string, variable);
    StrCat(string, ",");
    // Number of satellites
    sprintUnsignedNumber(variable, UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].numSvs, 2);
    StrCat(string, variable);

    for (i = 0; i < UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].numSvs; i++)
    {
      if (UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[i].gnssId == 0)
      {
        StrCat(string, ",[");
        sprintUnsignedNumber(variable, UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[i].svId, 2);
        StrCat(string, variable);
        StrCat(string, "]");
        sprintUnsignedNumber(variable, UBX_SAT_Buffer.Buffer[UBX_SAT_Buffer.Start].Satellites[i].cno, 2);
        StrCat(string, variable);
      }
    }
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
  else
  {
    Mfg_TransmitMessage(ERR_TYPE);
  }
} // SAT_Handler

// **************************************************************************
//
//  FUNCTION  : BAT_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the poll battery command.
//
//  UPDATED   : 2016-06-16 JHM
//
// **************************************************************************
static void BAT_Handler(char* Message)
{
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  if (Type == CMD_TYPE_NULL)
  {
    string[0] = 0;
    StrCat(string, "BAT=");
    sprintUnsignedNumber(variable, Battery.milliVoltsBattery, 4);
    StrCat(string, variable);
    StrCat(string, "\r\n");
    Mfg_TransmitMessage(string);
  }
  else
  {
    Mfg_TransmitMessage(ERR_TYPE);
  }
} // BAT_Handler

// **************************************************************************
//
//  FUNCTION  : VDD_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the digital voltage calibration command.
//
//  UPDATED   : 2016-06-27 JHM
//
// **************************************************************************
static void VDD_Handler(char* Message)
{
  uint8_t Type;
  uint16_t milliVolts;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize Array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      milliVolts = (uint16_t)ReadInt(Message);
      // Validate power setting
      if ((milliVolts < 2700) || (milliVolts > 3300))
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      // Set VDD
      ADC_Coefficients.VDD = milliVolts;
      iQ_Config.VDD = milliVolts;
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  StrCat(string, "VDD=");
  sprintUnsignedNumber(variable, ADC_Coefficients.VDD, 4);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // VDD_Handler

// **************************************************************************
//
//  FUNCTION  : TXP_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the change transmission power command.
//
//  UPDATED   : 2016-06-15 JHM
//
// **************************************************************************
static void TXP_Handler(char* Message)
{
  uint8_t Type;
  uint8_t Power;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize Array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Power = (uint8_t)ReadInt(Message);
      // Validate power setting
      if (Power > 5)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }

      if (Power == 0)
      {
        // Set transmitter state to idle
        CC115L_SetState(&Transmitter, DISABLE);
      }
      else
      {
        // Set to transmit
        CC115L_SetState(&Transmitter, ENABLE);
      }

      if (Power == 5)
      {
        // Set global structure variables
        Transmitter.Flag_AutoPowerLevel = SET;
        Transmitter.PowerLevel = RFPA_PWR_1;
        // Set configuration
        iQ_Config.Flag_AutoPowerLevel = SET;
        iQ_Config.PowerLevel = RFPA_PWR_1;
      }
      else
      {
        // Set the current power level
        CC115L_SetPowerAmpLevel(&Transmitter, (TypeDef_RFPA_PowerLevel)Power);
        CC115L_SetAutoLevel(&Transmitter, RESET);
        iQ_Config.PowerLevel = Power;
        iQ_Config.Flag_AutoPowerLevel = RESET;
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  StrCat(string, "TXP=");
  if (Transmitter.PowerLevel == RFPA_PWR_OFF)
  {
    StrCat(string, "0");
  }
  else if (Transmitter.PowerLevel == RFPA_PWR_1)
  {
    StrCat(string, "1");
  }
  else if (Transmitter.PowerLevel == RFPA_PWR_2)
  {
    StrCat(string, "2");
  }
  else if (Transmitter.PowerLevel == RFPA_PWR_3)
  {
    StrCat(string, "3");
  }
  else if (Transmitter.PowerLevel == RFPA_PWR_4)
  {
    StrCat(string, "4");
  }

  StrCat(string, ",");

  if (Transmitter.Flag_AutoPowerLevel)
  {
    StrCat(string, "1");
  }
  else
  {
    StrCat(string, "0");
  }

  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // TXP_Handler

// **************************************************************************
//
//  FUNCTION  : TXF_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Sets the current frequency channel to the frequency specified
//              by the user.
//
//  UPDATED   : 2016-06-22 JHM
//
// **************************************************************************
static void TXF_Handler(char* Message)
{
  uint8_t Type;
  uint16_t Frequency;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize Array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Frequency = (uint16_t)ReadInt(Message);
      if (Frequency < 4020 || Frequency > 4060)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }

      switch (FrequencyChannels.Channel)
      {
        case 0:
          iQ_Config.TxChannel0 = (uint16_t)Frequency;
          break;

        case 1:
          iQ_Config.TxChannel1 = (uint16_t)Frequency;
          break;

        case 2:
          iQ_Config.TxChannel2 = (uint16_t)Frequency;
          break;

        case 3:
          iQ_Config.TxChannel3 = (uint16_t)Frequency;
          break;

        case 4:
          iQ_Config.TxChannel4 = (uint16_t)Frequency;
          break;

        case 5:
          iQ_Config.TxChannel5 = (uint16_t)Frequency;
          break;

        case 6:
          iQ_Config.TxChannel6 = (uint16_t)Frequency;
          break;

        default:
          break;
      }
      FrequencyChannels.Frequencies[FrequencyChannels.Channel] = Frequency;
      // Change the frequency values in the register
      CC115L_SetFrequency(&Transmitter, Frequency);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  // Initialize the string
  string[0] = 0;
  StrCat(string, "TXF=");
  sprintUnsignedNumber(variable, FrequencyChannels.Frequencies[FrequencyChannels.Channel], 4);
  StrCat(string, variable);
  StrCat(string, "\r\n");

  Mfg_TransmitMessage(string);
} // TXF_Handler

// **************************************************************************
//
//  FUNCTION  : TXX_Handler
//
//  I/P       : char* Message = Pointer to the message
//              uint8_t Channel = Channel to set
//                      TX_CHANNEL_0
//                      TX_CHANNEL_1
//                      TX_CHANNEL_2
//                      TX_CHANNEL_3
//
//  O/P       : None.
//
//  OPERATION : Sets the transmission channel to the frequency specified.
//
//  UPDATED   : 2016-09-07 JHM
//
// **************************************************************************
static void TXX_Handler(char* Message, uint8_t Channel)
{
  uint16_t Frequency;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Frequency = ReadInt(Message);

      if (Frequency < 4000 || Frequency > 4100)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }

      // Set the frequency in the global variable and the configuration
      FrequencyChannels.Frequencies[Channel] = Frequency;

      switch (Channel)
      {
        case 0:
          iQ_Config.TxChannel0 = (uint16_t)Frequency;

          break;
        case 1:
          iQ_Config.TxChannel1 = (uint16_t)Frequency;
          break;
        case 2:
          iQ_Config.TxChannel2 = (uint16_t)Frequency;
          break;
        case 3:
          iQ_Config.TxChannel3 = (uint16_t)Frequency;
          break;
        case 4:
          iQ_Config.TxChannel4 = (uint16_t)Frequency;
          break;
        case 5:
          iQ_Config.TxChannel5 = (uint16_t)Frequency;
          break;
        case 6:
          iQ_Config.TxChannel6 = (uint16_t)Frequency;
          break;
        default:
          break;
      }

      // Set the new frequency value if we are on the channel we are changing
      if (Channel == FrequencyChannels.Channel)
      {
        CC115L_SetFrequency(&Transmitter, Frequency);
      }
      break;

    case CMD_TYPE_GET:
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  }

  // Initialize the string
  string[0] = 0;
  StrCat(string, "TX");
  sprintUnsignedNumber(variable, Channel, 1);
  StrCat(string, variable);
  StrCat(string, "=");
  sprintUnsignedNumber(variable, FrequencyChannels.Frequencies[Channel], 4);
  StrCat(string, variable);
  StrCat(string, "\r\n");

  Mfg_TransmitMessage(string);
} // TXX_Handler

// **************************************************************************
//
//  FUNCTION  : TXM_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the message to get/set the modulation mode.
//
//  UPDATED   : 2016-07-20 JHM
//
// **************************************************************************
static void TXM_Handler(char* Message)
{
  TypeDef_ModulationMode Mode;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Mode = (TypeDef_ModulationMode)ReadInt(Message);

      switch (Mode)
      {
        case MODULATION_MODE_OFF:
          // Disable the modulation
          Modulation_Disable();
          // Set the modulation line low
          GPIO_ResetBits(MOD0_PORT, MOD0_PIN);
          // Set the modulation formation to OOK
          CC115L_SetModulationFormat(&Transmitter, CC115L_MF_OOK);
          ModulationMode = Mode;
          iQ_Config.ModulationMode = Mode;
          break;
        case MODULATION_MODE_IMET1:
          // Disable the modulation
          Modulation_Disable();
          // Switch the mode in the global variable and flash configuration
          ModulationMode = Mode;
          iQ_Config.ModulationMode = Mode;
          // Set the transmitter to Gaussian Frequency Shift Keying
          CC115L_SetModulationFormat(&Transmitter, CC115L_MF_GFSK);
          // Turn the modulation back on
          Modulation_Init();
          Modulation_Enable();
          break;

        case MODULATION_MODE_TASK:
          ModulationMode = Mode;
          iQ_Config.ModulationMode = Mode;
          break;

        default:
          // Invalid modulation mode, send error
          Mfg_TransmitMessage(ERR_PARAM);
          return;

      } // switch
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      break;
  } // switch
  StrCat(string, "TXM=");
  sprintUnsignedNumber(variable, ModulationMode, 1);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // TXM_Handler

// **************************************************************************
//
//  FUNCTION  : TXB_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the message to set the bandwidth of the transmitter.
//
//  UPDATED   : 2016-09-09 JHM
//
// **************************************************************************
static void TXD_Handler(char* Message)
{
  uint8_t kiloHertz;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      kiloHertz = (uint8_t)ReadInt(Message);

      if ((kiloHertz < 1) || (kiloHertz > 160))
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      else
      {
        // Set the transmitter object variable
        Transmitter.Deviation = kiloHertz;
        // Update the configuration
        iQ_Config.Deviation = (uint16_t)kiloHertz;
        // Change the deviation of the transmitter
        CC115L_SetDeviation(&Transmitter, kiloHertz);
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  sprintUnsignedNumber(variable, iQ_Config.Deviation, 3);
  string[0] = 0;
  StrCat(string, "TXD=");
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // TXD_Handler

// **************************************************************************
//
//  FUNCTION  : HTR_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the message for the internal heater control.
//
//  UPDATED   : 2016-06-21 JHM
//
// **************************************************************************
static void HTR_Handler(char* Message)
{
  uint8_t Mode;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Mode = (uint8_t)ReadInt(Message);

      if (Mode > 2)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      else
      {
        // Set the new heater mode
        HVAC_SetHeaterMode(&Heater, (TypeDef_HeaterMode)Mode);
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch
  StrCat(string, "HTR=");
  sprintUnsignedNumber(variable, Heater.Mode, 1);
  StrCat(string, variable);
  StrCat(string, ",");
  sprintUnsignedNumber(variable, Heater.Flag_HeaterOn, 1);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // HTR_Handler

// **************************************************************************
//
//  FUNCTION  : HSP_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the message for the thermostat setting.
//
//  UPDATED   : 2016-11-16 JHM
//
// **************************************************************************
static void HSP_Handler(char* Message)
{
  int8_t SetPoint;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");

      // Check for zero
      if ((strcmp(Message, "0") == 0) || (strcmp(Message, "00") == 0))
      {
        SetPoint = 0;
      }
      else
      {
        // Get the new setpoint
        SetPoint = (int8_t)ReadInt(Message);

        if (!SetPoint)
        {
          // User has not entered a valid number
          Mfg_TransmitMessage(ERR_PARAM);
          return;
        }
        else if (SetPoint > 50)
        {
          // Too high
          Mfg_TransmitMessage(ERR_PARAM);
          return;
        }
        else if (SetPoint < -99)
        {
          // Too low
          Mfg_TransmitMessage(ERR_PARAM);
          return;
        }
      }
      // Set the global data structure and configuration value
      Heater.SetPoint = SetPoint;
      iQ_Config.HeaterSetPoint = SetPoint;
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  // Send the response
  StrCat(string, "HSP=");
  sprintSignedNumber(variable, Heater.SetPoint,2);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // HSP_Handler

// **************************************************************************
//
//  FUNCTION  : PWM_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles pulse-width-modulation setpoint of the heaters.
//
//  UPDATED   : 2016-12-08 JHM
//
// **************************************************************************
static void PWM_Handler(char* Message)
{
  int8_t PWM_Value;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");

      // Get the new setpoint
      PWM_Value = (int8_t)ReadInt(Message);

      if (PWM_Value < 0)
      {
        // User has not entered a valid number
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      else if (PWM_Value > 100)
      {
        // Too high
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }

      // Set the global data structure and configuration value
      Heater.PWM_Value = PWM_Value;
      iQ_Config.PWM_Value = PWM_Value;

      if (Heater.PWM_Value == 100)
      {
        HVAC_DisablePWM();
      }
      else if (Heater.PWM_Value > 0)
      {
        HVAC_EnablePWM();
      }
      else
      {
        HVAC_DisablePWM();
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  // Send the response
  StrCat(string, "PWM=");
  sprintUnsignedNumber(variable, Heater.PWM_Value, 3);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // PWM_Handler

// **************************************************************************
//
//  FUNCTION  : DFM_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the message to get/set the defrost mode.
//
//  UPDATED   : 2016-09-20 JHM
//
// **************************************************************************
static void DFM_Handler(char* Message)
{
  uint8_t Mode;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Mode = (uint8_t)ReadInt(Message);

      if (Mode > DF_Mode_Meulenberg)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      else
      {
        // Set the new heater mode
        Defroster_SetMode(&Defroster, (TypeDef_DF_Mode)Mode);
        // Set the config
        iQ_Config.Defrost_Mode = (TypeDef_DF_Mode)Mode;
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch
  StrCat(string, "DFM=");
  sprintUnsignedNumber(variable, Defroster.Mode, 1);
  StrCat(string, variable);
  StrCat(string, ",");
  sprintUnsignedNumber(variable, Defroster.Flag_DefrosterOn, 1);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // DFM_Handler

// **************************************************************************
//
//  FUNCTION  : XDATA_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the XDATA message.
//
//  UPDATED   : 2018-02-27 JHM
//
// **************************************************************************
static void XDATA_Handler(char* Message)
{
  uint8_t Type;
  char* start;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      start = strchr(Message,'=');
      start++;
      // Pass this along to XData
      XDATA_Build(&xData, start);
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch

  //Mfg_TransmitMessage("OK\r\n");
} // DFM_Handler

// **************************************************************************
//
//  FUNCTION  : DAT_Handler
//
//  I/P       : char* Message = Pointer to the message
//
//  O/P       : None.
//
//  OPERATION : Handles the message to get/set the ASCII output mode.
//
//  UPDATED   : 2018-04-05 JHM
//
// **************************************************************************
static void DAT_Handler(char* Message)
{
  uint8_t Mode;
  uint8_t Type;

  // Get the command type
  Type = GetCommandType(Message);

  // Initialize the array
  string[0] = 0;

  switch(Type)
  {
    case CMD_TYPE_GET:
      break;

    case CMD_TYPE_SET:
      // Read up to "="
      Message = strtok(Message, "=\r");
      // Read between "=" and "\r"
      Message = strtok(NULL, "=\r");
      Mode = (uint8_t)ReadInt(Message);

      if (Mode > ASCII_I4)
      {
        Mfg_TransmitMessage(ERR_PARAM);
        return;
      }
      else
      {
        // Set the new heater mode
        ASCII_Mode = (Type_ASCII_Mode)Mode;
        // Set the config
        iQ_Config.ASCII_Mode = ASCII_Mode;
      }
      break;

    default:
      Mfg_TransmitMessage(ERR_TYPE);
      return;
  } // switch
  StrCat(string, "DAT=");
  sprintUnsignedNumber(variable, ASCII_Mode, 1);
  StrCat(string, variable);
  StrCat(string, "\r\n");
  Mfg_TransmitMessage(string);
} // DAT_Handler

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : Mfg_Init
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes UART port of the manufacturing interface.
//
//  UPDATED   : 2016-03-31 JHM
//
// **************************************************************************
void Mfg_Init(void)
{
  // Set the GPS flag to not loopback
  Flag_GPS_Loopback = RESET;

  // Clear the buffers, which resets all the values
  ClearTxBuffer(&TX_Buffer);
  ClearRxBuffer(&RX_Buffer);

  // Get the ASCII mode from flash
  ASCII_Mode = iQ_Config.ASCII_Mode;

  // Initialize the USART peripheral
  InitUSART();
} // Mfg_Init

// **************************************************************************
//
//  FUNCTION  : Mfg_ClearTxBuffer
//
//  I/P       : sMFG_TX* Buffer = Pointer to transmit buffer structure
//
//  O/P       : None.
//
//  OPERATION : Resets the tx buffer to the default values
//
//  UPDATED   : 2016-09-01 JHM
//
// **************************************************************************
void Mfg_ClearTxBuffer(sMFG_TX* Buffer)
{
  ClearTxBuffer(Buffer);
} // Mfg_ClearTxBuffer

// **************************************************************************
//
//  FUNCTION  : Mfg_ClearRxBuffer
//
//  I/P       : sMFG_RX* Buffer = Pointer to transmit buffer structure
//
//  O/P       : None.
//
//  OPERATION : Resets the rx buffer to the default values
//
//  UPDATED   : 2016-09-01 JHM
//
// **************************************************************************
void Mfg_ClearRxBuffer(sMFG_RX* Buffer)
{
  ClearRxBuffer(Buffer);
} // Mfg_ClearRxBuffer

// **************************************************************************
//
//  FUNCTION  : Mfg_Handler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION :
//
//  UPDATED   : 2016-03-31 JHM
//
// **************************************************************************
void Mfg_Handler(sMFG_RX* Buffer)
{
  // Check if there is a message to process
  if (Buffer->MsgAvailable > 0)
  {
    // Handle the message
    MessageHandler(Buffer);

    // Decrement the message counter since we have handled it
    Buffer->MsgAvailable--;
  }
} // Mfg_Handler

// **************************************************************************
//
//  FUNCTION  : Mfg_TxHandler
//
//  I/P       : sMFG_TX* Buffer = Pointer to UART transmission data buffer
//
//  O/P       : None.
//
//  OPERATION : This routine should be called whenever a TXE interrupt has
//              occurred and there is data in the buffer to send. It loads
//              the data into the UART, waits for the byte to be sent using
//              the TXE flag, then increments the start of the TX buffer.
//
//  UPDATED   : 2016-03-31 JHM
//
// **************************************************************************
void Mfg_TxHandler(sMFG_TX* Buffer)
{
  // Send the data
  USART_SendData(MFG_USART, Buffer->TxBuffer[Buffer->Start]);

  // Wait for the transmission to end
  while (USART_GetFlagStatus(MFG_USART, USART_FLAG_TXE == RESET));

  // Increment the start index (safe)
  //Buffer->Start = (Buffer->Start + 1) % MFG_TX_BUF_SIZE;
  Buffer->Start++;
  if (MFG_TX_BUF_SIZE == Buffer->Start)
  {
    Buffer->Start = 0;
  }

  // End the transmission if the start and end index are the same
  if (Buffer->Start == Buffer->End)
  {
    // Disable the interrupt
    USART_ITConfig(MFG_USART, USART_IT_TXE, DISABLE);
  }
} // Mfg_TxHandler

// **************************************************************************
//
//  FUNCTION  : Mfg_RxHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the receive interrupt
//
//  UPDATED   : 2015-04-02 JHM
//
// **************************************************************************
void Mfg_RxHandler(sMFG_RX* Buffer)
{
  // Move the received data into the buffer
  Buffer->RxBuffer[Buffer->End] = USART_ReceiveData(MFG_USART);
  // Clear RXNE flag to prevent overflow error
  USART_ClearFlag(MFG_USART, USART_FLAG_RXNE);

  if (Buffer->RxBuffer[Buffer->End] == '\r')
  {
    Buffer->MsgAvailable++;
    // Increment the end index (safe)
    Buffer->End++;
    if (MFG_RX_BUF_SIZE == Buffer->End)
    {
      Buffer->End = 0;
    }
    // Terminate with null
    Buffer->RxBuffer[Buffer->End] = 0;
  }

  // Increment the end index (safe)
  Buffer->End++;
  if (MFG_RX_BUF_SIZE == Buffer->End)
  {
    Buffer->End = 0;
  }
} // Mfg_RxHandler

// **************************************************************************
//
//  FUNCTION  : Mfg_TransmitMessage
//
//  I/P       : char* message = Pointer to message array
//
//  O/P       : None.
//
//  OPERATION : Transmits a message via the external USART
//
//  UPDATED   : 2015-04-15 JHM
//
// **************************************************************************
void Mfg_TransmitMessage(char* Message)
{
  int i;

  for (i = 0; i < strlen(Message); i++)
  {
    TX_Buffer.TxBuffer[TX_Buffer.End] = Message[i];
    //TX_Buffer.End = (TX_Buffer.End + 1) % MFG_TX_BUF_SIZE;
    TX_Buffer.End++;
    if (MFG_TX_BUF_SIZE == TX_Buffer.End)
    {
      TX_Buffer.End = 0;
    }
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(MFG_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(MFG_USART, USART_IT_TXE, ENABLE);
  }
} // Mfg_TransmitMessage

// **************************************************************************
//
//  FUNCTION  : Mfg_SendByte
//
//  I/P       : uint8_t  = Byte to send
//
//  O/P       : None.
//
//  OPERATION : Transmits a single byte out the external USART
//
//  UPDATED   : 2016-05-12 JHM
//
// **************************************************************************
void Mfg_SendByte(uint8_t Byte)
{
  // Move the byte into the tx buffer
  TX_Buffer.TxBuffer[TX_Buffer.End] = Byte;
  // Increment the buffer
  //TX_Buffer.End = (TX_Buffer.End + 1) % MFG_TX_BUF_SIZE;
  TX_Buffer.End++;
  if (MFG_TX_BUF_SIZE == TX_Buffer.End)
  {
    TX_Buffer.End = 0;
  }

  // Enable the TX empty interrupt (which should immediately occur)
  if (USART_GetITStatus(MFG_USART, USART_IT_TXE) == RESET)
  {
    USART_ITConfig(MFG_USART, USART_IT_TXE, ENABLE);
  }
} // Mfg_SendByte

// **************************************************************************
//
//  FUNCTION  : Mfg_TransmitASCII
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Sends out an ASCII message based on the ASCII mode. This
//              function should be called each second.
//
//  UPDATED   : 2018-04-05 JHM
//
// **************************************************************************
void Mfg_TransmitASCII(void)
{
  uint32_t Time;
  uint8_t hms;
  double fValue;

  if (ASCII_Mode == ASCII_OFF)
  {
    return;
  }
  else
  {
    // Initialize the string
    string[0] = 0;
  }

  if (ASCII_Mode == ASCII_RSD)
  {
    StrCat(string, "PTUX: ");
    sprintf(variable,"%0.2f",((float)PressureSensor.CorrectedPressure) / 100.0);
    StrCat(string, variable);
    StrCat(string, ", ");
    sprintf(variable, "%0.2f",((float)Thermistor.TemperatureC) / 100.0);
    StrCat(string, variable);
    StrCat(string, ", ");
    sprintf(variable, "%0.1f", ((float)HumiditySensor.CorrectedHumidity) / 10.0);
    StrCat(string, variable);
    StrCat(string, ", ");
    sprintf(variable, "%0.2f", ((float)HumiditySensor.CorrectedTemperature) / 100.0);
    StrCat(string, variable);
    StrCat(string, "\r\n");
  }
  else if (ASCII_Mode == ASCII_I4)
  {
    // Print SRN
    sprintf(string, "%lu", iQ_Config.SerialNumber);
    StrCat(string, ",");

    // Calculate hh:mm:ss
    // Convert from milliseconds to seconds
    Time = UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].iTOW / 1000;
    // Hours
    hms = (Time / 3600) % 24;
    sprintUnsignedNumber(variable, hms, 2);
    StrCat(string, variable);
    StrCat(string, ":");
    // Minutes
    hms = (Time / 60) % 60;
    sprintUnsignedNumber(variable, hms, 2);
    StrCat(string, variable);
    StrCat(string, ":");
    // Seconds
    hms = Time % 60;
    sprintUnsignedNumber(variable, hms, 2);
    StrCat(string, variable);
    StrCat(string, ",");

    // Sensor Data
    sprintf(variable,"%0.2f",((float)PressureSensor.CorrectedPressure) / 100.0);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintf(variable, "%0.2f",((float)Thermistor.TemperatureC) / 100.0);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintf(variable, "%0.1f", ((float)HumiditySensor.CorrectedHumidity) / 10.0);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintf(variable, "%0.2f", ((float)HumiditySensor.CorrectedTemperature) / 100.0);
    StrCat(string, variable);
    StrCat(string, ",");

    // GPS Data
    fValue = ((double)UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lat) / 1.0e7;
    sprintf(variable, "%0.7lf", fValue);
    StrCat(string, variable);
    StrCat(string, ",");
    fValue = ((double)UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].lon) / 1.0e7;
    sprintf(variable, "%0.7lf", fValue);
    StrCat(string, variable);
    StrCat(string, ",");
    fValue = ((double)UBX_POSLLH_Buffer.Buffer[UBX_POSLLH_Buffer.Start].hMSL) / 1.0e3;
    sprintf(variable, "%0.1lf", fValue);
    StrCat(string, variable);
    StrCat(string, ",");
    sprintf(variable, "%d", UBX_SOL_Buffer.Buffer[UBX_SOL_Buffer.Start].numSV);
    StrCat(string, variable);

    StrCat(string, "\r\n");

  }

  Mfg_TransmitMessage(string);

}

