// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   HYT271.C
//
//      CONTENTS    :   Routines for initialization and communication with the
//                      HYT271 Humidity Sensor (I2C Interface)
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        LOCAL CONSTANTS
//
// **************************************************************************
#define  BIT_CMODE          0x80
#define  BIT_STALE          0x40

// ITS-90 Saturation Vapor Pressure over Water Coefficients
#define  ITS90_G0           -2.8365744e3
#define  ITS90_G1           -6.028076559e3
#define  ITS90_G2           1.954263612e1
#define  ITS90_G3           -2.737830188e-2
#define  ITS90_G4           1.6261698e-5
#define  ITS90_G5           7.0229056e-10
#define  ITS90_G6           -1.8680009e-13
#define  ITS90_G7           2.7150305

// *************************************************************************
//        LOCAL TYPES
// *************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
sHYT271_Sensor HumiditySensor;

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void MeasurementRequest(sHYT271_Sensor* ptrHumidity);
static void DataFetch(sHYT271_Sensor* ptrHumidity);
static void ProcessCounts(sHYT271_Sensor* ptrHumidity);
static double CalculateSaturationVapourPressure(double TemperatureK);
static void TranslateHumidity(sHYT271_Sensor* ptrHumidity, sThermistor* ptrThermistor);
static void CalculateHumidityValues(sHYT271_Sensor* ptrHumidity);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : MeasurementRequest
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Sends the Measurement Request via the I2C channel to start the
//              measurement. The method is to send the address with the write
//              bit set - immediately followed by a stop bit.
//
//  UPDATED   : 2015-07-09 JHM
//
// **************************************************************************
static void MeasurementRequest(sHYT271_Sensor* ptrHumidity)
{
  // Clear the flags we will be using
  I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_STOPF | I2C_FLAG_NACKF);

  // Set up the transfer. Since the number of data bytes is zero, the transfer
  // will send the start, address, and stop all at once
  I2C_TransferHandling(ptrHumidity->I2Cx, I2C_HYT271_ID, 0, I2C_AutoEnd_Mode, I2C_Generate_Start_Write);

  // Wait for transmission to complete
  while ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_STOPF) == RESET) && I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF == RESET));

  // Check for device acknowledge
  if (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == SET)
  {
    // Reset the NACKF
    I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);
    ptrHumidity->Flag_CommsFault = SET;
  }
} // MeasurementRequest

// **************************************************************************
//
//  FUNCTION  : DataFetch
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Sends the Data Fetch Request via the I2C channel to read the
//              measurement. The method is to send the address with the write
//              bit set - immediately followed by a stop bit.
//
//  UPDATED   : 2015-07-13 JHM
//
// **************************************************************************
static void DataFetch(sHYT271_Sensor* ptrHumidity)
{
  uint16_t i, Timeout;
  uint8_t Data[4];

  // Clear the NACK flag if it has been set
  I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);

  // Set up the transfer (immediately sends START and slave address - waits for ACK)
  I2C_TransferHandling(ptrHumidity->I2Cx, I2C_HYT271_ID, 4, I2C_AutoEnd_Mode, I2C_Generate_Start_Read);

  for (i = 0; i < 4; i++)
  {
    Timeout = 1000;
    // Wait for data to become available
    while ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_RXNE) == RESET) &&
           (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == RESET) &&
           Timeout){Timeout--;}

    if ((I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_NACKF) == SET) || (Timeout == 0))
    {
      // Clear the NACK flag
      I2C_ClearFlag(ptrHumidity->I2Cx, I2C_FLAG_NACKF);
      // Send the error to the humidity structure
      ptrHumidity->Flag_CommsFault = SET;
      return;
    }
    else
    {
      // Read the data
      Data[i] = I2C_ReceiveData(ptrHumidity->I2Cx);
    }
  }

  // Wait for transfer to end
  while (I2C_GetFlagStatus(ptrHumidity->I2Cx, I2C_FLAG_STOPF) == RESET);

  if (Data[0] & BIT_STALE)
  {
    ptrHumidity->Flag_Stale = SET;
  }
  else
  {
    ptrHumidity->Flag_Stale = RESET;
  }

  // Move humidity counts into structure. Mask off first two bits (status bits)
  ptrHumidity->HCount = (Data[0] << 8) & 0x3FFF;
  ptrHumidity->HCount |= Data[1];
  // Move temperature counts into structure
  ptrHumidity->TCount = (Data[2] << 8);
  ptrHumidity->TCount |= Data[3];
  ptrHumidity->TCount = ptrHumidity->TCount >> 2;
} // DataFetch

// **************************************************************************
//
//  FUNCTION  : ProcessCounts
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Calculates the humidity and temperature values and moves them
//              into the data structure. This function must be performed after
//              the DataFetch() routine, since it assumes the counts are
//              already in the data structure.
//
//  UPDATED   : 2015-07-13 JHM
//
// **************************************************************************
static void ProcessCounts(sHYT271_Sensor* ptrHumidity)
{
  float Humidity;
  float Temperature;

  // Calculate Humidity
  Humidity = ((float)ptrHumidity->HCount * 100.0) / 16383.0;
  // Calculate Temperature
  Temperature = (((float)ptrHumidity->TCount * 165.0) / 16383.0) - 40.0;

  // Convert from float to decimal
  ptrHumidity->RawHumidity = (int16_t)(round(Humidity * 10.0));
  ptrHumidity->RawTemperature = (int16_t)(round(Temperature * 100.0));
} // ProcessCounts

// **************************************************************************
//
//  FUNCTION  : CalculateSaturationVapourPressure
//
//  I/P       : double Temperature = Temperature in Kelvin
//
//  O/P       : None.
//
//  OPERATION : Calculates the saturation vapor pressure in Pa using the
//              ITS-90 formulations described in "ITS-90 FORMULATIONS FOR VAPOR
//              PRESSURE,FROSTPOINT TEMPERATURE,DEWPOINT TEMPERATURE,AND
//              ENHANCEMENT FACTORS IN THE RANGE -100 TO +100 C." by Bob Hardy.
//
//  UPDATED   : 2017-05-10 JHM
//
// **************************************************************************
static double CalculateSaturationVapourPressure(double Temperature)
{
  double SVP = 0.0;

  SVP += ITS90_G0 * pow(Temperature, -2.0);
  SVP += ITS90_G1 * pow(Temperature, -1.0);
  SVP += ITS90_G2;
  SVP += ITS90_G3 * Temperature;
  SVP += ITS90_G4 * pow(Temperature, 2);
  SVP += ITS90_G5 * pow(Temperature, 3);
  SVP += ITS90_G6 * pow(Temperature, 4);
  SVP += ITS90_G7 * log(Temperature);

  SVP = exp(SVP);

  return SVP;
} // CalculateSaturationVapourPressure

// **************************************************************************
//
//  FUNCTION  : TranslateHumidity
//
//  I/P       : sHYT271_Sensor* ptrHumidity = Pointer to humidity struct
//              sThermistor* ptrThermistor = Pointer to thermistor data struct
//
//  O/P       : None.
//
//  OPERATION : Translates the humidity of the HYT271 sensor object pointed to
//              from the sensor's raw temperature to the temperature of the
//              thermistor object. It stores the translated humidity into
//              ptrHumidity->TranslatedHumidity.
//
//  UPDATED   : 2017-07-04 JHM
//
// **************************************************************************
static void TranslateHumidity(sHYT271_Sensor* ptrHumidity, sThermistor* ptrThermistor)
{
  double TranslatedHumidity, HumiditySVP, AirSVP;
  double SensorHumidity = ((double)ptrHumidity->RawHumidity) / 10.0;
  double HumidityTemperature = ((double)(ptrHumidity->CorrectedTemperature + 27315)) / 100.0;
  double AirTemperature = ((double)ptrThermistor->TemperatureK) / 100.0;

  // Calculate the saturation vapor pressure of the humidity temperature
  HumiditySVP = CalculateSaturationVapourPressure(HumidityTemperature);

  // Calculate the saturation vapor pressure of the air temperature
  AirSVP = CalculateSaturationVapourPressure(AirTemperature);

  // Translate the humidity
  TranslatedHumidity = SensorHumidity * HumiditySVP / AirSVP;

  // Move the translated humidity into the data structure
  ptrHumidity->CorrectedHumidity = (int16_t)(round(TranslatedHumidity * 10.0));
} // TranslateHumidity

// **************************************************************************
//
//  FUNCTION  : CalculateHumidityValues
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to humidity structure
//
//  O/P       : None.
//
//  OPERATION : Corrects the humidity and temperature values. If the
//              humidity temperature is above -40.0C, the sensor humidity is
//              used. If the humidity temperature is below -40.0C, the air
//              temperature is used with the last difference taken into
//              account.
//
//  UPDATED   : 2017-07-04 JHM
//
// **************************************************************************
static void CalculateHumidityValues(sHYT271_Sensor* ptrHumidity)
{
  // Calculate the corrected temperatures
  if (ptrHumidity->RawTemperature <= -4000)
  {
    ptrHumidity->CorrectedTemperature = Thermistor.TemperatureC + ptrHumidity->DeltaT;
  }
  else
  {
    ptrHumidity->CorrectedTemperature = ptrHumidity->RawTemperature;
    ptrHumidity->DeltaT = ptrHumidity->RawTemperature - Thermistor.TemperatureC;
  }

  // Translate the humidity if the radiosonde is IQ-3
  if (ModulationMode == MODULATION_MODE_TASK)
  {
    // Translate the humidity
    TranslateHumidity(ptrHumidity, &Thermistor);
  }
  else
  {
    ptrHumidity->CorrectedHumidity = ptrHumidity->RawHumidity;
  }
} // CalculateHumidityValues

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : HYT271_Init
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to an HYT271 data
//              structure
//
//  O/P       : None.
//
//  OPERATION : Initializes the humidity sensor. The sensor state will be
//              IDLE if the initialization was successful, OFFLINE if
//              unsuccessful.
//
//  UPDATED   : 2014-11-20 JHM
//
// **************************************************************************
void HYT271_Init(sHYT271_Sensor* ptrHumidity)
{
  // Intialize structure values
  ptrHumidity->I2Cx = BOOM_I2C;
  ptrHumidity->State = HYT271_ST_OFFLINE;
  // Assign wait channel from peripherals.h
  ptrHumidity->WaitChannel = HUMIDITY_WAIT_CH;
  ptrHumidity->Flag_CommsFault = RESET;
  ptrHumidity->Flag_Stale = RESET;
  ptrHumidity->HCount = 0;
  ptrHumidity->TCount = 0;
  ptrHumidity->RawHumidity = 9999;
  ptrHumidity->RawTemperature = 9999;
  ptrHumidity->CorrectedHumidity = 9999;
  ptrHumidity->CorrectedTemperature = 9999;

  // Read a measurement to determine whether or not the sensor is online
  ptrHumidity->State = HYT271_ST_START;

  do
  {
    HYT271_Handler(ptrHumidity);
    if (ptrHumidity->Flag_CommsFault == SET)
    {
      ptrHumidity->State = HYT271_ST_OFFLINE;
      break;
    }
  } while (ptrHumidity->State != HYT271_ST_START);
} // HYT271_Init

// **************************************************************************
//
//  FUNCTION  : HYT271_Handler
//
//  I/P       : sHYT271_Sensor* ptrHumidity - Pointer to an HYT271 data
//              structure
//
//  O/P       : None.
//
//  OPERATION : Handles the state machine for the HYT271 sensor.
//
//  UPDATED   : 2015-12-12 JHM
//
// **************************************************************************
void HYT271_Handler(sHYT271_Sensor* ptrHumidity)
{
  // Immediately return if the state machine is waiting
  if (GetWaitFlagStatus(ptrHumidity->WaitChannel) == SET)
  {
    return;
  }

  // Check for faults
  if (ptrHumidity->Flag_CommsFault == SET)
  {
    // Reset the flag
    ptrHumidity->Flag_CommsFault = RESET;
    // Reset the state machine
    ptrHumidity->State = HYT271_ST_START;
  }

  switch (ptrHumidity->State)
  {
    case HYT271_ST_OFFLINE:
      break;

    case HYT271_ST_START:
      // Send the command to start the measurement
      MeasurementRequest(ptrHumidity);
      // Advance the state machine if the command was successful
      if (ptrHumidity->Flag_CommsFault == RESET)
      {
        ptrHumidity->State = HYT271_ST_READ;
      }
      // Wait to read data or handler fault
      Wait(ptrHumidity->WaitChannel, 100);
      break;

    case HYT271_ST_READ:
      // Send the command to start the measurement
      DataFetch(ptrHumidity);
      // Advance the state machine if the command was successful
      if (ptrHumidity->Flag_CommsFault == RESET)
      {
        if (ptrHumidity->Flag_Stale == RESET)
        {
          ptrHumidity->State = HYT271_ST_LOAD;
        }
        else
        {
          Wait(ptrHumidity->WaitChannel, 5);
        }
      }
      break;

    case HYT271_ST_LOAD:
      // Convert the counts to raw temperature and humidity
      ProcessCounts(ptrHumidity);
      // Calculate the humidity values
      CalculateHumidityValues(ptrHumidity);
      // Reset the state machine
      ptrHumidity->State = HYT271_ST_START;
      // Wait to restart
      Wait(ptrHumidity->WaitChannel, 10);
      break;

    case HYT271_ST_IDLE:
      break;

    default:
      break;
    } // switch (ptrHumidity->State)
} // HYT271_Handler
