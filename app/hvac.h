#ifndef __hvac_h__
#define __hvac_h__
// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   hvac.h
//
//      CONTENTS    :   Header file for the internal heaters and boom
//                      defroster
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Port IO Hardware Configuration
#define HTR_PORT          GPIOE
#define HTR_PIN           GPIO_Pin_9
#define HTR_RCC           RCC_AHBPeriph_GPIOE

#define PWM_TIM           TIM12
#define PWM_RCC           RCC_APB1Periph_TIM12
#define PWM_FREQ          100000 // 100 kHz
#define PWM_TIC           100


// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  HTR_OFF = 0,
  HTR_ON = 1,
  HTR_AUTO = 2
} TypeDef_HeaterMode;

typedef struct
{
  TypeDef_HeaterMode Mode;
  FlagStatus Flag_HeaterOn;
  int8_t SetPoint;
  uint8_t PWM_Value;
  FlagStatus Flag_Disable;
} sHEATER;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sHEATER Heater;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void HVAC_Init(sHEATER* ptrHeater);
void HVAC_Handler(sHEATER* ptrHeater);
void HVAC_SetHeaterMode(sHEATER* ptrHeater, TypeDef_HeaterMode Mode);
void HVAC_EnablePWM(void);
void HVAC_DisablePWM(void);

#endif
