// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// ***************************************************************************
//
//      MODULE      :   config.c
//
//      CONTENTS    :   Routines for getting and setting the configuration.
//
// ***************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//       PRIVATE TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       GLOBAL VARIABLES
//
// **************************************************************************
sConfig iQ_Config;
sConfig iQ_Default;
FlagStatus Flag_Config_Defaults = SET;

// **************************************************************************
//
//       PRIVATE VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//       PRIVATE FUNCTION PROTOTYPES
//
// **************************************************************************
static void InitCRC(void);

// **************************************************************************
//
//        PRIVATE FUNCTIONS
//
// **************************************************************************
// **************************************************************************
//
//  FUNCTION  : InitCRC
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Initializes the CRC peripheral
//
//  UPDATED   : 2014-06-23 JHM
//
// **************************************************************************
static void InitCRC(void)
{
  // Start with the peripheral disabled
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);

  // Set CRC registers to default values
  CRC_DeInit();
} // InitCRC

// **************************************************************************
//
//        PUBLIC FUNCTIONS
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION    : InitConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Initializes the configuration structure
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
void Config_Init(void)
{
  // Set defaults
  iQ_Default.SerialNumber = 8675309;
  iQ_Default.ST0 = DEFAULT_A0;
  iQ_Default.ST1 = DEFAULT_A1;
  iQ_Default.ST2 = DEFAULT_A2;
  iQ_Default.ST3 = DEFAULT_A3;
  iQ_Default.ParallelResistorT = NTC_DEFAULT_R17;
  iQ_Default.PressureBiasCorrection = 0;
  iQ_Default.TxChannel0 = 4020;
  iQ_Default.TxChannel1 = 4025;
  iQ_Default.TxChannel2 = 4030;
  iQ_Default.TxChannel3 = 4035;
  iQ_Default.TxChannel4 = 4040;
  iQ_Default.TxChannel5 = 4045;
  iQ_Default.TxChannel6 = 4050;
  iQ_Default.ModulationMode = (uint16_t)MODULATION_MODE_IMET1;
  iQ_Default.VDD = SDADC_VDD_DEFAULT;
  iQ_Default.VAccurate1 = SDADC_VACC1_DEFAULT;
  iQ_Default.VADC1 = SDADC_VADC1_DEFAULT;
  iQ_Default.Deviation = 3;
  iQ_Default.Flag_AutoPowerLevel = (uint16_t)SET;
  iQ_Default.PowerLevel = (uint16_t)RFPA_PWR_1;
  iQ_Default.HeaterSetPoint = 5;
  iQ_Default.PWM_Value = 75;
  iQ_Default.Defrost_Mode = DF_Mode_Benoit;
  iQ_Default.ASCII_Mode = ASCII_OFF;

  // Initialize the CRC engine
  InitCRC();

  // Disable the peripherals while retrieving the configuration from flash
  Peripherals_Disable();

  // Get the configuration - if CRC fails it will return default
  Config_Get();

  // Turn the peripherals back on
  Peripherals_Enable();
} // InitConfig

// **************************************************************************
//
//  FUNCTION    : SaveConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Saves the sonde configuration to flash
//
//  UPDATED     : 2014-06-11 JHM
//
// **************************************************************************
FLASH_Status Config_Save(void)
{
  uint16_t Offset;
  FLASH_Status fsResult = FLASH_COMPLETE;
  uConfig Union;

  // Calculate new CRC Value (excluding checksum value)
  CRC_DeInit();
  iQ_Config.DataCRC = CRC_CalcBlockCRC((uint32_t *)&iQ_Config, (sizeof(iQ_Config)) >> sizeof(iQ_Config.DataCRC));

  // Copy sonde configuration to union structure
  memcpy(&Union, &iQ_Config, sizeof(Union));

  // Check that no flash memory operation is ongoing by checking the BSY bit
  // in the FLASH_SR register
  while (FLASH->SR & FLASH_SR_BSY);

  // Unlock the flash so it can be erased
  FLASH_Unlock();

  // Erase the page containing the configuration
  fsResult = FLASH_ErasePage(CFG_PAGE_LOCN);

  if (FLASH_GetFlagStatus(FLASH_FLAG_EOP) == SET)
  {
    // Success!
    FLASH_ClearFlag(FLASH_FLAG_EOP);
  }

  // Program the page with the new values
  Offset = 0;
  while ((Offset < sizeof(sConfig) / sizeof(uint16_t)) &&
         (fsResult == FLASH_COMPLETE))
  {
    fsResult = FLASH_ProgramHalfWord(CFG_PAGE_LOCN + 2 * Offset, Union.Data[Offset]);
    Offset++;
  } // while

  // Lock the flash for safety
  FLASH_Lock();

  return(fsResult);
} // SaveConfig

// **************************************************************************
//
//  FUNCTION    : GetConfig
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Gets the sonde configuration from flash or loads the
//                default values
//
//  UPDATED     : 2014-06-26 JHM
//
// **************************************************************************
void Config_Get(void)
{
  uint16_t Offset;
  uint32_t CalculatedCRC;
  uConfig Union;


  // Read values from flash
  for (Offset = 0; Offset < sizeof(sConfig) / 2; Offset++)
  {
    Union.Data[Offset] = *(uint16_t*)(CFG_PAGE_LOCN + (uint32_t)Offset * 2);
  }

  // Calculate the expected CRC on this data, with the CRC generator set to
  // 32-bit operation with initial=0xFFFFFFFF, polynomial=0x04C11DB7 and no reversing.
  CRC_DeInit();
  CalculatedCRC = CRC_CalcBlockCRC((uint32_t *)&Union,(sizeof(Union) >> sizeof(Union.Config.DataCRC)));

  if (CalculatedCRC != Union.Config.DataCRC)
  {
    // Copy the default settings to the configuration structure
    memcpy(&iQ_Config, &iQ_Default, sizeof(sConfig));
    Flag_Config_Defaults = SET;
  }
  else
  {
    // Copy the saved settings to the configuration structure
    memcpy(&iQ_Config, &Union, sizeof(sConfig));
    Flag_Config_Defaults = RESET;
  }
} // GetConfig

// **************************************************************************
//
//  FUNCTION    : Config_Set
//
//  I/P         : None.
//
//  O/P         : None.
//
//  OPERATION   : Saves the current configuration to flash
//
//  UPDATED     :
//
// **************************************************************************
void Config_Set(uConfig* Configuration)
{
  // Calculate new CRC Value (excluding checksum value)
  CRC_DeInit();
  iQ_Config.DataCRC = CRC_CalcBlockCRC((uint32_t *)&iQ_Config, (sizeof(iQ_Config)) >> sizeof(iQ_Config.DataCRC));

  // Read flash unlock status and unlock if necessary
  if (FLASH->CR & FLASH_CR_LOCK)
  {
    // Unlock the Flash Program Erase controller for writing
    FLASH_Unlock();
  }

  // Write to programming register
  FLASH->CR |= FLASH_CR_PG;
} // GetConfig

