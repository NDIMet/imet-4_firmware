// **************************************************************************
//
//      International Met Systems
//
//      iQ Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   interrupts.c
//
//      CONTENTS    :   Interrupt service routines
//
// **************************************************************************

// **************************************************************************
//
//        INCLUDE FILES
//
// **************************************************************************
#include "includes.h"

// **************************************************************************
//
//        CONSTANTS
//
// **************************************************************************
// None.

// **************************************************************************
//
//        TYPES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        GLOBAL VARIABLES
//
// **************************************************************************
// None.

// **************************************************************************
//
//        LOCAL VARIABLES
//
// **************************************************************************
static uint16_t Baud_Capture;
static uint16_t Data_Capture;
static int16_t ConversionValue;
static uint32_t ConversionChannel;
static uint8_t Mfg_Byte;
static uint8_t Ublox_Byte;
static uint16_t PWM_Capture1;
static uint16_t PWM_Capture2;

// **************************************************************************
//
//        INTERRUPT SERVICE ROUTINES
//
// **************************************************************************

// **************************************************************************
//
//  FUNCTION  : USART1_IRQHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the transmit and receive interrupts on the UART
//              associated with the manufacturing port.
//
//  UPDATED   : 2016-05-04 JHM
//
// **************************************************************************
void USART1_IRQHandler(void)
{
  // Receive Interrupt
  if(USART_GetITStatus(MFG_USART, USART_IT_RXNE) != RESET)
  {
    if (Flag_GPS_Loopback)
    {
      // Transmit the received data to the UBlox port
      Mfg_Byte = USART_ReceiveData(MFG_USART);
      Ublox_SendByte(Mfg_Byte);
    }
    else
    {
      // Reception Handler
      Mfg_RxHandler(&RX_Buffer);
    }
  }

  // Transmit Interrupt
  if(USART_GetITStatus(MFG_USART, USART_IT_TXE) != RESET)
  {
    // Transmission Handler
    Mfg_TxHandler(&TX_Buffer);
  }
} // USART1_IRQHandler

// **************************************************************************
//
//  FUNCTION  : USART2_IRQHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the transmit and receive interrupts on the UART
//              associated with the UBlox GPS port.
//
//  UPDATED   : 2016-05-04 JHM
//
// **************************************************************************
void USART2_IRQHandler(void)
{
  // Receive Interrupt
  if(USART_GetITStatus(UBX_USART, USART_IT_RXNE) != RESET)
  {
    // Clear the flag
    USART_ClearITPendingBit(UBX_USART, USART_IT_RXNE);

    if (Flag_GPS_Loopback)
    {
      Ublox_Byte = USART_ReceiveData(UBX_USART);
      // Add to the MFG transmit buffer
      TX_Buffer.TxBuffer[TX_Buffer.End] = Ublox_Byte;
      TX_Buffer.End++;
      if (MFG_TX_BUF_SIZE == TX_Buffer.End)
      {
        TX_Buffer.End = 0;
      }
      // Enable the TX empty interrupt (which should immediately occur)
      if (USART_GetITStatus(MFG_USART, USART_IT_TXE) == RESET)
      {
        USART_ITConfig(MFG_USART, USART_IT_TXE, ENABLE);
      }
    }
    else
    {
      // Reception Handler
      Ublox_RxHandler(&GPS_RX_Buffer);
    }
  }

  // Transmit Interrupt
  if(USART_GetITStatus(UBX_USART, USART_IT_TXE) != RESET)
  {
    // Transmission Handler
    Ublox_TxHandler(&GPS_TX_Buffer);
  }
} // USART2_IRQHandler


// **************************************************************************
//
//  FUNCTION  : SDADC1_IRQHandler
//
//  I/P       : None.
//
//  O/P       : None.
//
//  OPERATION : Handles the interrupt for SDADC1. It simply moves conversion
//              value into the NTC data buffer.
//
//  UPDATED   : 2016-05-04 JHM
//
// **************************************************************************
void SDADC1_IRQHandler(void)
{
  if (SDADC_GetFlagStatus(SDADC1, SDADC_FLAG_JEOC) == SET)
  {
    // Clear the flag
    SDADC_ClearFlag(SDADC1, SDADC_FLAG_JEOC);

    // Add the conversion value to the battery buffer
    ConversionValue = SDADC_GetInjectedConversionValue(SDADC1, &ConversionChannel);

    if (ConversionChannel == BATTV_SDADC_CH)
    {
      // Add to the battery count buffer and set the flag
      Battery.Counts[Battery.Index] = ConversionValue;
      Battery.Flag_ConversionReady = SET;
    }
  }
} // SDADC1_IRQHandler


// **************************************************************************
//
//  FUNCTION    :  TIM2_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for the TASK modulation timer.
//
//  UPDATED     :  2016-04-06 JHM
//
// **************************************************************************
void TIM2_IRQHandler(void)
{
  if (TIM_GetITStatus(MOD0_TIM, TIM_IT_Update) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(MOD0_TIM, TIM_FLAG_Update);

    // Handle the interrupt
    Modulation_TASK_Handler();
  }
} // TIM2_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM3_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for the iMet-1 modulation timer.
//
//  UPDATED     :  2016-07-21 JHM
//
// **************************************************************************
void TIM3_IRQHandler(void)
{
  if (TIM_GetITStatus(IMET1_TIM, TIM_IT_CC1) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(IMET1_TIM, TIM_FLAG_CC1);

    // Set up the next interrupt at 1200 Hz
    Baud_Capture = TIM_GetCapture1(IMET1_TIM);
    Baud_Capture += IMET1_BAUD_CCR_VAL;
    TIM_SetCompare1(IMET1_TIM, Baud_Capture);

    // Handle the interrupt
    Modulation_iMet1_Handler();
  }
  if (TIM_GetITStatus(IMET1_TIM, TIM_IT_CC2) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(IMET1_TIM, TIM_FLAG_CC2);

    // Toggle the data pin
    MOD0_PORT->ODR ^= MOD0_PIN;

    // Set up the next interrupt
    Data_Capture = TIM_GetCapture2(IMET1_TIM);
    Data_Capture += iMet1Modulation.CCR_Value;
    TIM_SetCompare2(IMET1_TIM, Data_Capture);
  }
} // TIM3_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM12_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for heating PWM timer.
//
//  UPDATED     :  2016-12-07 JHM
//
// **************************************************************************
void TIM12_IRQHandler(void)
{
  if (TIM_GetITStatus(PWM_TIM, TIM_IT_CC1) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(PWM_TIM, TIM_FLAG_CC1);

    // Configure the compare values
    PWM_Capture1 = TIM_GetCapture1(PWM_TIM);
    PWM_Capture2 = PWM_Capture1 + Heater.PWM_Value;
    PWM_Capture1 += PWM_TIC;
    TIM_SetCompare1(PWM_TIM, PWM_Capture1);
    TIM_SetCompare2(PWM_TIM, PWM_Capture2);

    if (Heater.Flag_HeaterOn == SET)
    {
      // Turn the heaters on to start the PWM
      GPIO_SetBits(HTR_PORT, HTR_PIN);
    }
    else
    {
      GPIO_ResetBits(HTR_PORT, HTR_PIN);
    }
  }
  if (TIM_GetITStatus(PWM_TIM, TIM_IT_CC2) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(PWM_TIM, TIM_FLAG_CC2);

    // Turn the heaters off since the PWM value has been reached
    GPIO_ResetBits(HTR_PORT, HTR_PIN);
  }
} // TIM12_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM6_DAC1_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Interrupt Service Routine for the wait timer.  This is
//                 used for more accurate, non-blocking delays.
//
//  UPDATED     :  2016-04-12 JHM
//
// **************************************************************************
void TIM13_IRQHandler(void)
{
  if (TIM_GetITStatus(WAIT_TIM, TIM_IT_Update) != RESET)
  {
    // Clear the interrupt
    TIM_ClearFlag(WAIT_TIM, TIM_FLAG_Update);

    // Increment the wait tics
    Wait_Tics++;

    // From peripherals.c
    WaitHandler();
  }
} // TIM13_IRQHandler

// **************************************************************************
//
//  FUNCTION    :  TIM7_IRQHandler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :  Handles the interrupt for the 1 ms user timer.
//
//  UPDATED     :  2016-04-01 JHM
//
// **************************************************************************
void TIM7_IRQHandler(void)
{
  // Update interrupt
  if (TIM_GetITStatus(USER_TIM, TIM_IT_Update) == SET)
  {
    // Clear the interrupt
    TIM_ClearITPendingBit(USER_TIM, TIM_IT_Update);

    // Increment Counter
    UserInterface_Handler();
  } // if
} // TIM4_IRQHandler


// **************************************************************************
//
//  FUNCTION    :  HardFault_Handler
//
//  I/P         :  None.
//
//  O/P         :  None.
//
//  OPERATION   :
//
//  UPDATED     :  2016-04-01 JHM
//                 20220204   jww
//
// **************************************************************************
void HardFault_Handler(void)
{
  NVIC_SystemReset();           // fatal error, restart MCU
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  NVIC_SystemReset();           // fatal error, restart MCU
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  NVIC_SystemReset();           // fatal error, restart MCU
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  NVIC_SystemReset();           // fatal error, restart MCU
}

