#ifndef __CC115L_h__
#define __CC115L_h__
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI, USA 49512
//
//                  Ph : (616) 285-7810 x 214
//                  Fx : (616) 957-1280
//                  Email : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   CC115L.h
//
//      CONTENTS    :   Header file for communication and control of the
//                      Texas Instruments CC115L transmitter IC
//
// **************************************************************************
// *************************************************************************
// INCLUDES
// *************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************
// Serial Port Peripheral Configuration
#define CC115L_SPI             SPI1
#define CC115L_SPI_RCC         RCC_APB2Periph_SPI1

// Transmitter IO Hardware Configuration
#define CC115L_SCK_PORT        GPIOB
#define CC115L_SCK_RCC         RCC_AHBPeriph_GPIOB
#define CC115L_SCK_PIN         GPIO_Pin_3
#define CC115L_SCK_PIN_SRC     GPIO_PinSource3
#define CC115L_SCK_AF          GPIO_AF_5

#define CC115L_MISO_PORT       GPIOB
#define CC115L_MISO_RCC        RCC_AHBPeriph_GPIOB
#define CC115L_MISO_PIN        GPIO_Pin_4
#define CC115L_MISO_PIN_SRC    GPIO_PinSource4
#define CC115L_MISO_AF         GPIO_AF_5

#define CC115L_MOSI_PORT       GPIOB
#define CC115L_MOSI_RCC        RCC_AHBPeriph_GPIOB
#define CC115L_MOSI_PIN        GPIO_Pin_5
#define CC115L_MOSI_PIN_SRC    GPIO_PinSource5
#define CC115L_MOSI_AF         GPIO_AF_5

#define CC115L_CS_PORT         GPIOA
#define CC115L_CS_PIN          GPIO_Pin_15
#define CC115L_CS_RCC          RCC_AHBPeriph_GPIOA

// Amplifier IO Hardware Configuration
#define RFPA_G8_PORT           GPIOB
#define RFPA_G8_PIN            GPIO_Pin_6
#define RFPA_G8_RCC            RCC_AHBPeriph_GPIOA

#define RFPA_G16_PORT          GPIOB
#define RFPA_G16_PIN           GPIO_Pin_7
#define RFPA_G16_RCC           RCC_AHBPeriph_GPIOA

#define RFPA_VPD_PORT          GPIOA
#define RFPA_VPD_PIN           GPIO_Pin_11
#define RFPA_VPD_RCC           RCC_AHBPeriph_GPIOA

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  RFPA_PWR_OFF = 0,
  RFPA_PWR_1 = 1,
  RFPA_PWR_2 = 2,
  RFPA_PWR_3 = 3,
  RFPA_PWR_4 = 4,
} TypeDef_RFPA_PowerLevel;

typedef enum
{
 CC115L_ST_IDLE = 0b000,
 CC115L_ST_TX = 0b010,
 CC115L_ST_FSTXON = 0b011,
 CC115L_ST_CALIBRATE = 0b100,
 CC115L_ST_SETTLING = 0b101,
 CC115L_ST_UNDERFLOW = 0b111
} TypeDef_CC115L_State;

typedef enum
{
  CC115L_MF_2FSK = 0b000,
  CC115L_MF_GFSK = 0b001,
  CC115L_MF_OOK = 0b011,
  CC115L_MF_4FSK = 0b100
} TypeDef_CC115L_ModulationFormat;

typedef struct
{
  TypeDef_CC115L_State State;
  TypeDef_RFPA_PowerLevel PowerLevel;
  FlagStatus Flag_AutoPowerLevel;
  uint8_t Deviation;
  FlagStatus Flag_Online;
} sCC115L;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sCC115L Transmitter;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void CC115L_Init(sCC115L* ptrTx);
void CC115L_SetState(sCC115L* ptrTx, FunctionalState NewState);
void CC115L_SetPowerAmpLevel(sCC115L* ptrTx, TypeDef_RFPA_PowerLevel PowerLevel);
void CC115L_SetAutoLevel(sCC115L* ptrTx, FlagStatus AutoLevelOn);
void CC115L_SetFrequency(sCC115L* ptrTx, uint16_t Frequency);
void CC115L_SetDeviation(sCC115L* ptrTx, uint8_t kiloHertz);
void CC115L_SetModulationFormat(sCC115L* ptrTx, TypeDef_CC115L_ModulationFormat ModulationFormat);

#endif
