#ifndef __USER_H
#define __USER_H
// **************************************************************************
//
//      International Met Systems
//
//      iMet-4 Radiosonde Firmware
//
//      AUTHOR  :   Justin Meulenberg
//
//                  International Met Systems
//                  3854 Broadmoor Ave. SE
//                  Suite 107
//                  Grand Rapids, MI 49512
//
//                  Ph : (616) 285-7810
//                  Fx : (616) 957-1280
//                  E-mail : jmeulenberg@intermetsystems.com
//
// **************************************************************************

// **************************************************************************
//
//      MODULE      :   user.h
//
//      CONTENTS    :   Header file for pushbutton, power, and LED control
//
// **************************************************************************
#include "includes.h"

// *************************************************************************
// CONSTANTS
// *************************************************************************

// LED Hardware
#define LED1_PORT              GPIOB
#define LED1_PIN               GPIO_Pin_14
#define LED1_RCC               RCC_AHBPeriph_GPIOB

#define LED2_PORT              GPIOB
#define LED2_PIN               GPIO_Pin_15
#define LED2_RCC               RCC_AHBPeriph_GPIOB

#define LED3_PORT              GPIOD
#define LED3_PIN               GPIO_Pin_8
#define LED3_RCC               RCC_AHBPeriph_GPIOD

#define LED4_PORT              GPIOA
#define LED4_PIN               GPIO_Pin_8
#define LED4_RCC               RCC_AHBPeriph_GPIOA

// Pushbutton Input Hardware
#define BUTTON_PORT            GPIOE
#define BUTTON_PIN             GPIO_Pin_8
#define BUTTON_RCC             RCC_AHBPeriph_GPIOE

// PowerOn Output Hardware
#define POWER_PORT             GPIOB
#define POWER_PIN              GPIO_Pin_2
#define POWER_RCC              RCC_AHBPeriph_GPIOB

// Timer Hardware
#define USER_TIM               TIM7
#define USER_TIM_RCC           RCC_APB1Periph_TIM7
#define USER_TIM_IRQ           TIM7_IRQn
#define USER_TIM_FREQ          10000  // 10 kHz

// Timer Count Values
#define PWR_ON_TICS            1000
#define PWR_OFF_TICS           2000
#define FREQ_TICS              100
#define LED_BLINK_TICS         1000
#define LED_FAST_TICS          100

// Button State Machine
#define PB_ST_BYPASSED         0
#define PB_ST_ON_DETECT        1
#define PB_ST_WAIT             2
#define PB_ST_ON               3
#define PB_ST_FREQ             4
#define PB_ST_OFF              5

#define USER_CHANNELS          7

#define LED_NONE               0
#define LED1                   0x01
#define LED2                   0x02
#define LED3                   0x04
#define LED4                   0x08
#define LED_ALL                0x0F

// *************************************************************************
// TYPES
// *************************************************************************
typedef enum
{
  LED_OFF = 0,
  LED_ON = 1,
  LED_BLINK = 2,
  LED_FAST_BLINK = 3
} Type_LED_State;

typedef struct
{
  uint8_t State;
  uint16_t Counter;
} sPushButton;

typedef struct
{
  uint8_t Channel;
  uint16_t Frequencies[USER_CHANNELS];
} sFrequencyChannels;

typedef struct
{
  FlagStatus Flag_Enabled;
  uint8_t LEDs_On;
  Type_LED_State State;
  uint16_t LED_Counter;
} sLEDS;

// *************************************************************************
// VARIABLE DEFINITIONS
// *************************************************************************
extern sPushButton PushButton;
extern sLEDS UserLEDs;
extern sFrequencyChannels FrequencyChannels;

// *************************************************************************
// FUNCTION PROTOTYPES
// *************************************************************************
void UserInterface_Init(void);
void UserInterface_Handler(void);
void UserInterface_EnableLED(sLEDS* ptrLEDS, FunctionalState NewState);
void UserInterface_SetLED(sLEDS* ptrLEDS, uint8_t LED_Number, Type_LED_State State);
void UserInterface_ChangeChannel(uint8_t NewChannel);

#endif /* __PERIPHERALS_H */
